export const environment = {
  production: true,
   cloudUrl:'https://us-central1-rare-lambda-245821.cloudfunctions.net/TestApp/', // use for test side
   //domain:'http://cssellapi.ikaart.org//',
     domain:'http://localhost:49937/',
    
    Admin:{
       //1)Login
       PostAdminLogin:"api/RESTAuthentication/PostAdminLoginData",
       UserForgotPassword:"api/cssell/ForGotPassword",
       UserChangePassword:"api/RESTAuthentication/PostUserChangePasswordData",
       PostAdminUser:"api/cssell/AddUserMasterData",
       GetAdminProfileUser:"api/cssell/AddUserMasterData",
 
       //2)App User
       GetUserFilter:"api/cssell/GetUserMasterData",
       PostUser:"api/cssell/AddUserMasterData",
          
       //product-deetails
       postproductdetails:"api/cssell/CreateUpdateProduct",
       Getproductdetails:"api/cssell/GetProduct",
       //ImageProductdetails:"api/HousePlant/UploadImages",
       DropDownProduct:"api/cssell/GetDropDownBin",
       GetCategoryDetails:"api/cssell/GetCategoryMaster",
       GetSubCategoryDetails:"api/cssell/GetSubCategoryMaster",
       AddSubCategoryDetails:"api/cssell/CreateUpdateSubCategoryMaster",
       CreateCategoryDetails:"api/cssell/CreateUpdateCategoryMaster",
       DropDownCategory:"api/cssell/GetDropDownCategoryMaster",
       ImageProductDetails:"api/cssell/UploadImages",
       DropDownDetails:"api/cssell/GetDropDownProduct",
       createUpdate_ImageProduct:"api/cssell/CreateUpdateImageProduct",
       GetbinMasterdetails:"api/cssell/GetBinMaster",
       GetprodMasterdetailsbyId:"api/cssell/GetProduct",
       AddVendorMasterDetails:"api/cssell/CreateUpdateVendorMaster",
       GetVendorMasterDetails:"api/cssell/GetVendorMaster",
       AddLocationMasterDetails:"api/cssell/CreateUpdateLocationMaster",
GetLocationMasterDetails:"api/cssell/GetLocationMaster",
AddColourMasterDetails:"api/cssell/CreateUpdateColourMaster",
GetColourMasterDetails:"api/cssell/GetColourMaster",

    }
};