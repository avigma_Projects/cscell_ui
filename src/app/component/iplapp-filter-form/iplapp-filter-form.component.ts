import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'iplapp-filter-form',
  template: `
              <form>
                <div class="row" style="color: black;background-color: #9b9b9b;">
                  <div class="col-lg-12">
                    <div class="form-row">
                      <div class="form-group" [ngClass]="{'col-md-2': !item.style, 'col-md-1': item.style }" 
                        *ngFor="let item of formFilters">
                        <label class="col-form-label" [for]="item.id">{{item.label}}</label>
                          <ng-template [ngIf]="item.type === 'select'" [ngIfElse]="inputBlock">
                            <select id="item.id" class="form-control form-control-sm"
                              [(ngModel)]="modelObj[item.model]" [ngModelOptions]="{standalone: true}">
                              <option value='0'>select</option>
                              <option [ngValue]="kb[item.value]" *ngFor="let kb of item.data">
                                {{kb[item.option]}}
                              </option>
                            </select>
                          </ng-template>
                          <ng-template #inputBlock>
                            <div [ngClass]="{'custom-control custom-checkbox': item.type==='checkbox'}">
                              <ng-container [ngSwitch]="item.type">
                                <input *ngSwitchCase="'checkbox'" type="checkbox" class="custom-control-input" [id]="item.id"
                                  [(ngModel)]="modelObj[item.model]" [ngModelOptions]="{standalone: true}"
                                  >
                                <input *ngSwitchDefault [type]="item.type" class="form-control form-control-sm" [id]="item.id"
                                  [(ngModel)]="modelObj[item.model]" [ngModelOptions]="{standalone: true}" [placeholder]="item.placeholder">
                              </ng-container>
                              <label class="custom-control-label" [for]="item.id" *ngIf="item.type==='checkbox'"></label>
                            </div>
                          </ng-template>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-12">
                    <div class="form-row">
                      <div class="form-group col-md-1">
                        <button id="client_viewdetail_1" class="btn btn-primary" (click)="filter()"><i
                            class="fas fa-filter mr-1"></i>Filter</button>
                      </div>

                      <div class="form-group col-md-1">
                        <button id="client_viewdetail_1" class="btn btn-primary" (click)="clear()"><i
                            class="fas fa-eraser mr-1"></i>Clear</button>
                      </div>

                      <div class="form-group col-md-1">
                        <button id="client_viewdetail_1" class="btn btn-primary" (click)="save()"><i
                            class="fas fa-save mr-1"></i>Save</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            `
})

export class IplAppFilterForm implements OnInit {
  @Input() formFilters;
  @Input() modelObj;

  @Output() filterCall = new EventEmitter();
  @Output() clearData = new EventEmitter();
  @Output() saveFilterData = new EventEmitter();

  isActive: boolean = true;

  ngOnInit() {
    console.log(this.modelObj, this.formFilters)
  }
  save() {
    console.log(this.modelObj)
    this.saveFilterData.emit();
  }

  clear() { 
    this.clearData.emit();
  }

  filter() {
    console.log(this.modelObj)
    this.filterCall.emit();
  }
}