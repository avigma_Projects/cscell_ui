interface UserFilterForm {
  label: string;
  id: string;
  type: string;
  placeholder?: string;
  model: string;
  data?: Array<any>,
  value?: string;
  option?: string;
  style?: string;
}


export const IplCompanyFilters:UserFilterForm[] = [
  {
    label: 'Company Name ',
    id: 'userName1',
    type: 'text',
    model: 'IPL_Company_Name',
    placeholder: 'Enter Company Name'
  },
  {
    label: 'Address',
    id: 'userName1',
    type: 'text',
    model: 'IPL_Company_Address',
    placeholder: 'Enter Address Name'
  },
  {
    label: 'Active',
    id: 'customCheck11dc',
    type: 'checkbox',
    model: 'IPL_Company_IsActive',
  }
]