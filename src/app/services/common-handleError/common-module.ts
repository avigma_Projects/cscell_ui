import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Routes } from '@angular/router'
import {ErrorhandlepageServices} from '../common-handleError/commonhandleError.services'
import { GridModule } from '@progress/kendo-angular-grid';



// const HomeRouts: Routes = [
//   {
//     path: '',
//     component: ,
//   }
// ]

@NgModule({
  declarations: [],
  imports: [
  
    CommonModule,
    FormsModule,
  
    ReactiveFormsModule,
    HttpClientModule,
    GridModule
  ],
  providers: [ErrorhandlepageServices],
  bootstrap: []
})

export class CommonHandelModule { }
