import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";

import { BehaviorSubject } from 'rxjs'
import { BaseUrl } from '../apis/rest-api';

@Injectable({
    providedIn: "root"
  })
  
  export class ErrorhandlepageServices {
    public Errorcall;
    public token: any;
    pathParam: any;
  
    constructor(private _http: HttpClient, private _Route: Router) {
      this.token = JSON.parse(localStorage.getItem('TOKEN'));
    }

  // common handler
  private handleError(error: HttpErrorResponse) {
    alert("Something bad happened; please try again later....");
    if (error.error instanceof ErrorEvent) {
      console.error("An error occurred:", error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }

    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }

  // common handler
  public CommonhandleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User Found...!');
      // window.location.href = '/';
    } else {
      alert("Something bad happened, please try again later...😌");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}