import { Injectable } from "@angular/core";
import { tap } from "rxjs/operators";
import {
  HttpClient,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { BehaviorSubject } from 'rxjs';
 import { LoginModel } from "../../models/login-model";
 import { environment } from "../../../environments/environment";


@Injectable({
  providedIn: "root"
})

export class AuthService {
  token: any;
 
  menuList: BehaviorSubject<Array<any>> = new BehaviorSubject<Array<any>>(null);
  public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private baseUrl = environment.domain;
  private apiUrlPost = this.baseUrl + environment.Admin.PostAdminLogin;
  constructor(
    private _http: HttpClient, 
    private _Route: Router,  
   
  ) {
    if (localStorage.getItem('TOKEN') != null) {
      this.token = JSON.parse(localStorage.getItem('TOKEN'));
    } 
  }

  public LoginuserPost(loginModel: LoginModel) {
    debugger
    var anydTo: any = {};
    anydTo.Ipl_Ad_User_Login_Name = loginModel.Ipl_Ad_User_Login_Name;
    anydTo.Ipl_Ad_User_Password = loginModel.Ipl_Ad_User_Password;
    anydTo.User_Token_val = loginModel.tokendetails;
    anydTo.Type = loginModel.type;
     
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
   
    return this._http
      .post<any>(this.apiUrlPost, anydTo, { headers: headers })
      .pipe(
        tap(data => {
          this.loggedIn.next(true);
          return data;
        }),
      );
  }

  private apiUrlPOSTForToken =  this.baseUrl + "/token";  
  public LoginuserGetToken(loginModel: LoginModel) {
    let userData = "username=" + loginModel.Ipl_Ad_User_Login_Name + "&password=" + loginModel.Ipl_Ad_User_Password + "&ClientId=" + loginModel.ClientId +"&grant_type=password" ;
    console.log('param',userData)
    let headers = new HttpHeaders({ "Content-Type": "application/x-www-form-urlencoded",'No-Auth':'True' });
    return this._http
      .post<any>(this.apiUrlPOSTForToken, userData, { headers: headers })
      .pipe(
        tap(data => {
          this.loggedIn.next(true);
          return data;
        })
      );
    }

  private apiUrlPOST2 = this.baseUrl + environment.Admin.UserForgotPassword;
  public ForgotpasswordPost(loginModel: LoginModel) {

    var ANYDTO: any = {};
    ANYDTO.User_LoginName = loginModel.user_LoginNameForgot;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST2, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
      );
  }

   // change password;
   private apiUrlPOST3 = this.baseUrl + environment.Admin.UserChangePassword;

   public ChangepasswordPost(loginModel: LoginModel) {
   var ANYDTO: any = {};
   ANYDTO.Ipl_Ad_User_PkeyID = loginModel.Ipl_Ad_User_PkeyID;
   ANYDTO.User_Password = loginModel.user_PasswordNew;
   ANYDTO.Type = 2;

   let headers = new HttpHeaders({ "Content-Type": "application/json" });
   headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
   return this._http
     .post<any>(this.apiUrlPOST3, ANYDTO, { headers: headers })
     .pipe(
       tap(data => {
         return data;
       }),
     );
  }

  public isAuthenticated(): boolean {
    debugger
    const token = JSON.parse(localStorage.getItem('TOKEN'));
    this.loggedIn.next(token ? true : false);
    return token ? true : false;
  }

  public isAccess(): boolean {
    var groupRoleId;
    if (localStorage.getItem('usertemp_') != null) {
      var encuser = JSON.parse(localStorage.getItem('usertemp_'));
   
    }

    return groupRoleId === 1 ? true : false;
  }

  public logout() {
    localStorage.clear();
    this.loggedIn.next(false);
  }
}
