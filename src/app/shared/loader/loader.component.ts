import { Component, OnInit } from '@angular/core';
import { ApirequestsService } from '../../shared/apirequests.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html'
  
})
export class LoaderComponent implements OnInit {

  mode='indeterminate';
  isDataLoading:BehaviorSubject<boolean>
  constructor(private apiService:ApirequestsService) {
    this.isDataLoading=this.apiService.handlerLoader

   }

   ngOnInit(){

   }
}
