import { Injectable } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { HttpInterceptor,HttpHandler,HttpEvent, HttpRequest } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { ApirequestsService } from './apirequests.service';

@Injectable()

export class  ProgressIntercepter implements HttpInterceptor  {
    constructor(private apiService:ApirequestsService) {
        
    }

    intercept(req:HttpRequest<any>,handler:HttpHandler):
    Observable<HttpEvent<any>>{
        //req.headers.append('ContentType','applicatoin/json'); you can add header also if need
        this.apiService.showLoader();
        return handler.handle(req).pipe(
            finalize(()=>this.apiService.hideLoader())
        );
    }
}
