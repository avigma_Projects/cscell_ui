import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApirequestsService {

  handlerLoader=new BehaviorSubject<boolean>(false)
  constructor() { }

  showLoader()
  {
this.handlerLoader.next(true);
  }
  hideLoader()
  {
    this.handlerLoader.next(false)
  }
}
