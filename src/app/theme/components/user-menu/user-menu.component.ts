import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserMenuComponent implements OnInit {

  constructor( private encrDecr: EncrDecrService) { 
    this.getLoggedinUser();
  }

  ngOnInit() {
  }
  User_FirstName: any;
  usersx: any;
  decuser: any;

  getLoggedinUser() {
    if (localStorage.getItem('usertemp_') != null) {
      var encuser = JSON.parse(localStorage.getItem('usertemp_'));
      var decval = this.encrDecr.get('123456$#@$^@1ERF', (encuser));
      this.decuser = JSON.parse(decval);
      this.usersx = this.decuser;
      if (this.usersx != undefined && this.usersx != null && this.usersx != "undefined") {
        this.User_FirstName = this.usersx[0].Ipl_Ad_User_First_Name;
      }
     
    }
  }
}
