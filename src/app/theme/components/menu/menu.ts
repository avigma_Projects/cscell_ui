import { Menu } from './menu.model';

export const verticalMenuItems = [ 
    // new Menu (1, 'Dashboard', '/home/dashboard', null, 'tachometer', null, false, 0),
      new Menu (2, 'User', '/home/appuser/viewappuser', null, 'user', null, false, 0), 
      new Menu (3, 'Products', '/home/product/viewappproduct', null, 'user', null, false, 0),
      new Menu (4, 'Category', '/home/Category/viewappcategory', null, 'user', null, false, 0),
      new Menu (5, 'SubCategory', '/home/SubCategory/viewappsubcategory', null, 'user', null, false, 0),
      new Menu (6, 'Bin Master', '/home/bin-master/viewbinmaster', null, 'user', null, false, 0),
      new Menu (7, 'Vendor ', '/home/vendormaster/viewvendormaster', null, 'user', null, false, 0),
      new Menu (8, 'Location ', '/home/locationmaster/viewlocationmaster', null, 'user', null, false, 0),
      new Menu (9, 'Colour', '/home/Colour/viewappcolour', null, 'user', null, false, 0),
      //new Menu (4, 'AddProducts', '/home/product/addproduct', null, 'user', null, false, 3),
    // new Menu (3, 'Company', '/home/iplcompany/viewiplcompany', null, 'building', null, false, 0),
    // new Menu (4, 'Contact Details', '/home/contact/viewcontactus', null, 'address-book', null, false, 0),  
    // new Menu (5, 'Admin User Details', '/home/user/viewuser', null, 'users', null, false, 0), 
     
  
    // new Menu (6, 'Adopter', '/home/adopter/viewipladopter', null, 'cogs', null, false, 0), 
    // new Menu (7, 'User Access', '/home/company/getcompany', null, 'user', null, false, 0), 
    // new Menu (8, 'Groups', '/home/group/details', null, 'user', null, false, 0), 
    // // new Menu (9, 'Group Details', '/home/groupdetails/viewgroup', null, 'user', null, false, 0), 
    // new Menu (9, 'Support Ticket', '/home/support/ticket', null, 'support', null, false, 0),
    // new Menu (10, 'Appliances', '/home/applinces/details', null, 'user', null, false, 0), 
    // new Menu (11, 'Background Provider', '/home/background/viewprovider', null, 'user', null, false, 0), 
    // new Menu (12, 'Import Details', '/home/import/viewimport', null, 'user', null, false, 0), 
    // new Menu (13, 'Video Training', '/home/video/training', null, 'user', null, false, 0), 
    
    
   
   
   
   

   
  
    new Menu (45, 'Blank', '/blank', null, 'file-o', null, false, 40),
    new Menu (46, 'Error', '/pagenotfound', null, 'exclamation-circle', null, false, 40),
   
]

export const horizontalMenuItems = [ 
    // new Menu (1, 'Dashboard', '/home/dashboard', null, 'tachometer', null, false, 0),
    new Menu (2, 'User', '/home/appuser/viewappuser', null, 'user', null, false, 0),
     new Menu (3, 'Products', '/home/product/viewappproduct', null, 'product', null, false, 0),
     new Menu (4, 'Category', '/home/Category/viewappcategory', null, 'user', null, false, 0),
     new Menu (5, 'SubCategory', '/home/SubCategory/viewappsubcategory', null, 'user', null, false, 0),
     new Menu (6, 'Bin Master', '/home/bin-master/viewbinmaster', null, 'user', null, false, 0),
     new Menu (7, 'Vendor ', '/home/vendormaster/viewvendormaster', null, 'user', null, false, 0),
     new Menu (8, 'Location ', '/home/locationmaster/viewlocationmaster', null, 'user', null, false, 0),
     new Menu (9, 'Colour', '/home/Colour/viewappcolour', null, 'user', null, false, 0),
     //new Menu (4, 'AddProducts', '/home/product/addproduct', null, 'user', null, false, 3),
    // new Menu (3, 'Company', '/home/iplcompany/viewiplcompany', null, 'building', null, false, 0), 
    // new Menu (4, 'Contact Details', '/home/contact/viewcontactus', null, 'address-book', null, false, 0),   
    // new Menu (5, 'Admin User Details', '/home/user/viewuser', null, 'users', null, false, 0),  
   
    // new Menu (6, 'Adopter', '/home/adopter/viewipladopter', null, 'cogs', null, false, 0), 
    // new Menu (7, 'User Access', '/home/company/getcompany', null, 'user', null, false, 0), 
    // new Menu (8, 'Groups', '/home/group/details', null, 'user', null, false, 0), 
    // // new Menu (9, 'Group Details', '/home/groupdetails/viewgroup', null, 'user', null, false, 0), 
    // new Menu (9, 'Support Ticket', '/home/support/ticket', null, 'support', null, false, 0),
    // new Menu (10, 'Appliances', '/home/applinces/details', null, 'user', null, false, 0), 
    // new Menu (11, 'Background Provider', '/home/background/viewprovider', null, 'user', null, false, 0), 
    // new Menu (12, 'Import Details', '/home/import/viewimport', null, 'user', null, false, 0), 
    // new Menu (13, 'Video Training', '/home/video/training', null, 'user', null, false, 0), 

    new Menu (45, 'Blank', '/blank', null, 'file-o', null, false, 40),
    new Menu (46, 'Error', '/pagenotfound', null, 'exclamation-circle', null, false, 40),
    
]