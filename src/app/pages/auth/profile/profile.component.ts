import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { AddUserModel } from '../../user/add-user/add-user-model';
import { AddUserServices } from '../../user/add-user/add-user.service';
import { viewUserModel } from '../../user/view-user/view-user-model';
import { ViewUserService } from '../../user/view-user/view-user.service';
import { ProfileModel } from './profile-model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  ProfileModelObj:ProfileModel = new ProfileModel();
  viewUserModelObj: viewUserModel = new viewUserModel();
  AddUserModelObj: AddUserModel = new AddUserModel();
  public personalForm:FormGroup;
  MessageFlag: string;
  button = "Update";
  isLoading = false;
  IsEditDisable = false;
  constructor(private formBuilder: FormBuilder, private xViewUserService:ViewUserService,
    private xAddUserServices:AddUserServices, private xmodalService: NgbModal,
    private xRouter: Router,) {
    this.GetsingleData();
  }
  ngOnInit() {
    this.personalForm = this.formBuilder.group({
      'salutation': [''],
      'firstname': ['', Validators.required],
      'lastname': ['', Validators.required],
      'username': ['', Validators.required],
      'gender': [''],
      'email': ['', Validators.required],
      'phone': ['', Validators.required],
      'password': ['', Validators.required],
      'cnfflag': ['', Validators.nullValidator],
      'city': ['', Validators.required],
      'state' : [''],
      'address' : [''],
     
    });
  }
  matchpsd: String;
  public onSubmit(values:Object):void {
    debugger
      if (this.personalForm.valid) {
        if (this.ProfileModelObj.Ipl_Ad_User_Password == this.ProfileModelObj.cnfflag ) {
          this.isLoading = true;
          this.AddUserModelObj = this.ProfileModelObj;
          this.xAddUserServices.AddUserDetail(this.AddUserModelObj)
          .subscribe(res =>{
            if (res[0] != "0") {
              this.MessageFlag = "User Profile Updated...!";
              this.isLoading = false;
              this.commonMessage();
            }
          })
        }
        else{
          this.matchpsd = "Password & Confirm Password Not Matched";
        }
      }
  }
  commonMessage() {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });
  }
  GetsingleData(){
    debugger
    this.viewUserModelObj.Type = 4;
   this.xViewUserService.ViewUserData(this.viewUserModelObj)
   .subscribe(res =>{
     console.log('editdata',res);
     this.ProfileModelObj.Ipl_Ad_User_PkeyID = res[0][0].Ipl_Ad_User_PkeyID;
     this.ProfileModelObj.Ipl_Ad_User_First_Name = res[0][0].Ipl_Ad_User_First_Name;
     this.ProfileModelObj.Ipl_Ad_User_Last_Name = res[0][0].Ipl_Ad_User_Last_Name;
     this.ProfileModelObj.Ipl_Ad_User_Login_Name = res[0][0].Ipl_Ad_User_Login_Name;
     this.ProfileModelObj.Ipl_Ad_User_Mobile = res[0][0].Ipl_Ad_User_Mobile;
     this.ProfileModelObj.Ipl_Ad_User_Password = res[0][0].Ipl_Ad_User_Password;
     this.ProfileModelObj.cnfflag = res[0][0].Ipl_Ad_User_Password;
     this.ProfileModelObj.Ipl_Ad_User_City = res[0][0].Ipl_Ad_User_City;
     this.ProfileModelObj.Ipl_Ad_User_State = res[0][0].Ipl_Ad_User_State;
     this.ProfileModelObj.Ipl_Ad_User_Email = res[0][0].Ipl_Ad_User_Email;
     this.ProfileModelObj.Ipl_Ad_User_Address = res[0][0].Ipl_Ad_User_Address;
     this.ProfileModelObj.Type = 2;
     this.IsEditDisable = true;
     this.personalForm.disable();
   
   })
  }
  EditForms() {
    this.IsEditDisable = false;
    this.personalForm.enable();
  }
  back(){
    this.xRouter.navigate(["/home/deshboard"]);
  }

}

export function emailValidator(control: FormControl): {[key: string]: any} {
  var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;    
  if (control.value && !emailRegexp.test(control.value)) {
      return {invalidEmail: true};
  }
}

