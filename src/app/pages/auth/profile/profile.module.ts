import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile.component';


export const routes = [
  { 
    path: "viewprofile",  component: ProfileComponent 
  },
 
];

@NgModule({
  declarations: [
    ProfileComponent, 
  
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class ProfileModule { }
