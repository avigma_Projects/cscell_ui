import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routes } from './auth.routing';
import { LoginComponent } from './login/login.component';
import { AuthService } from '../../services/auth/auth.service';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    CommonModule  
   
  ],
  declarations: [LoginComponent],
  providers: [AuthService],
})

export class AuthModule { }
