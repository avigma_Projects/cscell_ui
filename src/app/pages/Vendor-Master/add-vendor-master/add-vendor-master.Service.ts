import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { BaseUrl } from 'src/app/services/apis/rest-api';
import { ErrorhandlepageServices } from 'src/app/services/common-handleError/commonhandleError.services';
import { environment } from 'src/environments/environment';
import { ViewVendorMasterModel } from '../view-vendor-master/view-vendor-master.Models';
import {AddVendorMasterModel } from './add-vendor-master.Models';


@Injectable({
  providedIn: 'root'
})
export class AddVendorMasterService {
  baseurl: string;
 
   open(IplAppModalContent: IplAppModalContent, arg1: { size: string; ariaLabelledBy: string; }) {
    throw new Error('Method not implemented.');
  }
  public token:any;

  constructor(private _http: HttpClient,
    private _Route: Router,
   
    private xHomepageServices: ErrorhandlepageServices) { }
   

  
  private apiUrlPOST = BaseUrl + environment.Admin.AddVendorMasterDetails;
  public VendorMasterData(Modelobj: AddVendorMasterModel) {
    var ANYDTO: any = {};
    ANYDTO.Ven_PkeyID = Modelobj.Ven_PkeyID;
    ANYDTO.Ven_Name = Modelobj.Ven_Name;
    ANYDTO.Ven_PhoneNumber = Modelobj.Ven_PhoneNumber;
    ANYDTO.Ven_MobileNumber = Modelobj.Ven_MobileNumber;
    ANYDTO.Ven_Address = Modelobj.Ven_Address;
    ANYDTO.Ven_zip = Modelobj.Ven_zip;
    ANYDTO.Ven_latitude = Modelobj.Ven_latitude;
    ANYDTO.Ven_logitude = Modelobj.Ven_logitude;
   
    ANYDTO.Ven_IsActive = Modelobj.Ven_IsActive;
    ANYDTO.Ven_IsDelete = Modelobj.Ven_IsDelete;
    ANYDTO.UserID = Modelobj.UserID;
    
    

    ANYDTO.Type=Modelobj.Type;
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.CommonhandleError)
      );
  
  }
  
  public CommonhandleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User Found...!');
      // window.location.href = '/';
    } else {
      alert("Something bad happened, please try again later...😌");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }  
}