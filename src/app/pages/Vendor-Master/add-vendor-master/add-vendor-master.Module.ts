import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddVendorMasterComponent } from './add-vendor-master.component';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import{NgMultiSelectDropDownModule}from 'ng-multiselect-dropdown'
import { ReactiveFormsModule } from '@angular/forms';
import { AddVendorMasterService} from './add-vendor-master.Service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
export const appproductRoutes=[
  { path:'addnewvendormaster', component:AddVendorMasterComponent},
  { path:'addnewvendormaster/:Id', component:AddVendorMasterComponent}
  

 ]
@NgModule({
  declarations: [AddVendorMasterComponent],
  imports: [RouterModule.forChild(appproductRoutes),
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule, 
    CommonModule,
    DropDownsModule
  ],
  providers: [AddVendorMasterService],
  bootstrap: [AddVendorMasterComponent]
})
export class AddVendorModule { }
