import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';

import { AddVendorMasterModel } from './add-vendor-master.Models';
import { AddVendorMasterService} from './add-vendor-master.Service';
import { FormBuilder } from '@angular/forms';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewVendorMasterModel } from '../view-vendor-master/view-vendor-master.Models';
import { ViewVendorMasterService} from '../view-vendor-master/view-vendor-master.Service';
@Component({
  selector: 'app-add-vendor-master',
  templateUrl: './add-vendor-master.component.html',
  styleUrls: ['./add-vendor-master.component.scss']
})
export class AddVendorMasterComponent implements OnInit {

  ViewVendorMasterModelObj:ViewVendorMasterModel = new ViewVendorMasterModel();
  AddVendorMasterModelObj: AddVendorMasterModel = new AddVendorMasterModel();
  
  ModelObj: number;
  formUsrCommonGroup:FormGroup;
  public myFiles:Array<File> = [];
  RecordValList: any;
  GroupList: any;
  griddata: any;
  YESNOList: any;
  ProductActivelist:any;
  MessageFlag: string;
  submitted = false; // submitted;
  docdisable = true;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  IsEditDisable: boolean;
  isReadOnly: boolean;
  
  
  
  constructor(private xRouter:Router,
    private xRoute:ActivatedRoute,
    private xmodalService: NgbModal,
    private EncrDecr:EncrDecrService,
    private xaddvendorservices:AddVendorMasterService,
    private xviewvendorservices:ViewVendorMasterService,
    private formBuilder:FormBuilder

  ) 
  
  
  {


   }

  ngOnInit() {
    debugger
    const id1 = this.xRoute.snapshot.params['Id'];
    if (id1 == 'new' || id1 =='undefined')
     {
      this. AddVendorMasterModelObj = new AddVendorMasterModel();
    } else 
    {
      if(id1 != undefined)
      {
        
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
    }
      // this.commonMessage();
    }

    this.formUsrCommonGroup = this.formBuilder.group({
      Ven_Name:["", Validators.required],
      Ven_PhoneNumber:["",Validators.required],
      Ven_MobileNumber:["",Validators.required],
      Ven_Address:["",Validators.required],
      Ven_zip:["",Validators.required],
      //Ven_latitude:["",Validators.nullValidator],
      //Ven_logitude:["",Validators.nullValidator],
     
      

      imageUpload: [''],
    
    });  




  }
  getModelData(){
    
    debugger
    const id1 = this.xRoute.snapshot.params['id'];
    if (id1 == 'new') {
      this.ViewVendorMasterModelObj = new ViewVendorMasterModel();
    } else {
      debugger
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      debugger
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
    }


  }

  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  EditForms() {
    // this.IsEditDisable = false;
    // this.formUsrCommonGroup.enable();
  }

  commonMessage() {
   
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
      modalRef.componentInstance.MessageFlag = this.MessageFlag;
      modalRef.result.then(result => { }, reason => { });
 
    
  
  }


  GetSingleData(){
    debugger
    console.log("editres");
    this.ViewVendorMasterModelObj.Ven_PkeyID =this.ModelObj;
    this.ViewVendorMasterModelObj.Type = 2 ;
    this.xviewvendorservices.ViewVendorData(this.ViewVendorMasterModelObj).subscribe(res =>{
      console.log('edit',res)
      debugger
this.AddVendorMasterModelObj.Ven_PkeyID = res[0][0].Ven_PkeyID;
this.AddVendorMasterModelObj.Ven_Name = res[0][0].Ven_Name;
this.AddVendorMasterModelObj.Ven_PhoneNumber = res[0][0].Ven_PhoneNumber;
this.AddVendorMasterModelObj.Ven_MobileNumber = res[0][0].Ven_MobileNumber;
this.AddVendorMasterModelObj.Ven_Address = res[0][0].Ven_Address;
this.AddVendorMasterModelObj.Ven_zip = res[0][0].Ven_zip;
this.AddVendorMasterModelObj.Ven_latitude = res[0][0].Ven_latitude;
this.AddVendorMasterModelObj.Ven_logitude= res[0][0].Ven_logitude;

this.AddVendorMasterModelObj.Ven_IsActive = res[0][0].Ven_IsActive;
this.AddVendorMasterModelObj.Type=2;
if(this.AddVendorMasterModelObj.Ven_IsActive==false){
  this.AddVendorMasterModelObj.Ven_IsActive=false;
 }
 else{
  this.AddVendorMasterModelObj.Ven_IsActive=true;
 }

})
}

onIsActiveChanged(val){
  debugger
  if(val.target.value=='1: false'){
    this.AddVendorMasterModelObj.Ven_IsActive=false;
    }
    else{
      this.AddVendorMasterModelObj.Ven_IsActive=true;
    }

}


formButton(){
  debugger
 // .fromButton(this.AddAppproductModelObj)
  console.log('responese');
  // if (this.myimgaes.length > 0)
  // {
  //   this.AddTryeModelObj.Pro_Images = JSON.stringify(this.myimgaes)
  // } 
  if(this.ModelObj>0)
  {
    this.AddVendorMasterModelObj.Ven_PkeyID=this.ModelObj;
    this.AddVendorMasterModelObj.Type=2;
  }
  else{
    this.AddVendorMasterModelObj.Type=1;
  }
  if(this.formUsrCommonGroup.valid){
    debugger
    console.log('check',this.AddVendorMasterModelObj)
    this.xaddvendorservices.VendorMasterData(this.AddVendorMasterModelObj)
  .subscribe(respose =>{
    
    alert("Vendor Details Saved Sucessfully");
    console.log('res',respose)
    this.xRouter.navigate(["/home/vendormaster/viewvendormaster"]);
 

  })
  }
  else{
    alert("All Field are Required")
  }
     

}

}
