export class AddVendorMasterModel{
    Ven_PkeyID:number=0;
    Ven_Name:string='';
    Ven_PhoneNumber:string='';
    Ven_MobileNumber:string='';
    Ven_Address:string='';
    Ven_zip:string='';
    Ven_latitude:string='';
    Ven_logitude:string='';
    Ven_IsActive:boolean=false;
    Ven_IsDelete:boolean=false;
    UserID: Number = 0;
    Type:number=1;
}