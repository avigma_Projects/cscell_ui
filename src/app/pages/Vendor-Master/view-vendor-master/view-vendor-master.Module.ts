import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ViewVendorMasterComponent } from './view-vendor-master.component';
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ViewVendorMasterService } from './view-vendor-master.Service';


export const ViewAppproductRouts: Routes = [
  {  path: 'viewvendormaster', component:ViewVendorMasterComponent},
  

    // { 
    //  path: 'add/:id',
    //  loadChildren: () => import('../add-app-products/add-app-products.module').then(m => m.AddAppProductsModule ),
    //  },
  { path: '', redirectTo: 'home', pathMatch: 'full'}
]



@NgModule({
  declarations: [ViewVendorMasterComponent],
  imports: [ RouterModule.forChild(ViewAppproductRouts),
     
    GridModule, ExcelModule,
    FormsModule, ReactiveFormsModule,
    CommonModule
  ],
  providers: [ViewVendorMasterService],
  bootstrap: [ViewVendorMasterComponent]
})
export class ViewVendorModule { }
