import { Component, OnInit } from '@angular/core';
//import { Buttons } from './constants';
//import { GridColumnsproduct } from './constants';
import { State } from "@progress/kendo-data-query";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import{ViewVendorMasterModel}from './view-vendor-master.Models'
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { Router, Routes } from '@angular/router';
import { ViewVendorMasterService  } from './view-vendor-master.Service';

import { AddVendorMasterModel } from '../add-vendor-master/add-vendor-master.Models';
import {AddVendorMasterService } from '../add-vendor-master/add-vendor-master.Service';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-view-vendor-master',
  templateUrl: './view-vendor-master.component.html',
  styleUrls: ['./view-vendor-master.component.scss']
})
export class ViewVendorMasterComponent implements OnInit {

  ViewVendorMasterModelObj:ViewVendorMasterModel = new ViewVendorMasterModel();
  AddVendorMasterModelObj: AddVendorMasterModel = new AddVendorMasterModel();
  public griddata: any;
  formUsrCommonGroup:FormGroup;

  GroupList: any;
  //buttons = Buttons;
  //gridColumns = GridColumnsproduct;
  public state: State = {};
  MessageFlag: string;
  
  
  
  
  
  constructor(private xaddvendorservices:AddVendorMasterService,
    private xviewvendorservices:ViewVendorMasterService,
    private xRouter:Router,
    private EncrDecr: EncrDecrService,
    private xmodalService: NgbModal,

  ) 
  
  
  { 

  }

  ngOnInit(): void {
    debugger
    this.getvendorproduct()
  }
  getvendorproduct() 
  {
    debugger
    this.ViewVendorMasterModelObj.Type=1;
    console.log("response");
   this.xviewvendorservices.ViewVendorData(this.ViewVendorMasterModelObj).subscribe((res)=>{
     debugger
      console.log('nikhil',res);
    this.griddata = res[0];
    })
    var rec=this.xviewvendorservices.ViewVendorData;
    console.log(rec);
    // console.log(rec);
    

  }

  showDetails(event,dataItem)
  {
    debugger
    var encrypted=this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Ven_PkeyID);
    this.xRouter.navigate(["/home/addvendormaster/addnewvendormaster", btoa(encrypted)]);
  
  
  }
  deleteDetails(event,dataItem) {
    debugger

    var cfrm = confirm("Delete this Record...!");

    if (cfrm == true) {
      this.AddVendorMasterModelObj.Ven_PkeyID=dataItem.Ven_PkeyID;
      this.AddVendorMasterModelObj.Ven_IsActive=false;
      this.AddVendorMasterModelObj.Ven_IsDelete=true;
      this.AddVendorMasterModelObj.Type= 4;

      debugger
      this.xaddvendorservices.VendorMasterData(this.AddVendorMasterModelObj)
        .subscribe(response => {

          debugger
          this.MessageFlag = "Data Deleted Succesfully";
          this.commonMessage();
          console.log('delete response', response);
          this.getvendorproduct();
           
        });
     }
  
  
  }


  AddNewUser()
  {
 
 
 }
 commonMessage() {
   const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
   modalRef.componentInstance.MessageFlag = this.MessageFlag;
   modalRef.result.then(result => { }, reason => { });

 }
 checkChange(event, dataItem)
 {
   debugger
   this.AddVendorMasterModelObj.Ven_PkeyID = dataItem.Ven_PkeyID;
   this.AddVendorMasterModelObj.Ven_IsActive =event;//dataItem.HP_IsActive;
   this.AddVendorMasterModelObj.Type = 5;

   this.xaddvendorservices.VendorMasterData(this.AddVendorMasterModelObj)
   .subscribe(response => {
     this.MessageFlag = "Vendor status upated...!";
     this.commonMessage();
       this.getvendorproduct();
     });    
 
 
 }
 public dataStateChange(state: DataStateChangeEvent): void {
   this.state = state;
 }




}
