import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule } from '@progress/kendo-angular-grid';
import { UserMasterComponent } from '../user-master-list/user-master-list.component';
import { UserMatsersService } from './user-master-list.service';


const UserRouts = [

  { path:'', component: UserMasterComponent }

]



@NgModule({
  declarations: [UserMasterComponent],
 
  imports: [
    RouterModule.forChild(UserRouts),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    GridModule
  ],
  providers: [UserMatsersService],
  bootstrap: [UserMasterComponent]
})

export class UserMasterModule {

}
