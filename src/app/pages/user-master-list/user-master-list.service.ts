import { Injectable } from '@angular/core';
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";

import { environment } from '../../../environments/environment';
import { UserMastersModel } from './user-master-list.model';



@Injectable({
  providedIn: 'root'
})
export class UserMatsersService {
  
  public token:any;


  constructor(private http: HttpClient, private router: Router) { 
    debugger
    var currentUser = JSON.parse(localStorage.getItem('TOKEN'));
     //this.token = currentUser.token; // your token
     this.token = '9RweoYNgahH32VIhrhFvufDsxC9WEYYUoops7-_k3ma6yjCa3i245Tj7BqBxL5tjloJ9vk2Tu0ZmmtqaEePR8FgkQY_MP9IKBZ67czvRE8euvJkeDH8JrrLpYjLoOQLLtYxlvH2Vgxs_uUfKxY3nGIyNt9khBBl4t9UFPRJTAEjGjfca1q6qW6aQsM4Zc53LtqfzfA';//currentUser;
   }

   //BasetURL = "http://localhost:64562/";
   private baseUrl = environment.domain;
  private apiUrlGet = this.baseUrl + "api/Medsie/GetUserMasterListData";
  private apiUrlGet1 = this.baseUrl + "api/Medsie/";
  private apiUrlPost1 = this.baseUrl + "api/Medsie/AddUserMasterListData";

  public updateUserMasterListData(Modeluserinfo: UserMastersModel) {
    debugger
    if (localStorage.getItem('Token') != null) {
        this.token = JSON.parse(localStorage.getItem('Token'));
    }
     debugger
    var UserDTO: any = {};
    UserDTO.User_PkeyID = Modeluserinfo.User_PkeyID;
    // UserDTO.UM_Name = Modeluserinfo.UM_Name;
    // UserDTO.UM_Email = Modeluserinfo.UM_Email;
    // UserDTO.UM_Phone = Modeluserinfo.UM_Phone;
    // UserDTO.UM_Address = Modeluserinfo.UM_Address;
    UserDTO.User_IsActive = Modeluserinfo.User_IsActive;
  // AnyDTO.Type = Modelobjinfo.Type;
  UserDTO.Type = 3;

    let headers = new HttpHeaders({ 'Authorization': 'Bearer ' + `${this.token}` });
    return this.http
        .post<any>(this.apiUrlPost1, UserDTO, { headers: headers })
        .pipe(
            tap(data => {
                return data;
            }),
        );
}

  public GetUserMasterDetails(Modelobjinfo: UserMastersModel) {
    
    debugger;

    var AnyDTO: any = {};
    
    AnyDTO.User_PkeyID = Modelobjinfo.User_PkeyID;
    // AnyDTO.Type = Modelobjinfo.Type;
    AnyDTO.Type = Modelobjinfo.User_Name;
    AnyDTO.Type = Modelobjinfo.User_Email;
    AnyDTO.Type = Modelobjinfo.User_Phone;
    AnyDTO.Type = Modelobjinfo.User_Address;
    AnyDTO.Type = Modelobjinfo.User_IsActive;
    AnyDTO.Type = Modelobjinfo.Type;
   

    let headers = new HttpHeaders ({"content-Type": "application/json"});
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.post<any>(this.apiUrlGet, AnyDTO, { headers: headers})
    .pipe(
      tap(data => {
        return data;
      }),
    )
  
  }

  public GetUserMasterListDetails(Modelobjinfo: UserMastersModel) {
    
    debugger;

    var AnyDTO: any = {};
    
    AnyDTO.User_PkeyID = Modelobjinfo.User_PkeyID;
    AnyDTO.Type = Modelobjinfo.Type=1;

    let headers = new HttpHeaders ({"content-Type": "application/json"});
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.post<any>(this.apiUrlGet, AnyDTO, { headers: headers})
    .pipe(
      tap(data => {
        return data;
      }),
    )
  
  }

}