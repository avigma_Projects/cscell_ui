import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { ViewAppUserModel } from "./view-app-user-model";
import { BaseUrl } from "../../../services/apis/rest-api";
import { ErrorhandlepageServices } from '../../../services/common-handleError/commonhandleError.services';
import { environment } from "src/environments/environment";


@Injectable({
  providedIn: "root"
})
export class ViewAppUserServices {

  public token: any;

  constructor(private _http: HttpClient,
     private _Route: Router,
     private xHomepageServices: ErrorhandlepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // get user data
  //private apiUrlGet = BasetUrl.Domain + "api/RESTIPL/GetUserOrder";
  private apiUrlGet = BaseUrl + environment.Admin.GetUserFilter;

  public ViewUserData(Modelobj: ViewAppUserModel) {
    debugger
    var ANYDTO: any = {};
    ANYDTO.User_PkeyID = Modelobj.User_PkeyID;
    ANYDTO.Type = Modelobj.Type;

    // if (Modelobj.Single) {
    //   ANYDTO.User_PkeyID = Modelobj.User_PkeyID;
    //   ANYDTO.Type=Modelobj.Type;
      

    // }
    debugger
console.log('userjson',ANYDTO)
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
    } else {
      alert('Invalid Request...');
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}
