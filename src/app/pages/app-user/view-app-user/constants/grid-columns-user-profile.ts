import { Column } from '../../../../models/grid-column-model';

export const GridColumnsUserProfile: Column[] = [
  {
    title: 'First Name',
    field: 'User_Name'
  },
  {
    title: 'Email',
    field: 'User_Email'
  },
  
  
]
