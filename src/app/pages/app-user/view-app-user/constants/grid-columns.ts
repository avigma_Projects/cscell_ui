import { Column } from '../../../../models/grid-column-model';

export const GridColumnsUser: Column[] = [
  {
    title: 'First Name',
    field: 'User_Name'
  },
  {
    title: 'Email',
    field: 'User_Email'
  },
  // {
  //   title: 'Password',
  //   field: 'User_Password'
  // },
  {
    title: 'User Phone',
    field: 'User_Phone'
  },
  {
    title: 'Address',
    field: 'User_Address',
  },
  {
    title: 'City',
    field: 'User_City'
  },
  {
    title: 'Country',
    field: 'User_Country'
  },
  {
    title: 'Zip',
    field: 'User_Zip'
  },
  {
    title: 'DOB',
    field: 'User_DOB'
  },
  {
    title: 'Latitude',
    field: 'User_latitude'
  },
  {
    title: 'Logitude',
    field: 'User_longitude'
  },
  
]
