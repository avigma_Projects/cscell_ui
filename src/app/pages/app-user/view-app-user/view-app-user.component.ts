import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
import { ViewAppUserServices } from "./view-app-user.service";
import { ViewAppUserModel } from "./view-app-user-model";
import { AddAppUserModel } from '../add-app-user/add-app-user-model';
import { EncrDecrService } from '../../../services/util/encr-decr.service';
import { Buttons, GridColumnsUser } from './constants';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { IplAppModalContent } from '../../../component/iplapp-modal-content/iplapp-modal-content.component';
import { AddAppUserServices } from "../add-app-user/add-app-user.service";

@Component({
  templateUrl: "./view-app-user.component.html",
  styleUrls: ['./view-app-user.component.css']
})

export class ViewAppUserComponent implements OnInit {

  viewUserModelObj: ViewAppUserModel = new ViewAppUserModel();
  AddAppUserModelObj: AddAppUserModel = new AddAppUserModel();
 
  public griddata: any[];

  GroupList: any;
  buttons = Buttons;
  gridColumns = GridColumnsUser;
  public state: State = {};
  MessageFlag: string;
  constructor(
    private xViewUserServices: ViewAppUserServices, 
    private xAddAppUserServices:AddAppUserServices,
    private xRouter: Router,
    private EncrDecr: EncrDecrService,
    private xmodalService: NgbModal,
  ) {
    
    this.getautoUserdata();
    //this.GetDropDowndata();
  }

  ngOnInit() { 
    console.log(this.buttons)
  }

  getautoUserdata() {
    debugger
    this.viewUserModelObj.Type = 5;
    this.xViewUserServices
      .ViewUserData(this.viewUserModelObj)
      .subscribe(response => {
        console.log("nikhilusers", response);
        this.griddata = response[0];
      });
  }
  // commonMessage() {
    // const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    // modalRef.componentInstance.MessageFlag = this.MessageFlag;
    // modalRef.result.then(result => { }, reason => { });
  // }

  // this code selected event row
  showDetails(event, dataItem) {
    debugger
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.User_PkeyID);
    //this.xRouter.navigate(["/home/appuser/addappuser/new", btoa(encrypted)]);
    this.xRouter.navigate(["/home/appuser/addappuser/", btoa(encrypted)]);
    ///home/appuser/addappuser/2
  }

  // clear data
  AddNewUser() 
  {
    this.xRouter.navigate(["/home/appuser/addappuser", 'new']);
  }


  // common code End
  deleteDetails(event, dataItem) {
    debugger
     var cfrm = confirm("Do you want to delete this Record...!");

    if (cfrm == true) {
      this.AddAppUserModelObj.User_PkeyID = dataItem.User_PkeyID;
      this.AddAppUserModelObj.User_IsActive =false;
      this.AddAppUserModelObj.User_IsDelete =true;
      this.AddAppUserModelObj.User_DOB=null;
      this.AddAppUserModelObj.Type=3;

      this.xAddAppUserServices.UsertDataPost(this.AddAppUserModelObj)
        .subscribe(response => {

          debugger
          this.MessageFlag = "Data Deleted Succesfully";
          this.commonMessage();
          console.log('delete response', response);
          this.getautoUserdata();
         
        });
    }
  }
  

  commonMessage() 
  {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });
  }

  checkChange(event, dataItem) {

    this.AddAppUserModelObj.User_PkeyID = dataItem.User_PkeyID;
    this.AddAppUserModelObj.User_IsActive = !dataItem.User_IsActive;
    this.AddAppUserModelObj.Type = 4;

    this.xAddAppUserServices
    .UsertDataPost(this.AddAppUserModelObj)
    .subscribe(response => {
      this.MessageFlag = "user status upated...!";
      this.commonMessage();
        this.getautoUserdata();
      });    
  }


  //kendo check box event action
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }
}
