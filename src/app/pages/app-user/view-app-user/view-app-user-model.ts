export class ViewAppUserModel {
  User_PkeyID: number = 0;
  Type: Number = 1;
  User_FirstName: string = "";
  User_LastName: string = "";
  // User_LastName: string = "";
  User_Address: string = "";
  User_City: string = "";
  User_State: string = "";
  User_Zip: string = "";
  User_CellNumber: string = "";
  User_CompanyName: string = "";
  User_LoginName: string = "";
  user_email: string = "";
  User_Group: number = 0;
  User_Misc_Contractor_Score: string = "";
  User_IsActive: Boolean = true;
  // MenuID: Number = 0;
  // UserID: Number = 0;
  WhereClause: string = "";
  User_DOB:number=0;
  User_latitude:number=0;
  User_longitude:number=0;
  // Type: Number = 1;
  Single:boolean = false;
}
