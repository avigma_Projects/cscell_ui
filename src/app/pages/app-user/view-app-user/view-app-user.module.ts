import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
import { CommonModule } from '@angular/common';
import { ViewAppUserComponent } from './view-app-user.component';





export const ViewAppUserRouts: Routes = [
  {  path: 'viewappuser', component: ViewAppUserComponent },
  { 
    path: 'addappuser/:id',
    loadChildren: () => import('../add-app-user/add-app-user.module').then(m => m.AddAppUserModule),data: { breadcrumb: 'User Details' }
  },

  
  { path: '', redirectTo: 'viewappuser', pathMatch: 'full'}
]

@NgModule({
  declarations: [ViewAppUserComponent],
  imports: [
    RouterModule.forChild(ViewAppUserRouts),
   
    GridModule, ExcelModule,
    FormsModule, ReactiveFormsModule,CommonModule
  ],
  providers: [],
  bootstrap: [ViewAppUserComponent]
})

export class AppUserViewModule { }
