import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { AddAppUserModel, } from './add-app-user-model';
import { BaseUrl } from '../../../services/apis/rest-api';
import { ErrorhandlepageServices } from '../../../services/common-handleError/commonhandleError.services';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: "root"
})
export class AddAppUserServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: ErrorhandlepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // post data
  private apiUrlPOST = BaseUrl + environment.Admin.PostUser;


  public UsertDataPost(Modelobj: AddAppUserModel) {
    ////////dfebugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    ////dfebugger;
    var ANYDTO: any = {};
    ANYDTO.User_PkeyID = Modelobj.User_PkeyID;
    ANYDTO.User_Name = Modelobj.User_Name;
    ANYDTO.User_Email = Modelobj.User_Email;
    ANYDTO.User_Password = Modelobj.User_Password;
    ANYDTO.User_Phone = Modelobj.User_Phone;
    ANYDTO.User_Address = Modelobj.User_Address;
    ANYDTO.User_City = Modelobj.User_City;
    ANYDTO.User_Country = Modelobj.User_Country;
    ANYDTO.User_Zip = Modelobj.User_Zip;
    ANYDTO.User_latitude = Modelobj.User_latitude;
    ANYDTO.User_longitude = Modelobj.User_longitude;
    ANYDTO.User_DOB = Modelobj.User_DOB;
    ANYDTO.User_IsActive = Modelobj.User_IsActive;
    ANYDTO.User_IsDelete = Modelobj.User_IsDelete;
      
    // if (Modelobj.User_PkeyID !=  0  && Modelobj.User_PkeyID !=  null)
    // {
    //   Modelobj.Type = 2;
    // }
    // else
    // {
    //   Modelobj.Type=1;
    // }
    ANYDTO.Type=Modelobj.Type;
   



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
  // check user Name
  // post data

}
