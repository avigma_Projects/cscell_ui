export class  AddAppUserModel {
  
  // UM_PKID: Number = 0;
  // UM_Name: string = "";
  // UM_Email: string = "";
  // UM_Phone: string = "";
  // UM_Address: string = "";
  // UM_Password: string = "";
  // UM_City: string = "";
  // UM_Country: string = "";
  // UM_Zip: string = "";
  // UM_DOB: string = "";
  // UM_IsActive: boolean = true;
  // Type: Number = 1;
  User_PkeyID: number = 0;
  User_Name: string = "";
  User_Email: string = "";
  User_Password: string = "";
  User_Phone: string = "";
  User_Address: string = "";
  User_City: string = "";
  User_Country: string = "";
  User_Zip: string = "";
  User_IsActive: boolean = true;
  User_IsDelete: boolean;
  User_DOB:number=0;
  User_latitude:number=0;
  User_longitude:number=0;
  Type: number = 2;
  // User_Group: number;
}