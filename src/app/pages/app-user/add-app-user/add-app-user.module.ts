import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule, NgbDateAdapter, NgbDateNativeAdapter } from "@ng-bootstrap/ng-bootstrap";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { GridModule } from '@progress/kendo-angular-grid';
// import { SharedModule } from '../../../shared.module';
 import { AddAppUserComponent } from './add-app-user.component';
 import { AddAppUserServices } from './add-app-user.service';
// import { ColorDirective } from '../../../directives/color-change.directive';
 import { CommonDocumentServices } from '../../../services/document-upload/document-upload.service';
// import { CommonDirectiveModule } from '../../../directives/common-directive.module';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

export const AdduserRouts = [

  { path: "", component: AddAppUserComponent },
  { 
    path: 'addappuser/:id',
    loadChildren: () => import('../add-app-user/add-app-user.module').then(m => m.AddAppUserModule),data: { breadcrumb: 'User Details' }
  }

]

@NgModule({
  declarations: [AddAppUserComponent],
  imports: [
    RouterModule.forChild(AdduserRouts),
    NgbModule,
    NgMultiSelectDropDownModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule, 
   // CommonDirectiveModule,
   //SharedModule,
    DropDownsModule,
    GridModule,
    CommonModule
  ],

  providers: [
    AddAppUserServices, 
    CommonDocumentServices,
    {
      provide: NgbDateAdapter,
      useClass: NgbDateNativeAdapter
    }
  ],
  bootstrap: [AddAppUserComponent]
})

export class AddAppUserModule { }
