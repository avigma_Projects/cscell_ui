import { Component, OnInit, } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ActivatedRoute } from '@angular/router';
import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
import { AddAppUserServices } from "./add-app-user.service";
import { AddAppUserModel, } from "./add-app-user-model";
import { WorkOrderDrodownServices } from "../../../services/util/dropdown.service";
// import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { ViewAppUserServices } from "../view-app-user/view-app-user.service";
import { ViewAppUserModel } from "../view-app-user/view-app-user-model";
import { CommonDocumentServices } from "../../../services/document-upload/document-upload.service";
import { CommonDocumentUploadModel } from "../../../services/document-upload/document-upload-model";
import { BaseUrl } from "../../../services/apis/rest-api";
import { EncrDecrService } from '../../../services/util/encr-decr.service';
import { IplAppModalContent } from '../../../component/iplapp-modal-content/iplapp-modal-content.component';
import { from } from 'rxjs';
import { AuthService } from "src/app/services/auth/auth.service";
import { LoginModel } from "src/app/models/login-model";

import  _ from 'underscore';
import { GridColumnsUserProfile } from "../view-app-user/constants/grid-columns-user-profile";


@Component({
  templateUrl: "./add-app-user.component.html",
  styleUrls: ['./add-app-user.component.css']
})

export class AddAppUserComponent implements OnInit {
 
  userLoginModel: LoginModel = new LoginModel();
  AddUserModelObj: AddAppUserModel = new AddAppUserModel();
  // ContractorMapObj: ContractorMap = new ContractorMap();
  // ContractorCoverageAreaObj:ContractorCoverageArea = new ContractorCoverageArea();
  // ContractorMapStateObj: ContractorMapState = new ContractorMapState();
  // WorkOrderCustomizeObj: WorkOrderCustomize = new WorkOrderCustomize();
  ViewUserModelObj: ViewAppUserModel = new ViewAppUserModel();
  CommonDocumentUploadModelObj: CommonDocumentUploadModel = new CommonDocumentUploadModel();
  // CountyZipModelObj: CountyZipModel = new CountyZipModel();
  formUsrCommonGroup: FormGroup;
  submitted = false; // submitted;
  docdisable = true;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  UserNameReadOnly = false;
  MessageFlag: string; // custom msg sathi
  RecordValList: any;
  GroupList: any;
  griddata: any;
  YESNOList: any;
  CarrierList: any;
  StateList: any;
  TimeList: any;
  StartDateList: any;
  DocumentTypeList: any;
  OrderDisplayList: any;
  PastOrderDisplayList: any;
  BackgroundCheckProviderList: any;
  FormArrayVal = [];
  // only drop down
  dropCkck = false; // common
  RecordValFlag = false;
  GroupValFlag = false;
  DisplayWOValFlag = false;
  ActiveValFlag = false;
  HistoryValFlag = false;
  TimeValFlag = false;
  StateValFlag = false;
  IsPasswordDisable = false;
  imgpath: string; //img sath
  docPath: string; // document sathi
  BaseURLpath: string;
  AddressArray:any;
  ConstateList:any;
  County:any;
  UserTimeList:any;
  checkAll: boolean = false;
  isStateInValid = false;
  isCountyInValid = false;
  isReadOnly:boolean;
  gridColumns = GridColumnsUserProfile;  

  public defaultStateItem: { IPL_StateName: string, IPL_StateID: number } = { IPL_StateName: 'Select', IPL_StateID: 0 };
  public defaultCountyItem: { COUNTY: string, ID: number } = { COUNTY: 'Select', ID: 0 };
  public drpStateList: Array<string>;
  public drpCountyList: Array<string>;
  CountyList: any;
  public state: State = {};
  FormArrayValtwo:any;
  ModelObj: number;
  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xAddUserServices: AddAppUserServices,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices,
    // private xMasterlayoutComponent: MasterlayoutComponent,
    private xViewUserServices: ViewAppUserServices,
    private xCommonDocumentServices: CommonDocumentServices,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrService,
    private authService: AuthService,
  ) {
   
    // this.GetContactrorDropDown();
    // this.GetDropDowndata();
    //this.ConstateList = this.source.slice();
  }

  ngOnInit()
  {
   debugger
    const id1 = this.xRoute.snapshot.params['id'];
    if (id1 == 'new'  )
     {
      this.AddUserModelObj = new AddAppUserModel();
    } else {
      this.isReadOnly =true;
      let id =this.EncrDecr.get('123456$#@$^@1ERF',atob(id1) );
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
      // this.commonMessage();
    }
    this.formUsrCommonGroup = this.formBuilder.group({
      FirstName: ["", Validators.required],
      LastName: ["", Validators.required],
      EmailVal: ["", [Validators.required, Validators.email]],
      LoginNameVal: ["", Validators.required],
      PasswordVal: [""/*, [Validators.required, Validators.minLength(6)]*/],
      userphone:["",Validators.nullValidator],
      GroupVal: ["", Validators.required],
      desaddress: ["", Validators.required],
      descity: ["", Validators.nullValidator],
      desstate: ["", Validators.required],
      descounty: ["", Validators.nullValidator],
      deszip: ["", Validators.required],
      dob:["",Validators.nullValidator],
      latitude:["",Validators.nullValidator],
      longitude:["",Validators.nullValidator],
   ///***   ***////
      
    });


    // this.AddressArray = [
    //  { Cont_Coverage_Area_PkeyId:  0,
    //   Cont_Coverage_Area_UserID: 0,
    //   Cont_Coverage_Area_State_Id: 0,
    //   Cont_Coverage_Area_County_Id: 0,
    //   Cont_Coverage_Area_Zip_Code: 0,
    //   Cont_Coverage_Area_IsActive: true,
    //   Cont_Coverage_Area_IsDelete: false,
    // }
    // ]

   

    

    // this.dropdownSettings = {
    //   singleSelection: false,
    //   idField: "Client_pkeyID",
    //   textField: "Client_Company_Name",
    //   selectAllText: "Select All",
    //   unSelectAllText: "UnSelect All",
    //   itemsShowLimit: 4,
    //   allowSearchFilter: true
    // };
  }
  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  getModelData(){
   
    this.GetSingleData();
     
      debugger
  
    
  
    
  
  
  }
  commonMessage() 
  {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
      modalRef.componentInstance.MessageFlag = this.MessageFlag;
      modalRef.result.then(result => { }, reason => { });
  
  }
  GetSingleData()
  {
    debugger
    console.log("editres");
    this.ViewUserModelObj.User_PkeyID =this.ModelObj;
    this.ViewUserModelObj.Type = 6 ;
    this.xViewUserServices.ViewUserData(this.ViewUserModelObj).subscribe(res =>{
      console.log('edit',res);
      debugger
this. AddUserModelObj.User_PkeyID = res[0][0].User_PkeyID;
this.AddUserModelObj.User_Name = res[0][0].User_Name;
this.AddUserModelObj.User_Email = res[0][0].User_Email;
this.AddUserModelObj.User_Password = res[0][0].User_Password;
this.AddUserModelObj.User_Phone = res[0][0].User_Phone;
this.AddUserModelObj.User_Address = res[0][0].User_Address;
this.AddUserModelObj.User_City = res[0][0].User_City;
this.AddUserModelObj.User_Country = res[0][0].User_Country;
this.AddUserModelObj.User_Zip = res[0][0].User_Zip;
this.AddUserModelObj.User_DOB = res[0][0].User_DOB;
this.AddUserModelObj.User_latitude = res[0][0].User_latitude;
this.AddUserModelObj.User_longitude = res[0][0].User_longitude;
this.AddUserModelObj.Type =2;

    })
  
  }
  formButton()
  {
    debugger
    // console.log('user res');
    if(this.ModelObj>0)
    {
      this.AddUserModelObj.User_PkeyID=this.ModelObj;
      this.AddUserModelObj.User_IsActive=true;
      this.AddUserModelObj.User_IsDelete=false;
  
      this.AddUserModelObj.Type=2;

    }
    
    else
    {
      this.AddUserModelObj.Type=1;
      this.AddUserModelObj.User_IsActive=true;
      this.AddUserModelObj.User_IsDelete=false;

      // ModelObj.Type=1;
    }

    
       
        console.log('check',this.AddUserModelObj)
      this.xAddUserServices.UsertDataPost(this.AddUserModelObj)
      .subscribe(respose =>{
        debugger
      console.log('res',respose);
      // var xe=respose[0].splice("#");

      })
  
  
  
  }
  
  GetuserProfileDatabyId(){
    debugger
    console.log("editres");
    this.ViewUserModelObj.User_PkeyID =this.ModelObj;
    
    this.ViewUserModelObj.Type = 6 ;
    this.xViewUserServices.ViewUserData(this.ViewUserModelObj).subscribe(res =>{
      console.log('edit',res)
      debugger
      this.griddata = res[0];

  });
  }
  openXl(content) {
  
    this.xmodalService.open(content, { size: 'xl' });
    this.GetuserProfileDatabyId();
  }

}

  // shortcurt Namefor form sathi
