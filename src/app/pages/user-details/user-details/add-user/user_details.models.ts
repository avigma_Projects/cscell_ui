export class UserMasterModel{
    User_PkeyID:Number = 0;
    User_Name:String="";
    User_Email:String="";
    User_Password:String="";
    User_Phone:String="";
    User_Address:String="";
    User_City:String="";
    User_Country:String="";
    User_Zip:String="";
    User_DOB:string = '';
    User_IsActive:boolean= true;
    User_IsDelete:boolean=false;
    UserID:Number=0;
    Type:Number=1;
}
