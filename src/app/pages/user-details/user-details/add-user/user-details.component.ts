import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
//import { EncrDecrService } from '../../services/encr-decr.service';
import { ViewUserModel } from '../view-user/view-user.model';
import { ViewUserService } from '../view-user/view-user.service';
import { UserMasterModel } from './user_details.models';
import { Userdetailsservice } from './user_details.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.sass']
})
export class UserDetailsComponent  implements OnInit {

  UserMasterObj:UserMasterModel=new UserMasterModel();
  ViewUserModelObj: ViewUserModel = new ViewUserModel()
  formUsrCommonGroup:FormGroup;
  ModelObj:any;
  constructor( private formBuilder:FormBuilder,private xuser_detailsservice:Userdetailsservice,
    private EncrDecr: EncrDecrService,
    private xRouter: Router, private xRoute: ActivatedRoute,
    private xViewUserService:ViewUserService) { }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      usernameval:["",Validators.nullValidator],
      useremailval:["",Validators.nullValidator],
      userpasswordval:["",Validators.nullValidator],
      userphoneval:["",Validators.nullValidator],
      useraddressval:["",Validators.nullValidator],
      usercityval:["",Validators.nullValidator],
      usercountryval:["",Validators.nullValidator],
      userzipval:["",Validators.nullValidator],
      userdobval:["",Validators.nullValidator],
    });    
// this.getModelData();
  }
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  getModelData() {
    debugger
    const id1 = this.xRoute.snapshot.params['id'];
    if (id1 == 'new') {
      this.UserMasterObj = new UserMasterModel();
    } else {
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
    }
  }
  GetSingleData(){
    debugger
    
    this.ViewUserModelObj.User_PkeyID =   this.ModelObj;
    this.ViewUserModelObj.Type = 2;
    this.xViewUserService.ViewUserMaster(this.ViewUserModelObj).subscribe(res =>{
      console.log('edit',res)
this.UserMasterObj.User_PkeyID = res[0][0].User_PkeyID;
this.UserMasterObj.User_Address = res[0][0].User_Address;
this.UserMasterObj.User_City = res[0][0].User_City;
this.UserMasterObj.User_Country = res[0][0].User_Country;
this.UserMasterObj.User_DOB = res[0][0].User_DOB;
this.UserMasterObj.User_Email = res[0][0].User_Email;
this.UserMasterObj.User_Name = res[0][0].User_Name;
this.UserMasterObj.User_Password = res[0][0].User_Password;
this.UserMasterObj.User_Phone = res[0][0].User_Phone;
this.UserMasterObj.User_Zip = res[0][0].User_Zip;
this.UserMasterObj.Type =2;

    })

  }

  formButton(){
    debugger
    console.log('check',this.UserMasterObj)
this.xuser_detailsservice.AddUserMaster(this.UserMasterObj)
.subscribe(respose =>{
  console.log('res',respose)

})
  }

}
