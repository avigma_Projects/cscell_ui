import { Injectable } from "@angular/core";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {UserMasterModel } from "./user_details.models";
import { throwError } from "rxjs";
import { environment} from '../../../../environments/environment'

@Injectable({
  providedIn: "root"
})
export class Userdetailsservice {
public baseurl = environment.domain;
  public token: any;

  constructor(private _http: HttpClient, private _Route: Router) {
  
  }

  

  private apiUrlPOST = this.baseurl + "/api/CommunaV/AddUserMasterData";
  public AddUserMaster(Modelobj:UserMasterModel ) {
    debugger
    let ANYDTO: any = {};
    ANYDTO.User_PkeyID = Modelobj.User_PkeyID;
    ANYDTO.User_Name = Modelobj.User_Name;
    ANYDTO.User_Email = Modelobj.User_Email;
    ANYDTO.User_Password = Modelobj.User_Password;
    ANYDTO.User_Phone = Modelobj.User_Phone;
    ANYDTO.User_Address = Modelobj.User_Address;
    ANYDTO.User_City = Modelobj.User_City;
    ANYDTO.User_Country = Modelobj.User_Country;
    ANYDTO.User_Zip = Modelobj.User_Zip;
    ANYDTO.User_DOB = Modelobj.User_DOB;
    ANYDTO.User_IsActive = Modelobj.User_IsActive;
    ANYDTO.User_IsDelete = Modelobj.User_IsDelete;
    ANYDTO. UserID = Modelobj. UserID;
   debugger

    if (Modelobj.User_PkeyID != 0) {
      ANYDTO.Type = 2;
      if ( Modelobj.Type == 4) 
      {
        ANYDTO.Type = 4;
      }
    }
    else
    {
      ANYDTO.Type = 1;
    }

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.CommonhandleError)
      );
  }

  public CommonhandleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User Found...!');
      // window.location.href = '/';
    } else {
      alert("Something bad happened, please try again later...😌");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }  

}





    
    


    

    

