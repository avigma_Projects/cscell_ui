import { NgModule } from "@angular/core";
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import {UserDetailsComponent} from "./user-details.component";

export const UserRouts=[
    {
        path:"",component:UserDetailsComponent,
    }
];
@NgModule({
    declarations: [
        UserDetailsComponent
    ],
    imports: [
        RouterModule.forChild(UserRouts),
        FormsModule,
        ReactiveFormsModule,
    ],
    providers:[],
    bootstrap:[UserDetailsComponent]
})
export class UserMasterMModule{}
