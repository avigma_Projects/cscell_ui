import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {ViewUserModel} from '../view-user/view-user.model'
import { ViewUserService } from './view-user.service';
import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
// import{EncrDecrService}from "../../../services/encr-decr.service";
import { Router } from '@angular/router';
import { Userdetailsservice } from '../add-user/user_details.service';
import { UserMasterModel } from '../add-user/user_details.models';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.sass']
})
export class ViewUserComponent implements OnInit {
  ViewUserModelObj: ViewUserModel = new ViewUserModel();
  UserMasterObj:UserMasterModel=new UserMasterModel();
  formUsrCommonGroup:FormGroup;
  public state: State = {};
  griddata:any;
  constructor(formbuilder:FormBuilder,
    private xViewUserService:ViewUserService,
    // private EncrDecr: EncrDecrService,
    private xRouter: Router,
    private xuserdetailsservice:Userdetailsservice) { }

  ngOnInit(){
    this.GetGridDetails();
  }

  GetGridDetails(){ 
    debugger
this.xViewUserService.ViewUserMaster(this.ViewUserModelObj)
.subscribe(res =>{
  console.log('unnati',res)
  this.griddata = res[0];
})
  

  }
  showDetails(val){
 debugger
    // var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', val.User_PkeyID);
    // this.xRouter.navigate(["home/user/adduser", btoa(encrypted)]);

  }
  deleteDetails(event, item){
    this.UserMasterObj.User_PkeyID = item.User_PkeyID;
    this.UserMasterObj.Type= 4;
    this.xuserdetailsservice.AddUserMaster(this.UserMasterObj)
    .subscribe(respose =>{
      console.log('res',respose)

      this.GetGridDetails()
    
    })
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }

}
