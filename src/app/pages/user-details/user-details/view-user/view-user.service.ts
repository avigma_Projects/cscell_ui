import { Injectable } from "@angular/core";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {ViewUserModel} from '../view-user/view-user.model'
import { throwError } from "rxjs";
import { environment } from "src/environments/environment";

// import { environment} from '../../../../environments/environment'

@Injectable({
  providedIn: "root"
})

export class ViewUserService{
    public baseurl = environment.domain;
    public token: any;
  
    constructor(private _http: HttpClient, private _Route: Router) {
    
    }


    private apiUrlPOST = this.baseurl + "/api/CommunaV/GetUserMasterData";
    public ViewUserMaster(Modelobj:ViewUserModel ) {
      debugger
      let ANYDTO: any = {};
      ANYDTO.User_PkeyID = Modelobj.User_PkeyID;
      ANYDTO.Type = Modelobj.Type;
  
      let headers = new HttpHeaders({ "Content-Type": "application/json" });
      headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
      return this._http
        .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
        .pipe(
          tap(data => {
            return data;
          }),
          catchError(this.CommonhandleError)
        );
    }

    public CommonhandleError(error: HttpErrorResponse) {
        if (error.status == 401) {
          alert('Unauthorized User Found...!');
          // window.location.href = '/';
        } else {
          alert("Something bad happened, please try again later...😌");
        }
    
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error("An error occurred:", error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(
            `Backend returned code ${error.status}, ` + `body was: ${error.error}`
          );
        }
        // return an observable with a user-facing error message
        return throwError("Something bad happened; please try again later.");
      }  

}







    
    


    

    

