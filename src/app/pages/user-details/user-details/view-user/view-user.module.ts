import { NgModule } from "@angular/core";
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import {ViewUserComponent} from "./view-user.component";
import {ViewUserService} from '../view-user/view-user.service'
import { GridModule } from '@progress/kendo-angular-grid';

export const ViewUserRouts=[
    {
        path:"details",component:ViewUserComponent,
    },
    {

        path:'adduser/:id',
        loadChildren:()=>import('../add-user/user_details.module').then(m=> m.UserMasterMModule),
      },
      { path: '', redirectTo: 'details', pathMatch: 'full'}
];
@NgModule({
    declarations: [
        ViewUserComponent
    ],
    imports: [
        RouterModule.forChild(ViewUserRouts),
        FormsModule,
        ReactiveFormsModule,
        GridModule
    ],
    providers:[ViewUserService],
    bootstrap:[ViewUserComponent]
})
export class ViewUserModule{}
