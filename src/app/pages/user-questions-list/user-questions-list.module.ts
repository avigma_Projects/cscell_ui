import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule } from '@progress/kendo-angular-grid';
import { UserQuestinsListService } from './user-questions-list.service';
import { UserQuestionsListComponent } from './user-questions-list.component';


const UserRouts = [

  { path:'viewlist', component: UserQuestionsListComponent }

]



@NgModule({
  declarations: [UserQuestionsListComponent],
 
  imports: [
    RouterModule.forChild(UserRouts),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    GridModule
  ],
  providers: [],
  bootstrap: [UserQuestionsListComponent]
})

export class UserQuestionsListModule {

}
