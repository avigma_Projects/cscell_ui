import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EncrDecrService } from '../../services/util/encr-decr.service';
// import { EncrDecrService } from '../../../services/util/encr-decr.service';
//import { UserListService } from '../user_list_service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common'
// import { FilterdataPipe } from '../../shared/filterdata.pipe';
import { DataBindingDirective } from '@progress/kendo-angular-grid';
import { UserQuestinsListService } from './user-questions-list.service';
import { UserQuestionsModel } from '../user-questions/user-questions.model';

@Component({
  selector: 'app-user-questions-list',
  templateUrl: './user-questions-list.component.html',
  styleUrls: ['./user-questions-list.component.scss'],
  providers:[DatePipe]
})
export class UserQuestionsListComponent implements OnInit {
  UserMasterFromGroups: FormGroup;
  @ViewChild(DataBindingDirective) dataBinding: DataBindingDirective;
  public gridData: any[];
  public gridView: any[];
  UsrName:any;
  public mySelection: string[] = [];
  UserQuesListModelobj: UserQuestionsModel = new UserQuestionsModel();
  searchText:string;
  constructor(private xUserMatsersService: UserQuestinsListService, private xRouter: Router,
    private EncrDcr: EncrDecrService,public datepipe: DatePipe
    
    ) {
      
      debugger
      if (localStorage.getItem('TOKEN') != null) {
        this.UsrName = localStorage.getItem('UserName');
      } 
      
  }

  ngOnInit() {
    
    if (localStorage.getItem('TOKEN') != null) {
      this.UsrName = localStorage.getItem('UserName');
    } 
    this.gridView = this.gridData;
    this.getUserMasterDetails();
  }


  list: any=[];

  getUserMasterDetails() {
    this.UserQuesListModelobj.Type=1;
    this.xUserMatsersService.GetUserQuestionsListDetails(this.UserQuesListModelobj)
      .subscribe(response => {
        debugger;
        console.log("User List", response);
        //let date=response[0].Um_SubscriptionDate;
        //var latest_date =this.datepipe.transform(date, 'yyyy-MM-dd');

        this.list = response[0];
        this.gridView=response[0];
      
        //this.list
        //latest_date =this.datepipe.transform(this.date, 'yyyy-MM-dd');
        console.log(response);
        debugger
    //   for (var i = 0; i < this.gridView.length; i++)
    // {
    //   if(this.gridView[i]["Um_IsPaid"]==false)
    //   {
    //     this.gridView[i]["Um_IsPaid"] = "No";
    //   }
    //   else{
    //     this.gridView[i]["Um_IsPaid"] = "Yes"; 
    //   }
    //   this.gridView[i]["Um_SubscriptionDate"]=this.datepipe.transform(this.gridView[i]["Um_SubscriptionDate"], 'yyyy-MM-dd');
    // }

  });

      }
    


  showMasterDetails($event, dataItem) {

    debugger;
    console.log("check data", dataItem.Um_PKeyId);
    let data = dataItem.Um_PKeyId;
    var encrypted = this.EncrDcr.set('123456$#@$^@1ERF', data);
    console.log(encrypted);
    //this.xRouter.navigate(["/user-master/details/", btoa(encrypted)]);

  }
  
  OndeleteRow($event, dataItem) {

    debugger;
    if(confirm("Are you sure want to delete")) {
      debugger
    console.log("check data", dataItem.User_PkeyID);
    let userId = dataItem.User_PkeyID;
    var encrypted = this.EncrDcr.set('123456$#@$^@1ERF', userId);
    console.log(encrypted);
    //this.xUserMasterListService._deleteUserMasterData(userId).subscribe(res =>{
      debugger;
//var result=res;
alert('Deleted Successfully');
this.getUserMasterDetails();
    // })
  }
  }
  
 
    // if(this.UserMasterFromGroups.valid)
  
  // validateAllFormField(formGroup: FormGroup) {
  //   Object.keys(formGroup.controls).forEach(field => {
  //     const control = formGroup.get(field);
  //     if (control instanceof FormControl) {
  //       control.markAsTouched({ onlySelf: true });
  //     } else if (control instanceof FormGroup) {
  //       this.validateAllFormField(control);
  //     }
  //   });
  // }
  checkChanges (val ,dataItem){
console.log(dataItem);

this.UserQuesListModelobj.Question_Pkey=dataItem.User_PkeyID;
//this.UserQuesListModelobj.User_IsActive=val.target.checked;
this.xUserMatsersService.updateUserMasterListData(this.UserQuesListModelobj).subscribe(res =>{
  console.log(dataItem);
  alert('Update Successfully');
this.getUserMasterDetails();
})

  }

  showDetails(dataItem) {
    debugger
    var encrypted = this.EncrDcr.set('123456$#@$^@1ERF', dataItem.Question_Pkey);
    //this.xRouter.navigate(["/home/appuser/addappuser/new", btoa(encrypted)]);
    this.xRouter.navigate(["/home/questions/add/", btoa(encrypted)]);
    ///home/appuser/addappuser/2
  }
 }
