import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
//import { AddUserModel } from './add-user-model';
//import {AddUserServices} from './add-user.service'
import { IplAppModalContent } from '../../component/iplapp-modal-content/iplapp-modal-content.component';
import { EncrDecrService } from '../../services/util/encr-decr.service';
import { UserQuestinsListService } from "../user-questions-list/user-questions-list.service";
import { UserQuestionsModel } from "./user-questions.model";
import { UserQuestionsServices } from "./user-questions.service";
//import {viewUserModel} from '../view-user/view-user-model'
//import {ViewUserService} from '../view-user/view-user.service'


@Component({
  templateUrl: "./user-questions.component.html",
  styleUrls: ['./user-questions.component.scss']
})

export class UserQuestionsComponent implements OnInit {
 
  form: FormGroup; // create obj
  submitted = false; // submitted;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  MessageFlag: string; // custom msg sathi
  ModelObj: any;
  IsEditDisable = false;
  statusList=[];
  userquesModelObj: UserQuestionsModel = new UserQuestionsModel();
  

  constructor( private formBuilder: FormBuilder,private userServices:UserQuestionsServices,
    private userqueslistServices:UserQuestinsListService,
    private xmodalService: NgbModal,private xRoute: ActivatedRoute,private xRouter: Router,
    private EncrDecr: EncrDecrService
    ) {
    this.form = this.formBuilder.group({
      Question_Title: ['', Validators.compose([Validators.nullValidator])],
      Question_Desc: ['', Validators.compose([Validators.nullValidator])],
      Question_Status: ['', Validators.compose([Validators.nullValidator])],
      Quest_Ans_ReplDesc: ['', Validators.compose([Validators.required])],
      //Question_Date: ['', Validators.compose([Validators.nullValidator])],
     
    });
    //this. getModelData();
  }

  ngOnInit() { 
    debugger
    this.getStatus();
    const id1 = this.xRoute.snapshot.params['Id'];
    if(id1 != undefined)
      {
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetsingleData();
    }
  }

  getStatus(){
    this.statusList=[{
      'id':1,'status':'Select'
    },
    {
      'id':2,'status':'Pending'
    },
    {
      'id':3,'status':'In Developement'
    },
    {
      'id':4,'status':'Closed'
    },
    {
      'id':5,'status':'Open'
    },
    ]
  }
  get fx() {
    return this.form.controls;
  }
  matchpsd: String;
  submit(){
    debugger
    if (this.form.invalid) {
      this.form.get('Quest_Ans_ReplDesc').markAsTouched();
      this.form.get('Question_Status').markAsTouched();
      return;
    }
    if (this.form.valid) {
      this.userServices.AddUserQuesDetails(this.userquesModelObj)
      .subscribe(res =>{
        if (res[0] != "0") {
          this.MessageFlag = "User Questions Saved...!";
          this.commonMessage();
          this.xRouter.navigate(["/home/questions/viewlist"]);
        }
      })
     }
    // else{
    //   this.matchpsd = "Password & Confirm Password Not Matched";
    // }
   
 

  }


  EditForms() {
    this.IsEditDisable = false;
    this.form.enable();
  }
  GetsingleData(){
    debugger
    this.userquesModelObj.Type = 2;
    this.userquesModelObj.Question_Pkey =this.ModelObj
   this.userqueslistServices.GetUserQuestionsListDetails(this.userquesModelObj)
   .subscribe(res =>{
     debugger
     console.log('editdata',res);
     this.userquesModelObj.Question_Pkey = res[0][0].Question_Pkey;
     this.userquesModelObj.Question_Title = res[0][0].Question_Title;
     this.userquesModelObj.Question_Desc = res[0][0].Question_Desc;
     this.userquesModelObj.Question_Status = res[0][0].Question_Status;
     this.userquesModelObj.Question_Date = res[0][0].Question_Date;
     if(res[0][0].quest_Answer_Master_DTO.length>0)
     {
      this.userquesModelObj.Quest_Ans_Pkey =res[0][0].quest_Answer_Master_DTO[0].Quest_Ans_Pkey
      this.userquesModelObj.Quest_Ans_ReplDesc =res[0][0].quest_Answer_Master_DTO[0].Quest_Ans_Desc
     }
    
   })
  }

  commonMessage() {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });
  }

  onStatusChanged(event) {
    debugger
    this.userquesModelObj.Question_Status =event.target.value
    this.userquesModelObj.Status= +event.target.value
  }

  // get hasDropDownError() {
  //   return (
  //     this.form.get('Question_Status').touched &&
  //     this.form.get('Question_Status').errors &&
  //     this.form.get('Question_Status').errors.required
  //   )
  // }
  
}
