import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserQuestionsComponent } from './user-questions.component';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';


export const AdduserRouts = [

  { path: '', component: UserQuestionsComponent },
  //{ path:'addques/:Id', component:UserQuestionsComponent}

]

@NgModule({
  declarations: [UserQuestionsComponent],
  imports: [
    RouterModule.forChild(AdduserRouts),
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],

  providers: [],
  bootstrap: [UserQuestionsComponent]
})

export class UserQuestionsModule { }
