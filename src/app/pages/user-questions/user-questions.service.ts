import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { BaseUrl } from '../../services/apis/rest-api';
import { ErrorhandlepageServices } from '../../services/common-handleError/commonhandleError.services';
import { environment } from "src/environments/environment";
import { UserQuestionsModel } from "./user-questions.model";



@Injectable({
  providedIn: "root"
})
export class UserQuestionsServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router,
     private xHomepageServices: ErrorhandlepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  private apiUrlPOST = BaseUrl + environment.Admin.adduserquesdetails;
  
  public AddUserQuesDetails(Modelobj: UserQuestionsModel) {
  console.log('post',Modelobj)
  var ANYDTO: any = {};
  ANYDTO.Question_Pkey = Modelobj.Question_Pkey;
  ANYDTO.Question_Title = Modelobj.Question_Title;
  ANYDTO.Question_Desc = Modelobj.Question_Desc;
  ANYDTO.Question_Status = Modelobj.Question_Status;
  ANYDTO.Question_Date = Modelobj.Question_Date;
  ANYDTO.Quest_Ans_ReplDesc= Modelobj.Quest_Ans_ReplDesc;
  ANYDTO.Quest_Ans_Pkey= Modelobj.Quest_Ans_Pkey;
  ANYDTO.Question_IsActive = true;//Modelobj.Question_IsActive;
  ANYDTO.Question_IsDelete =false; //.Question_IsDelete;
  

  ANYDTO.Type = Modelobj.Type;
  if (Modelobj.Question_Pkey != 0) {
    ANYDTO.Type = 2;
  }
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
       
      );
  }

  private apiUrldel //= BaseUrl + environment.Admin.DeleteAdminUser;
  
  // public DeleteUserDetail(Modelobj: AddUserModel) {
  // var ANYDTO: any = {};
  // ANYDTO.Ipl_Ad_User_PkeyID = Modelobj.Ipl_Ad_User_PkeyID;
  // ANYDTO.Type = Modelobj.Type;

  //   let headers = new HttpHeaders({ "Content-Type": "application/json" });
  //   headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
  //   return this._http
  //     .post<any>(this.apiUrldel, ANYDTO, { headers: headers })
  //     .pipe(
  //       tap(data => {
  //         return data;
  //       }),
  //       catchError(this.xHomepageServices.CommonhandleError)
       
  //     );
  // }
 
}
