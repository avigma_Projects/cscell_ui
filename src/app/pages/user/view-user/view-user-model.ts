export class viewUserModel{
    Ipl_Ad_User_PkeyID: Number = 0;
    Ipl_Ad_User_First_Name: String = '';
    Ipl_Ad_User_Last_Name: String = '';
    Ipl_Ad_User_Address: String = '';
    Ipl_Ad_User_City: String = '';
    Ipl_Ad_User_State: String = '';
    Ipl_Ad_User_Mobile: String = '';
    Ipl_Ad_User_Login_Name: String = '';
    Ipl_Ad_User_Password: String = '';
    cnfflag: String = '';
    Ipl_Ad_User_Email: String = '';
    Ipl_Ad_User_IsActive: boolean = true;
    Ipl_Ad_User_IsDelete: boolean = false;
    Ipl_Ad_User_UserVal: String = '';
    Type:Number = 3;
    
}