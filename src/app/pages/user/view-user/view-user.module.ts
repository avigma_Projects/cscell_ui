import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GridModule } from '@progress/kendo-angular-grid';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from '@angular/common';

import { ViewUserComponent } from './view-user.component';
import {} from '../add-user/add-user.module'

const ViewUserRouts: Routes = [
  { path: "viewuser", component: ViewUserComponent },
  { 
    path: 'adduser/:id', 
    loadChildren: () => import('../add-user/add-user.module').then(m => m.AddUserModule)
  },
  { path: '', redirectTo: 'viewuser', pathMatch: 'full'}
];

@NgModule({
  declarations: [ViewUserComponent],
  imports: [
    RouterModule.forChild(ViewUserRouts),
    GridModule,
    NgbModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule
   
  ],
  providers: [],
  bootstrap: [ViewUserComponent]
})

export class ViewUserModule { }
