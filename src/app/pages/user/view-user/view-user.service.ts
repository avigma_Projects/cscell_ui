import { Injectable } from "@angular/core";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {viewUserModel} from './view-user-model'

import { BaseUrl } from "../../../services/apis/rest-api";
import { ErrorhandlepageServices } from "../../../services/common-handleError/commonhandleError.services";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})

export class ViewUserService {

  public token: any;
  pathParam: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: ErrorhandlepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }

  private apiUrlautoGet = BaseUrl + environment.Admin.GetAdminProfileUser;

  public ViewUserData(Modelobj: viewUserModel) {
    var ANYDTO: any = {};
    ANYDTO.Type = Modelobj.Type;
    ANYDTO.Ipl_Ad_User_PkeyID = Modelobj.Ipl_Ad_User_PkeyID;

    var obj = {
      Ipl_Ad_User_First_Name: Modelobj.Ipl_Ad_User_First_Name,
      Ipl_Ad_User_Last_Name: Modelobj.Ipl_Ad_User_Last_Name,
    
    };

    ANYDTO.FilterData = JSON.stringify(obj);

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlautoGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
      );
  }

  // setPathParam(param) {
  //   this.pathParam = param;
  // }

  // getPathParam() {
  //   return this.pathParam;
  // }
}
