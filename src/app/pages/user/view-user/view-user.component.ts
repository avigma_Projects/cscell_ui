import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
import {  IplCompanyFilters } from '../../../component/iplapp-filter-form/user-filter-form';
import {viewUserModel} from './view-user-model';
import  {ViewUserService} from '../view-user/view-user.service'
import {  GridColumns } from '../constants';
import {AddUserModel} from '../add-user/add-user-model'
import { EncrDecrService } from '../../../services/util/encr-decr.service';
import {AddUserServices} from '../add-user/add-user.service'
import { from } from 'rxjs';

@Component({
  templateUrl: "./view-user.component.html"
})

export class ViewUserComponent implements OnInit {
  viewUserModelObj: viewUserModel = new viewUserModel();
  AddUserModelObj: AddUserModel = new AddUserModel();
  public griddata: any[];
  
  gridColumns = GridColumns;
  IplCompanyFilter = IplCompanyFilters;
  
  constructor(
    private xRouter: Router,
    private xViewUserService: ViewUserService,
    private EncrDecr: EncrDecrService,
    private xAddUserServices: AddUserServices
 
  
  ) {
    this.GetGridData();
  }

  ngOnInit() { }


  showDetails(event, dataItem) {
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Ipl_Ad_User_PkeyID);
    this.xRouter.navigate(["/home/user/adduser", btoa(encrypted)]);
  }
  deleteDetails(event, dataItem){
    debugger
    var cfrm=confirm("Delete Admin User Details.....!");
    if(cfrm==true)
    {

    
    this.AddUserModelObj.Ipl_Ad_User_PkeyID = dataItem.Ipl_Ad_User_PkeyID;
    this.AddUserModelObj.Ipl_Ad_User_IsDelete=true;
    this.AddUserModelObj.Type = 4;


    this.xAddUserServices.DeleteUserDetail(this.AddUserModelObj)
    .subscribe(res=>{
      console.log('delete response',res);
      if(res[0].length!=0)
      {
        this.AddUserModelObj.Ipl_Ad_User_PkeyID=0;
        this.AddUserModelObj.Type=1;



      this.GetGridData();
      }
    })

  }
}

 //get grid
 GetGridData() {
  this.xViewUserService
    .ViewUserData(this.viewUserModelObj)
    .subscribe(response => {
      console.log('griddata',response);
      this.griddata = response[0];
    });
}


filterCall() {
   
   
  }

  clearData() {
    this.viewUserModelObj = new   viewUserModel();
    
  }
  saveFilterData() {
    alert("save called");
  }

 
 

  //kendo check box event action
  public state: State = {};
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }
}
