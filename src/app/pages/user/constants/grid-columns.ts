import { Column } from '../../../models/grid-column-model';

export const GridColumns: Column[] = [
  {
    title: 'User Name',
    field: 'Ipl_Ad_User_Login_Name'
  },
  {
    title: 'First Name',
    field: 'Ipl_Ad_User_First_Name',
  },
  {
    title: 'Last Name',
    field: 'Ipl_Ad_User_Last_Name'
  },
  {
    title: 'Address',
    field: 'Ipl_Ad_User_Address'
  },
  {
    title: 'City',
    field: 'Ipl_Ad_User_City'
  },
  {
    title: 'State',
    field: 'Ipl_Ad_User_State'
  },
  {
    title: 'Mobile',
    field: 'Ipl_Ad_User_Mobile'
  },
  {
    title: 'Active',
    field: 'Ipl_Ad_User_IsActive'
  }
]
