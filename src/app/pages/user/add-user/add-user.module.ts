import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AddUserComponent } from './add-user.component';


export const AdduserRouts = [

  { path: '', component: AddUserComponent }

]

@NgModule({
  declarations: [AddUserComponent],
  imports: [
    RouterModule.forChild(AdduserRouts),
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
   
  ],

  providers: [],
  bootstrap: [AddUserComponent]
})

export class AddUserModule { }
