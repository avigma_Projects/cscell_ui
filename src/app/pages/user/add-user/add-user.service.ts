import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { BaseUrl } from '../../../services/apis/rest-api';
import { ErrorhandlepageServices } from '../../../services/common-handleError/commonhandleError.services';
import {AddUserModel} from './add-user-model'
import { environment } from "src/environments/environment";



@Injectable({
  providedIn: "root"
})
export class AddUserServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router,
     private xHomepageServices: ErrorhandlepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  private apiUrlPOST = BaseUrl + environment.Admin.PostAdminUser;
  
  public AddUserDetail(Modelobj: AddUserModel) {
  console.log('post',Modelobj)
  var ANYDTO: any = {};
  ANYDTO.Ipl_Ad_User_PkeyID = Modelobj.Ipl_Ad_User_PkeyID;
  ANYDTO.Ipl_Ad_User_First_Name = Modelobj.Ipl_Ad_User_First_Name;
  ANYDTO.Ipl_Ad_User_Last_Name = Modelobj.Ipl_Ad_User_Last_Name;
  ANYDTO.Ipl_Ad_User_Address = Modelobj.Ipl_Ad_User_Address;
  ANYDTO.Ipl_Ad_User_City = Modelobj.Ipl_Ad_User_City;
  ANYDTO.Ipl_Ad_User_State = Modelobj.Ipl_Ad_User_State;
  ANYDTO.Ipl_Ad_User_Mobile = Modelobj.Ipl_Ad_User_Mobile;
  ANYDTO.Ipl_Ad_User_Login_Name = Modelobj.Ipl_Ad_User_Login_Name;
  ANYDTO.Ipl_Ad_User_Password = Modelobj.Ipl_Ad_User_Password;
  ANYDTO.Ipl_Ad_User_Email = Modelobj.Ipl_Ad_User_Email;
  ANYDTO.Ipl_Ad_User_IsActive = Modelobj.Ipl_Ad_User_IsActive;
  ANYDTO.Ipl_Ad_User_IsDelete = Modelobj.Ipl_Ad_User_IsDelete;
  ANYDTO.Type = Modelobj.Type;
  if (Modelobj.Ipl_Ad_User_PkeyID != 0) {
    ANYDTO.Type = 2;
  }
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
       
      );
  }

  private apiUrldel //= BaseUrl + environment.Admin.DeleteAdminUser;
  
  public DeleteUserDetail(Modelobj: AddUserModel) {
  var ANYDTO: any = {};
  ANYDTO.Ipl_Ad_User_PkeyID = Modelobj.Ipl_Ad_User_PkeyID;
  ANYDTO.Type = Modelobj.Type;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrldel, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
       
      );
  }
 
}
