import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AddUserModel } from './add-user-model';
import {AddUserServices} from './add-user.service'
import { IplAppModalContent } from '../../../component/iplapp-modal-content/iplapp-modal-content.component';
import { EncrDecrService } from '../../../services/util/encr-decr.service';
import {viewUserModel} from '../view-user/view-user-model'
import {ViewUserService} from '../view-user/view-user.service'


@Component({
  templateUrl: "./add-user.component.html"
})

export class AddUserComponent implements OnInit {
 
  form: FormGroup; // create obj
  submitted = false; // submitted;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  MessageFlag: string; // custom msg sathi
  ModelObj: any;
  IsEditDisable = false;
  AddUserModelObj: AddUserModel = new AddUserModel();
  viewUserModelObj: viewUserModel = new viewUserModel();

  constructor( private formBuilder: FormBuilder,private xAddUserServices:AddUserServices,
    private xmodalService: NgbModal,private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrService, private xViewUserService:ViewUserService,
    ) {
    this.form = this.formBuilder.group({
      firstnameval: ['', Validators.compose([Validators.nullValidator])],
      lastnameval: ['', Validators.compose([Validators.nullValidator])],
      usernameval: ['', Validators.compose([Validators.nullValidator])],
      emialval: ['', Validators.compose([Validators.nullValidator])],
      cityval: ['', Validators.compose([Validators.nullValidator])],
      stateval: ['', Validators.compose([Validators.nullValidator])],
      psdval: ['', Validators.compose([Validators.nullValidator])],
      cnfpsdval: ['', Validators.compose([Validators.nullValidator])],
      mobileval: ['', Validators.compose([Validators.nullValidator])],
      addressval: ['', Validators.compose([Validators.nullValidator])],
      activeval: ['', Validators.compose([Validators.nullValidator])],
    
    });
    this. getModelData();
  }

  ngOnInit() { 
 
  }
  get fx() {
    return this.form.controls;
  }
  matchpsd: String;
  submit(){
    debugger
    if (this.AddUserModelObj.Ipl_Ad_User_Password == this.AddUserModelObj.cnfflag ) {
      this.xAddUserServices.AddUserDetail(this.AddUserModelObj)
      .subscribe(res =>{
        if (res[0] != "0") {
          this.MessageFlag = "User Detail Saved...!";
          this.commonMessage();
        }
      })
    }
    else{
      this.matchpsd = "Password & Confirm Password Not Matched";
    }
   
 

  }

  getModelData() {
    debugger
    
    const id1 = this.xRoute.snapshot.params['id'];
    if (id1 == 'new') {
      this.AddUserModelObj = new AddUserModel();
    } else {
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    
      this.ModelObj = parseInt(id);
    }

    if (this.ModelObj == undefined) {
     this.AddUserModelObj = new AddUserModel();

      this.submitted = false; 
      this.button = "Save"; 
      this.isLoading = false; 
    } else {
      console.log("grid to f", this.ModelObj);
      this.AddUserModelObj.Type = this.ModelObj.Type;
      this.AddUserModelObj.Ipl_Ad_User_PkeyID = this.ModelObj;
      this.viewUserModelObj.Type = this.ModelObj.Type;
      this.viewUserModelObj.Ipl_Ad_User_PkeyID = this.ModelObj;
     

      this.GetsingleData();

     // this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;
      this.button = "Update";
      this.AddUserModelObj.Type = 4;
    }
  }

  EditForms() {
    this.IsEditDisable = false;
    this.form.enable();
  }
  GetsingleData(){
    debugger
    this.viewUserModelObj.Type = 4;
   this.xViewUserService.ViewUserData(this.viewUserModelObj)
   .subscribe(res =>{
     console.log('editdata',res);
     this.AddUserModelObj.Ipl_Ad_User_PkeyID = res[0][0].Ipl_Ad_User_PkeyID;
     this.AddUserModelObj.Ipl_Ad_User_First_Name = res[0][0].Ipl_Ad_User_First_Name;
     this.AddUserModelObj.Ipl_Ad_User_Last_Name = res[0][0].Ipl_Ad_User_Last_Name;
     this.AddUserModelObj.Ipl_Ad_User_Login_Name = res[0][0].Ipl_Ad_User_Login_Name;
     this.AddUserModelObj.Ipl_Ad_User_Mobile = res[0][0].Ipl_Ad_User_Mobile;
     this.AddUserModelObj.Ipl_Ad_User_Password = res[0][0].Ipl_Ad_User_Password;
     this.AddUserModelObj.cnfflag = res[0][0].Ipl_Ad_User_Password;
     this.AddUserModelObj.Ipl_Ad_User_City = res[0][0].Ipl_Ad_User_City;
     this.AddUserModelObj.Ipl_Ad_User_State = res[0][0].Ipl_Ad_User_State;
     this.AddUserModelObj.Ipl_Ad_User_Email = res[0][0].Ipl_Ad_User_Email;
     this.AddUserModelObj.Ipl_Ad_User_Address = res[0][0].Ipl_Ad_User_Address;
   
   })
  }

  commonMessage() {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });
  }
  
}
