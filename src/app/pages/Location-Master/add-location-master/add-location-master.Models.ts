export class AddLocationMasterModel{
    Loc_PkeyID:number=0;
    Loc_Name:string='';
    Loc_Description:string='';
    Loc_latitude:string='';
    Loc_longitude:string='';
    Loc_Zip:string='';
    Loc_IsActive:boolean=false;
    Loc_IsDelete:boolean=false;
    UserID: Number = 0;
    Type:number=1;
}