import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';

import { AddLocationMasterModel } from './add-location-master.Models';
import { AddLocationMasterService} from './add-location-master.Service';
import { FormBuilder } from '@angular/forms';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewLocationMasterModel } from '../view-location-master/view-location-master.Models';
import { ViewLocationMasterService} from '../view-location-master/view-location-master.Service';

@Component({
  selector: 'app-add-location-master',
  templateUrl: './add-location-master.component.html',
  styleUrls: ['./add-location-master.component.scss']
})
export class AddLocationMasterComponent implements OnInit {

  ViewLocationMasterModelObj:ViewLocationMasterModel = new ViewLocationMasterModel();
  AddLocationMasterModelObj: AddLocationMasterModel = new AddLocationMasterModel();
  
  ModelObj: number;
  formUsrCommonGroup:FormGroup;
  public myFiles:Array<File> = [];
  RecordValList: any;
  GroupList: any;
  griddata: any;
  YESNOList: any;
  ProductActivelist:any;
  MessageFlag: string;
  submitted = false; // submitted;
  docdisable = true;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  IsEditDisable: boolean;
  isReadOnly: boolean;
  
  
  
  
  constructor(private xRouter:Router,
    private xRoute:ActivatedRoute,
    private xmodalService: NgbModal,
    private EncrDecr:EncrDecrService,
    private xaddlocationservices:AddLocationMasterService,
    private xviewlocationservices:ViewLocationMasterService,
    private formBuilder:FormBuilder

  ) 
  
  
  { }

  ngOnInit() {
    debugger
    const id1 = this.xRoute.snapshot.params['Id'];
    if (id1 == 'new' || id1 =='undefined')
     {
      this.AddLocationMasterModelObj = new AddLocationMasterModel();
    } else 
    {
      if(id1 != undefined)
      {
        
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
    }
      // this.commonMessage();
    }

    this.formUsrCommonGroup = this.formBuilder.group({
      Loc_Name:["", Validators.required],
      Loc_Description:["",Validators.required],
     
     
      Loc_Zip:["",Validators.required],
     
     
      

      imageUpload: [''],
    
    });  


  }

  getModelData(){
    
    debugger
    const id1 = this.xRoute.snapshot.params['id'];
    if (id1 == 'new') {
      this.ViewLocationMasterModelObj = new ViewLocationMasterModel();
    } else {
      debugger
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      debugger
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
    }


  }

  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  EditForms() {
    // this.IsEditDisable = false;
    // this.formUsrCommonGroup.enable();
  }

  commonMessage() {
   
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
      modalRef.componentInstance.MessageFlag = this.MessageFlag;
      modalRef.result.then(result => { }, reason => { });
 
    
  
  }

  GetSingleData(){
    debugger
    console.log("editres");
    this.ViewLocationMasterModelObj.Loc_PkeyID =this.ModelObj;
    this.ViewLocationMasterModelObj.Type = 2 ;
    this.xviewlocationservices.ViewLocationData(this.ViewLocationMasterModelObj).subscribe(res =>{
      console.log('edit',res)
      debugger
this.AddLocationMasterModelObj.Loc_PkeyID = res[0][0].Loc_PkeyID;
this.AddLocationMasterModelObj.Loc_Name = res[0][0].Loc_Name;
this.AddLocationMasterModelObj.Loc_Description = res[0][0].Loc_Description;
this.AddLocationMasterModelObj.Loc_latitude = res[0][0].Loc_latitude;
this.AddLocationMasterModelObj.Loc_longitude = res[0][0].Loc_longitude;
this.AddLocationMasterModelObj.Loc_Zip = res[0][0].Loc_Zip;


this.AddLocationMasterModelObj.Loc_IsActive = res[0][0].Loc_IsActive;
this.AddLocationMasterModelObj.Type=2;
if(this.AddLocationMasterModelObj.Loc_IsActive==false){
  this.AddLocationMasterModelObj.Loc_IsActive=false;
 }
 else{
  this.AddLocationMasterModelObj.Loc_IsActive=true;
 }

})
}
onIsActiveChanged(val){
  debugger
  if(val.target.value=='1: false'){
    this.AddLocationMasterModelObj.Loc_IsActive=false;
    }
    else{
      this.AddLocationMasterModelObj.Loc_IsActive=true;
    }

}
formButton(){
  debugger
 // .fromButton(this.AddAppproductModelObj)
  console.log('responese');
  // if (this.myimgaes.length > 0)
  // {
  //   this.AddTryeModelObj.Pro_Images = JSON.stringify(this.myimgaes)
  // } 
  if(this.ModelObj>0)
  {
    this.AddLocationMasterModelObj.Loc_PkeyID=this.ModelObj;
    this.AddLocationMasterModelObj.Type=2;
  }
  else{
    this.AddLocationMasterModelObj.Type=1;
  }
  if(this.formUsrCommonGroup.valid){
    debugger
    console.log('check',this.AddLocationMasterModelObj)
  this.xaddlocationservices.LocationMasterData(this.AddLocationMasterModelObj)
  .subscribe(respose =>{
    
    alert("location Details Saved Sucessfully");
    console.log('res',respose)
    this.xRouter.navigate(["/home/locationmaster/viewlocationmaster"]);
 

  })
  }
  else{
    alert("All Field are Required")
  }
     

}

}
