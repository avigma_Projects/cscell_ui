import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddLocationMasterComponent } from './add-location-master.component';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import{NgMultiSelectDropDownModule}from 'ng-multiselect-dropdown'
import { ReactiveFormsModule } from '@angular/forms';
import {AddLocationMasterService} from './add-location-master.Service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
export const appproductRoutes=[
  { path:'addnewlocationmaster', component:AddLocationMasterComponent},
  { path:'addnewlocationmaster/:Id', component:AddLocationMasterComponent}
  

 ]
@NgModule({
  declarations: [AddLocationMasterComponent],
  imports: [RouterModule.forChild(appproductRoutes),
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule, 
    CommonModule,
    DropDownsModule
  ],
  providers: [AddLocationMasterService],
  bootstrap: [AddLocationMasterComponent]
})
export class AddLocationModule { }
