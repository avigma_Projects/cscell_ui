import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { BaseUrl } from 'src/app/services/apis/rest-api';
import { ErrorhandlepageServices } from 'src/app/services/common-handleError/commonhandleError.services';
import { environment } from 'src/environments/environment';
import {  ViewLocationMasterModel } from '../view-location-master/view-location-master.Models';
import {AddLocationMasterModel } from './add-location-master.Models';


@Injectable({
  providedIn: 'root'
})
export class AddLocationMasterService {
  baseurl: string;
 
   open(IplAppModalContent: IplAppModalContent, arg1: { size: string; ariaLabelledBy: string; }) {
    throw new Error('Method not implemented.');
  }
  public token:any;

  constructor(private _http: HttpClient,
    private _Route: Router,
   
    private xHomepageServices: ErrorhandlepageServices) { }
   

  
  private apiUrlPOST = BaseUrl + environment.Admin.AddLocationMasterDetails;
  public LocationMasterData(Modelobj: AddLocationMasterModel) {
    var ANYDTO: any = {};
    ANYDTO.Loc_PkeyID = Modelobj.Loc_PkeyID;
    ANYDTO.Loc_Name = Modelobj.Loc_Name;
    ANYDTO.Loc_Description = Modelobj.Loc_Description;
    ANYDTO.Loc_latitude = Modelobj.Loc_latitude;
    ANYDTO.Loc_longitude = Modelobj.Loc_longitude;
    ANYDTO.Loc_Zip = Modelobj.Loc_Zip;
   
   
    ANYDTO.Loc_IsActive = Modelobj.Loc_IsActive;
    ANYDTO.Loc_IsDelete = Modelobj.Loc_IsDelete;
    ANYDTO.UserID = Modelobj.UserID;
    
    

    ANYDTO.Type=Modelobj.Type;
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.CommonhandleError)
      );
  
  }
  
  public CommonhandleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User Found...!');
      // window.location.href = '/';
    } else {
      alert("Something bad happened, please try again later...😌");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }  
}