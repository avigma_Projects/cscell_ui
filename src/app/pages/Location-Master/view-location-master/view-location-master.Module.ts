import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ViewLocationMasterComponent } from './view-location-master.component';
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ViewLocationMasterService } from './view-location-master.Service';


export const ViewAppproductRouts: Routes = [
  {  path: 'viewlocationmaster', component:ViewLocationMasterComponent},
  

    // { 
    //  path: 'add/:id',
    //  loadChildren: () => import('../add-app-products/add-app-products.module').then(m => m.AddAppProductsModule ),
    //  },
  { path: '', redirectTo: 'home', pathMatch: 'full'}
]



@NgModule({
  declarations: [ViewLocationMasterComponent],
  imports: [ RouterModule.forChild(ViewAppproductRouts),
     
    GridModule, ExcelModule,
    FormsModule, ReactiveFormsModule,
    CommonModule
  ],
  providers: [ViewLocationMasterService],
  bootstrap: [ViewLocationMasterComponent]
})
export class ViewLocationModule { }
