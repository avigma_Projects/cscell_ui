import { Component, OnInit } from '@angular/core';

import { State } from "@progress/kendo-data-query";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import{ViewLocationMasterModel}from './view-location-master.Models'
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { Router, Routes } from '@angular/router';
import { ViewLocationMasterService  } from './view-location-master.Service';

import { AddLocationMasterModel } from '../add-location-master/add-location-master.Models';
import {AddLocationMasterService  } from '../add-location-master/add-location-master.Service';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-view-location-master',
  templateUrl: './view-location-master.component.html',
  styleUrls: ['./view-location-master.component.scss']
})
export class ViewLocationMasterComponent implements OnInit {

  ViewLocationMasterModelObj:ViewLocationMasterModel = new ViewLocationMasterModel();
  AddLocationMasterModelObj: AddLocationMasterModel = new AddLocationMasterModel();
  public griddata: any;
  formUsrCommonGroup:FormGroup;

  GroupList: any;
  //buttons = Buttons;
  //gridColumns = GridColumnsproduct;
  public state: State = {};
  MessageFlag: string;
  
  
  
  constructor(private xaddlocationservices:AddLocationMasterService,
    private xviewlocationservices:ViewLocationMasterService,
    private xRouter:Router,
    private EncrDecr: EncrDecrService,
    private xmodalService: NgbModal,

  ) 
  
  
  { 

  }

  ngOnInit(): void {
    debugger
    this.getlocationproduct()
  }

  getlocationproduct() 
  {
    debugger
    this.ViewLocationMasterModelObj.Type=1;
    console.log("response");
   this.xviewlocationservices.ViewLocationData(this.ViewLocationMasterModelObj).subscribe((res)=>{
     debugger
      console.log('nikhil',res);
    this.griddata = res[0];
    })
    var rec=this.xviewlocationservices.ViewLocationData;
    console.log(rec);
    // console.log(rec);
    

  }

  showDetails(event,dataItem)
  {
    debugger
    var encrypted=this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Loc_PkeyID);
    this.xRouter.navigate(["/home/addlocationmaster/addnewlocationmaster", btoa(encrypted)]);
  
  
  }
  deleteDetails(event,dataItem) {
    debugger

    var cfrm = confirm("Delete this Record...!");

    if (cfrm == true) {
      this.AddLocationMasterModelObj.Loc_PkeyID=dataItem.Loc_PkeyID;
      this.AddLocationMasterModelObj.Loc_IsActive=false;
      this.AddLocationMasterModelObj.Loc_IsDelete=true;
      this.AddLocationMasterModelObj.Type= 4;

      debugger
      this.xaddlocationservices.LocationMasterData(this.AddLocationMasterModelObj)
        .subscribe(response => {

          debugger
          this.MessageFlag = "Data Deleted Succesfully";
          this.commonMessage();
          console.log('delete response', response);
          this.getlocationproduct();
           
        });
     }
  
  
  }


  AddNewUser()
  {
 
 
 }
 commonMessage() {
   const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
   modalRef.componentInstance.MessageFlag = this.MessageFlag;
   modalRef.result.then(result => { }, reason => { });

 }
 checkChange(event, dataItem)
 {
   debugger
   this.AddLocationMasterModelObj.Loc_PkeyID = dataItem.Loc_PkeyID;
   this.AddLocationMasterModelObj.Loc_IsActive =event;//dataItem.HP_IsActive;
   this.AddLocationMasterModelObj.Type = 5;

   this.xaddlocationservices.LocationMasterData(this.AddLocationMasterModelObj)
   .subscribe(response => {
     this.MessageFlag = "location status upated...!";
     this.commonMessage();
       this.getlocationproduct();
     });    
 
 
 }
 public dataStateChange(state: DataStateChangeEvent): void {
   this.state = state;
 }



}
