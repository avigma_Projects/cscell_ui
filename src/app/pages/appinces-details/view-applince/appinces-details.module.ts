import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbDateAdapter, NgbDateNativeAdapter, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {AppincesDetailsComponent} from '../view-applince/appinces-details.component'
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { CommonModule } from "@angular/common";
import {AddApplincesServices} from '../view-applince/appinces-detail.service'
import { GridModule } from '@progress/kendo-angular-grid';


const AppRouts = [
  { path: "details", component: AppincesDetailsComponent },
  { 
    path: 'addapplince/:id', 
    loadChildren: () => import('../add-applince/add-applinces-detailsmodule').then(m => m.AddApplincesModule), data: { breadcrumb: 'Appliances' }
  },
  { path: '', redirectTo: 'details', pathMatch: 'full'}
];

@NgModule({
  declarations: [AppincesDetailsComponent],
  imports: [
    RouterModule.forChild(AppRouts),
    HttpClientModule, NgbModule,FormsModule,ReactiveFormsModule, DropDownsModule,CommonModule,
    GridModule,
  
    
  ],
  providers: [AddApplincesServices,{
    provide: NgbDateAdapter,
    useClass: NgbDateNativeAdapter
  }],
  bootstrap: [AppincesDetailsComponent]
})

export class ViewApplincesModule { }
