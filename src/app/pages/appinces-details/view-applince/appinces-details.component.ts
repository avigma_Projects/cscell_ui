import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import {AddApplincesServices} from '../view-applince/appinces-detail.service'
import {GridColumns} from '../constants/grid-columns';
import { AddApplincesModel } from './appinces-details.model';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { State } from '@progress/kendo-data-query';
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
@Component({
  selector: 'app-appinces-details',
  templateUrl: './appinces-details.component.html',
  styleUrls: ['./appinces-details.component.scss']
})
export class AppincesDetailsComponent implements OnInit {
  gridColumns = GridColumns;
  MessageFlag: string; 
  public state: State = {};
  AddApplincesModelObj:AddApplincesModel = new AddApplincesModel();
  public griddata: any[];
  constructor( private xRouter: Router,
    private xmodalService: NgbModal,
    private EncrDecr: EncrDecrService,
    private xAddApplincesServices : AddApplincesServices) {
      this.GetGridData();
   }

  ngOnInit(): void {
  }
  GetGridData() {
    debugger
    this.xAddApplincesServices
      .GetApplincesDetails(this.AddApplincesModelObj)
      .subscribe(response => {
        console.log('grid',response);
        this.griddata = response[0];
      });
  }
  showDetails(event, dataItem) {
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.App_pkeyId);
    this.xRouter.navigate(["/home/applinces/addapplince/", btoa(encrypted)]);
  }
  checkChange(event, dataItem) {

    this.AddApplincesModelObj.App_pkeyId = dataItem.App_pkeyId;
    this.AddApplincesModelObj.App_IsActive = !dataItem.App_IsActive;
    this.AddApplincesModelObj.Type = 3;

    this.xAddApplincesServices
    .PostApplianceData(this.AddApplincesModelObj)
    .subscribe(response => {
      this.MessageFlag = "Appliance upated...!";
      this.commonMessage();
      
      });    
  }

  commonMessage() {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });
  }
  //kendo check box event action
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }
}
