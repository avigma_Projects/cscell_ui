import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { BaseUrl } from '../../../services/apis/rest-api';
import { ErrorhandlepageServices } from '../../../services/common-handleError/commonhandleError.services';
import { environment } from 'src/environments/environment';
import { AddApplincesModel } from "./appinces-details.model";



@Injectable({
  providedIn: "root"
})
export class AddApplincesServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: ErrorhandlepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // post data
  private apiUrlPOST = BaseUrl + environment.Admin.GetApplincesName;
  public GetApplincesDetails(Modelobj:AddApplincesModel ) {

    var ANYDTO: any = {};
    ANYDTO.App_pkeyId = Modelobj.App_pkeyId;
    ANYDTO.Type = Modelobj.Type;
   

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
          
        }),
        catchError(this.xHomepageServices.CommonhandleError)
      );
  }
  private apiUrlPOS = BaseUrl + environment.Admin.PostApplincesName;
  public PostApplianceData(Modelobj:AddApplincesModel ) {

    var ANYDTO: any = {};
    ANYDTO.App_pkeyId = Modelobj.App_pkeyId;
    ANYDTO.App_Apliance_Name = Modelobj.App_Apliance_Name;
    ANYDTO.App_IsActive = Modelobj.App_IsActive;
    ANYDTO.App_IsDelete = Modelobj.App_IsDelete;
    ANYDTO.Type = Modelobj.Type;
   

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOS, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
       
      );
  }



}
