import { Column } from '../../../models/grid-column-model';

export const GridColumns: Column[] = [
  {
    title: 'Appliance Name',
    field: 'App_Apliance_Name'
  },
 
  // {
  //   title: 'Active',
  //   field: 'App_IsActive'
  // }
]
