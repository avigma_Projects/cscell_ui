import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import {AddApplincesServices} from '../view-applince/appinces-detail.service'
import {GridColumns} from '../constants/grid-columns';
import { AddApplincesModel } from '../view-applince/appinces-details.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({

  templateUrl: './add-applinces-details.component.html',
  styleUrls: ['./add-applinces-details.component.scss']
})
export class AddAppincesDetailsComponent implements OnInit {

  AddApplincesModelObj:AddApplincesModel = new AddApplincesModel();
  formUsrCommonGroup: FormGroup;
  submitted = false; 
  ModelObj: any;
  button = "Save"; 
  isLoading = false; 
  MessageFlag: string; 
  IsEditDisable = false;
  constructor( private xRouter: Router,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrService,
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xAddApplincesServices : AddApplincesServices) {
      this.getModelData();
   }

   ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      Nameval: ["", Validators.required],
      activeval: ["", Validators.nullValidator],
    });
  }
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  FormButton() {
    debugger
      this.submitted = true;

      if (this.formUsrCommonGroup.invalid) {
        return;
      }
      this.isLoading = true;
      this.button = "Processing";
  
      this.xAddApplincesServices.PostApplianceData(this.AddApplincesModelObj).subscribe(res =>{
       
        this.MessageFlag = 'Appliance Details Saved...';
       this.commonMessage()
       this.isLoading = false;
       this.button = "Update";
      })
     
     }

     getModelData() {
      debugger
      
      const id1 = this.xRoute.snapshot.params['id'];
      if (id1 == 'new') {
        this.AddApplincesModelObj = new AddApplincesModel();
      } else {
        let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      
        this.ModelObj = parseInt(id);
      }
  
      if (this.ModelObj == undefined) {
        this.AddApplincesModelObj = new AddApplincesModel();
  
        this.submitted = false; 
        this.button = "Save"; 
        this.isLoading = false; 
      } else {
        console.log("grid to f", this.ModelObj);
        this.AddApplincesModelObj.Type = 2;
        this.AddApplincesModelObj.App_pkeyId = this.ModelObj;
       
     
        this.GetsingleData();
      
      // this.formUsrCommonGroup.disable();
      
        this.IsEditDisable = true;
        this.button = "Update";
       
      }
    }

  commonMessage() {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });
  }
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }

  GetsingleData(){
    debugger
    this.xAddApplincesServices
    .GetApplincesDetails(this.AddApplincesModelObj)
   .subscribe(res =>{
    console.log(res)
     this.AddApplincesModelObj.App_pkeyId = res[0][0].App_pkeyId;
     this.AddApplincesModelObj.App_Apliance_Name = res[0][0].App_Apliance_Name;
     this.AddApplincesModelObj.App_IsActive = res[0][0].App_IsActive;
   
  })


}

}
