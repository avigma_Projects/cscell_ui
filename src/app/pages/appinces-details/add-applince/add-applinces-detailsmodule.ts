import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbDateAdapter, NgbDateNativeAdapter, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {AddAppincesDetailsComponent} from './add-applinces-details.component'
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { CommonModule } from "@angular/common";
import {AddApplincesServices} from '../view-applince/appinces-detail.service'



const AppliRouts = [
  { path: "", component: AddAppincesDetailsComponent },
  
];

@NgModule({
  declarations: [AddAppincesDetailsComponent],
  imports: [
    RouterModule.forChild(AppliRouts),
    HttpClientModule, NgbModule,FormsModule,ReactiveFormsModule, DropDownsModule,CommonModule,

  
    
  ],
  providers: [AddApplincesServices,{
    provide: NgbDateAdapter,
    useClass: NgbDateNativeAdapter
  }],
  bootstrap: [AddAppincesDetailsComponent]
})

export class AddApplincesModule { }
