import { Column } from '../../../models/grid-column-model';

export const ProdBinGridColumnsproduct: Column[] = [
     {
        title: 'BrandName',
       field: 'Pro_BrandName'
      },
      {
        title: 'Gender',
       field: 'Gender'
      },
      {
        title: 'Color',
        field: 'Pro_Color',
      },
   
      {
        title: 'Category',
       field: 'Cat_Name'
      },
      {
        title: 'SubCategory',
       field: 'SubCat_Name'
      },
     

]
