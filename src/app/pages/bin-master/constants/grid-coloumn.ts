import { Column } from '../../../models/grid-column-model';

export const GridColumnsBinMaster: Column[] = [
     {
        title: 'Bin Name',
       field: 'Bin_Name'
      },
      {
        title: 'Quantity',
       field: 'Bin_Quantity'
      },
      {
        title: 'Unit',
        field: 'Bin_Unit',
      },
      {
        title: 'Bin Shelf',
       field: 'Bin_Shelf'
      },
      {
        title: 'Location',
       field: 'Loc_Name'
      },
      {
        title: 'Description',
       field: 'Bin_Description'
      },
      // {
      //   title: 'Bin Image',
      //  field: 'Bin_Image_Path'
      // },
     
      // {
      //   title: 'Bin QR Image',
      //  field: 'Bin_QR_Path'
      // },
      

]
