import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BinMasterComponent } from './bin-master.component';
import { BinMasterService } from './bin-master.service';


export const ViewAppproductRouts: Routes = [
  {  path: 'viewbinmaster', component:BinMasterComponent},

    // { 
    //  path: 'add/:id',
    //  loadChildren: () => import('../add-app-products/add-app-products.module').then(m => m.AddAppProductsModule ),
    //  },
  { path: '', redirectTo: 'home', pathMatch: 'full'}
]



@NgModule({
  declarations: [BinMasterComponent],
  imports: [ RouterModule.forChild(ViewAppproductRouts),
     
    GridModule, ExcelModule,
    FormsModule, ReactiveFormsModule,
    CommonModule
  ],
  providers: [BinMasterService],
  bootstrap: [BinMasterComponent]
})
export class BinMasterModule { }
