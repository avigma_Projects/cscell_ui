export class BinMastermodel
{
    Bin_PkeyID:number=0;
    Pro_Bin_PkeyID:number=0;
    Bin_Name:string='';
   
    Bin_Quantity:number=0;
    
    Bin_Description:string='';
    Bin_PkeyID_Owner:number=0;
    Bin_Loc_ID:number=0;
    Bin_Image_Path:string='';
    Bin_QR_Path:string='';
    Bin_QR_PrintPath:string='';
    Bin_Unit:string;
    Bin_Shelf:string='';
    Loc_Name:string='';
    Type:Number=1;
}