import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';;
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { BinMasterService } from '../bin-master.service';
import { BinMasterDetailsComponent } from './bin-master-details.component';
import { ExcelModule, GridModule } from '@progress/kendo-angular-grid';
export const appproductRoutes=[
  { path:'bin-master-details', component:BinMasterDetailsComponent},
  { path:'bin-master-details/:Id', component:BinMasterDetailsComponent}
  

 ]
@NgModule({
  declarations: [BinMasterDetailsComponent],
  imports: [RouterModule.forChild(appproductRoutes),
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule, 
    CommonModule,
    DropDownsModule,
    GridModule,
    ExcelModule
  ],
  providers: [BinMasterService],
  bootstrap: [BinMasterDetailsComponent]
})
export class BinMasterDetailsModule { }
