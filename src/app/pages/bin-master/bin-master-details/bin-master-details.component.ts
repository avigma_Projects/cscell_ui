import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import { FormBuilder } from '@angular/forms';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BinMastermodel } from '../bin-master.model';
import { BinMasterService } from '../bin-master.service';
import { addappproductmodel } from '../../app-products/add-app-products/add-app-product.model';
import { GridColumnsproduct } from '../../app-products/view-app-products/constants';

@Component({
  selector: 'app-bin-master-details',
  templateUrl: './bin-master-details.component.html',
  styleUrls: ['./bin-master-details.component.scss']
})
export class BinMasterDetailsComponent implements OnInit {
  binmasterModelObj: BinMastermodel = new BinMastermodel();
 promasterModelObj: addappproductmodel = new addappproductmodel();
  formUsrCommonGroup:FormGroup;
  ModelObj:any;
  MessageFlag: string;
  submitted = false; 
  button = "Save"; 
  isLoading = false; 
 isReadOnly:boolean;
 printurl:string;
 public griddata: any;
 gridColumns = GridColumnsproduct;
  constructor(
    private xRouter:Router,
    private xRoute:ActivatedRoute,
    private xmodalService: NgbModal,
    private EncrDecr:EncrDecrService,
    private xbinMaster_services:BinMasterService,
    private formBuilder:FormBuilder
  ) {
    
  }

  ngOnInit()  
   {
     
    debugger
    const id1 = this.xRoute.snapshot.params['Id'];
    if (id1 == 'new' || id1 =='undefined')
     {
      this.binmasterModelObj = new BinMastermodel();
      this.promasterModelObj= new addappproductmodel();
    } else 
    {
      if(id1 != undefined)
      {
        this.isReadOnly=true;
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
    }
      // this.commonMessage();
    }
    this.formUsrCommonGroup = this.formBuilder.group({
      Bin_Name:["",Validators.required],
      Bin_Quantity:["",Validators.required],
      Bin_Description:["",Validators.required],
      Bin_Image_Path:["",Validators.required],
      Bin_QR_Path:["",Validators.required],
      Bin_Unit:["",Validators.required],
      Bin_Shelf:["",Validators.required],
      Loc_Name:["",Validators.required],
    });  
    
    
    
  
  }
 
  
  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  EditForms() {
    // this.IsEditDisable = false;
    // this.formUsrCommonGroup.enable();
  }
  commonMessage() {
   
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
      modalRef.componentInstance.MessageFlag = this.MessageFlag;
      modalRef.result.then(result => { }, reason => { });
 
    
  
  }





  GetSingleData(){
    debugger
    console.log("editres");
    this.binmasterModelObj.Bin_PkeyID =this.ModelObj;
    this.binmasterModelObj.Type = 2 ;
    this.xbinMaster_services.getbinMasterData(this.binmasterModelObj).subscribe(res =>{
      console.log('edit',res)
      debugger
this.binmasterModelObj.Bin_PkeyID = res[0][0].Bin_PkeyID;
this.binmasterModelObj.Bin_Name = res[0][0].Bin_Name;
this.binmasterModelObj.Bin_Quantity = res[0][0].Bin_Quantity;
this.binmasterModelObj.Bin_Description = res[0][0].Bin_Description;
this.binmasterModelObj.Bin_PkeyID_Owner = res[0][0].Bin_PkeyID_Owner;
this.binmasterModelObj.Bin_Loc_ID = res[0][0].Bin_Loc_ID;
this.binmasterModelObj.Bin_Image_Path = res[0][0].Bin_Image_Path;
this.binmasterModelObj.Bin_QR_Path = res[0][0].Bin_QR_Path;
this.binmasterModelObj.Bin_Unit = res[0][0].Bin_Unit;
this.binmasterModelObj.Bin_Shelf = res[0][0].Bin_Shelf;
//this.binmasterModelObj.Bin_Shelf = res[0][0].Pro_Bin_Shelf;
this.binmasterModelObj.Loc_Name = res[0][0].Loc_Name;
this.binmasterModelObj.Bin_QR_PrintPath = res[0][0].Bin_QR_PrintPath;
this.printurl=this.binmasterModelObj.Bin_QR_PrintPath;
this.binmasterModelObj.Type=2;
});
}
GetprodmasterDatabyId(){
  debugger
  console.log("editres");
  this.binmasterModelObj.Pro_Bin_PkeyID =this.ModelObj;
  this.binmasterModelObj.Type = 3 ;
  this.xbinMaster_services.getProductMasterDataById(this.binmasterModelObj).subscribe(res =>{
    console.log('edit',res)
    debugger
    this.griddata = res[0];
// this.promasterModelObj.Bin_PkeyID = res[0][0].Bin_PkeyID;
// this.promasterModelObj.Bin_Name = res[0][0].Bin_Name;
// this.promasterModelObj.Pro_BrandName = res[0][0].Pro_BrandName;
// this.promasterModelObj.Pro_Bin_Shelf = res[0][0].Pro_Bin_Shelf;
// this.promasterModelObj.Pro_Bin_Unit = res[0][0].Pro_Bin_Unit;
// this.promasterModelObj.Pro_Category = res[0][0].Pro_Category;
// //this.promasterModelObj.Pro_Celeb_Name = res[0][0].Pro_Celeb_Name;
// this.promasterModelObj.Pro_Color = res[0][0].Pro_Color;
// this.promasterModelObj.Pro_Condition = res[0][0].Pro_Condition;
// this.promasterModelObj.Pro_Desc = res[0][0].Pro_Desc;
// this.promasterModelObj.Pro_Detail = res[0][0].Pro_Detail;
// this.promasterModelObj.Pro_Gender = res[0][0].Pro_Gender;
// this.promasterModelObj.ProImage_ImagePath = res[0][0].ProImage_ImagePath;
// //this.promasterModelObj.Gender = res[0][0].Gender;
// this.promasterModelObj.Pro_Name = res[0][0].Pro_Name;
// //this.promasterModelObj.Pro_Price = res[0][0].Pro_Price;
// this.promasterModelObj.Pro_Size = res[0][0].Pro_Size;
// this.promasterModelObj.Pro_Vendor_Name = res[0][0].Pro_Vendor_Name;
//this.binmasterModelObj.Type=2;
});
}
openXl(content) {
  
  this.xmodalService.open(content, { size: 'xl' });
  this.GetprodmasterDatabyId();
}
//this.modalReference = this.modalService.open(ModelComponentName, {backdrop: 'static',size: 'lg', keyboard: false, centered: true});
downloadImg(event){
  debugger
  const printContents = document.getElementById('print-index-invoice').innerHTML;
  const originalContents = document.body.innerHTML;
  document.body.innerHTML = printContents;
  window.print();
  document.body.innerHTML = originalContents;
}

DownloadProfilePic(url) { ;
  debugger;
 // var url = 'http://cssellapi.ikaart.org//Images/QR/2d7597e4-6d8d-414c-9dde-24c8e32ff27c.Jpeg';
  window.open(this.printurl);
}

 


}



