import { Component, OnInit } from '@angular/core';
import { State } from "@progress/kendo-data-query";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { AddAppProductsComponent } from '../add-app-products/add-app-products.component';
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { Router, Routes } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { BinMastermodel } from './bin-master.model';
import { BinMasterService } from './bin-master.service';
import { Buttons } from '../app-user/view-app-user/constants';
import { GridColumnsBinMaster } from './constants/grid-coloumn';
// import { Observable } from 'rxjs/observable';
@Component({
  selector: 'app-bin-master',
  templateUrl: './bin-master.component.html',
  styleUrls: ['./bin-master.component.scss']
})
export class BinMasterComponent implements OnInit {

  binmasterModelObj: BinMastermodel = new BinMastermodel();
  AddAppproductModelObj: BinMastermodel = new BinMastermodel();
  
  public griddata: any;
  formUsrCommonGroup:FormGroup;

  GroupList: any;
  buttons = Buttons;
  gridColumns = GridColumnsBinMaster;
  public state: State = {};
  MessageFlag: string;
 
   constructor( 
    private binmasterservice:BinMasterService ,
    private xRouter:Router,
    private EncrDecr: EncrDecrService,
    private xmodalService: NgbModal,
    )

    
    {

   }

  ngOnInit(): void {
    debugger
    this.getbinMasterData()
     
  }


  getbinMasterData() 
  {
    debugger
    this.binmasterModelObj.Type=1;
    console.log("response");
   this.binmasterservice.getbinMasterData(this.binmasterModelObj).subscribe((res)=>{
     debugger
      console.log('nikhil',res);
    this.griddata = res[0];
    })
    var rec=this.binmasterservice.getbinMasterData;
    console.log(rec);
    // console.log(rec);
    

  }

  showDetails(event,dataItem) 
  {
    debugger
    var encrypted=this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Bin_PkeyID);
    this.xRouter.navigate(["/home/bin-master-datils/bin-master-details", btoa(encrypted)]);
  
  
  }

  AddNewUser()
   {
  
  
  }
  commonMessage() {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });

  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }


}
