import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ViewAppProductsComponent } from './view-app-products.component';
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ViewAppProductsService } from './view-app-products.service';


export const ViewAppproductRouts: Routes = [
  {  path: 'viewappproduct', component:ViewAppProductsComponent},

    // { 
    //  path: 'add/:id',
    //  loadChildren: () => import('../add-app-products/add-app-products.module').then(m => m.AddAppProductsModule ),
    //  },
  { path: '', redirectTo: 'home', pathMatch: 'full'}
]



@NgModule({
  declarations: [ViewAppProductsComponent],
  imports: [ RouterModule.forChild(ViewAppproductRouts),
     
    GridModule, ExcelModule,
    FormsModule, ReactiveFormsModule,
    CommonModule
  ],
  providers: [ViewAppProductsService],
  bootstrap: [ViewAppProductsComponent]
})
export class ViewAppProductsModule { }
