import { Component, OnInit } from '@angular/core';
import { Buttons } from './constants';
import { GridColumnsproduct } from './constants';
import { State } from "@progress/kendo-data-query";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { AddAppProductsComponent } from '../add-app-products/add-app-products.component';
import{viewappproductmodel}from './view-app-products.model'
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { Router, Routes } from '@angular/router';
import { ViewAppProductsService } from './view-app-products.service';
// import { Injectable } from '@angular/core';
import { addappproductmodel } from '../add-app-products/add-app-product.model';
import { AddAppProductsService } from '../add-app-products/add-app-products.service';
import { FormGroup } from '@angular/forms';
// import { Observable } from 'rxjs/observable';
@Component({
  selector: 'app-view-app-products',
  templateUrl: './view-app-products.component.html',
  styleUrls: ['./view-app-products.component.scss']
})
export class ViewAppProductsComponent implements OnInit {

  viewproductModelObj: viewappproductmodel = new viewappproductmodel();
  AddAppproductModelObj: addappproductmodel = new addappproductmodel();
  
  public griddata: any;
  formUsrCommonGroup:FormGroup;

  GroupList: any;
  buttons = Buttons;
  gridColumns = GridColumnsproduct;
  public state: State = {};
  MessageFlag: string;
 
   constructor( 
    private xviewproductservice:ViewAppProductsService ,
    private xaddproductservice:AddAppProductsService,
    private xRouter:Router,
    private EncrDecr: EncrDecrService,
    private xmodalService: NgbModal,
    )

    
    {

   }

  ngOnInit(): void {
    debugger
    this.getautoproduct()
     
  }


  getautoproduct() 
  {
    debugger
    this.viewproductModelObj.Type=1;
    console.log("response");
   this.xviewproductservice.ViewProductData(this.viewproductModelObj).subscribe((res)=>{
     debugger
      console.log('nikhil',res);
    this.griddata = res[0];
    })
    var rec=this.xviewproductservice.ViewProductData;
    console.log(rec);
    // console.log(rec);
    

  }

  showDetails(event,dataItem) 
  {
    debugger
    var encrypted=this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Pro_PkeyID);
    this.xRouter.navigate(["/home/product/add/addproduct", btoa(encrypted)]);
  
  
  }
  deleteDetails(event,dataItem) {
    debugger

    var cfrm = confirm("Delete this Record...!");

    if (cfrm == true) {
      this.AddAppproductModelObj.Pro_PkeyID=dataItem.Pro_PkeyID;
      this.AddAppproductModelObj.Pro_IsActive=false;
      this.AddAppproductModelObj.Pro_IsDelete=true;
      this.AddAppproductModelObj.Type= 3;

      this.xaddproductservice.ProductData(this.AddAppproductModelObj).subscribe(response => {
          console.log('delete response', response);
          this.getautoproduct();
        });
     }
  
  
  }
  AddNewUser()
   {
  
  
  }
  commonMessage() {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });

  }
  checkChange(event, dataItem)
  {
    debugger
    this.AddAppproductModelObj.Pro_PkeyID = dataItem.Pro_PkeyID;
    this.AddAppproductModelObj.Pro_IsActive =event;//dataItem.HP_IsActive;
    this.AddAppproductModelObj.Type = 5;

    this.xaddproductservice
    .ProductData(this.AddAppproductModelObj)
    .subscribe(response => {
      this.MessageFlag = "product status upated...!";
      this.commonMessage();
        this.getautoproduct();
      });    
  
  
  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }


}
