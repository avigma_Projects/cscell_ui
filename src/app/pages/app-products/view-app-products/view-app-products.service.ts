import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { BaseUrl } from 'src/app/services/apis/rest-api';
import { ErrorhandlepageServices } from 'src/app/services/common-handleError/commonhandleError.services';
import { environment } from 'src/environments/environment';
import { viewappproductmodel } from './view-app-products.model';

@Injectable({
  providedIn: 'root'
})
export class ViewAppProductsService {
  open(IplAppModalContent: IplAppModalContent, arg1: { size: string; ariaLabelledBy: string; }) {
    throw new Error('Method not implemented.');
  }
  public token:any;
  // ViewproductData(productModelObj: viewappproductmodel) {
  //   throw new Error('Method not implemented.');
  // }

  constructor(private _http: HttpClient,
    private _Route: Router,
    private xHomepageServices: ErrorhandlepageServices) 
    { 
      this.token = JSON.parse(localStorage.getItem('TOKEN'));

  }
  private apiUrlPOST = BaseUrl + environment.Admin.Getproductdetails;
  public ViewProductData(Modelobj:viewappproductmodel)
   {
    var ANYDTO:any = {};
    ANYDTO.Pro_PkeyID=Modelobj.Pro_PkeyID;
    ANYDTO.Type=Modelobj.Type;


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
       
      );

  }
    // common handler

    public CommonhandleError(error: HttpErrorResponse) {
      if (error.status == 401) {
        alert('Unauthorized User Found...!');
        // window.location.href = '/';
      } else {
        alert("Something bad happened, please try again later...😌");
      }
  
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error("An error occurred:", error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` + `body was: ${error.error}`
        );
      }
      // return an observable with a user-facing error message
      return throwError("Something bad happened; please try again later.");
    }  

   
}
