import { Column } from '../../../../models/grid-column-model';

export const GridColumnsproduct: Column[] = [
     {
        title: 'BrandName',
       field: 'Pro_BrandName'
      },
      {
        title: 'Gender',
       field: 'Gender'
      },
      {
        title: 'Color',
        field: 'Col_Name',
      },
   
      {
        title: 'Category',
       field: 'Cat_Name'
      },
      {
        title: 'SubCategory',
       field: 'SubCat_Name'
      },
      {
        title: 'Product Name',
       field: 'Pro_Name'
      },
     
      
]
