import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import { viewappproductmodel } from '../view-app-products/view-app-products.model';
import { ViewAppProductsService } from '../view-app-products/view-app-products.service';
import { addappproductmodel } from './add-app-product.model';
import { AddAppProductsService } from './add-app-products.service';
import { FormBuilder } from '@angular/forms';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductImagesModel } from './Product-images.Model';

@Component({
  selector: 'app-add-app-products',
  templateUrl: './add-app-products.component.html',
  styleUrls: ['./add-app-products.component.scss']
})
export class AddAppProductsComponent implements OnInit {
  viewproductModelObj: viewappproductmodel = new viewappproductmodel();
  AddAppproductModelObj: addappproductmodel = new addappproductmodel();
  ProductImagesModelObj: ProductImagesModel = new ProductImagesModel();

  // formBuilder: any;
  formUsrCommonGroup:FormGroup;
  ModelObj:any;
  MessageFlag: string;
  submitted = false; // submitted;
  docdisable = true;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  UserNameReadOnly = false;
  // MessageFlag: string; // custom msg sathi
  public myFiles:Array<File> = [];
  RecordValList: any;
  GroupList: any;
  griddata: any;
  YESNOList: any;
  ProductActivelist:any;
  binlist: any;
  BinList: any;
  Binlist: any;
  isReadonly:boolean;
  //public myimgaes:Array<ProductImagesModel> = [];
  public myimgaes= [];
  public defaultMenuItem: { Bin_Name: string, Bin_PkeyID: number } = { Bin_Name: 'Select', Bin_PkeyID: 0 };
  public defaultTaskItem: { Cat_Name: string, Cat_Pkey: number } = { Cat_Name: 'Select', Cat_Pkey: 0 };
  public defaultTestItem: { SubCat_Name: string, SubCat_Pkey: number } = { SubCat_Name: 'Select', SubCat_Pkey: 0 };
  public defaultColourItem: { Col_Name: string, Col_PkeyID : number } = { Col_Name: 'Select', Col_PkeyID : 0 };
  public defaultVendorItem: { Ven_Name: string,  Ven_PkeyID : number } = { Ven_Name: 'Select',  Ven_PkeyID : 0 };
  genderlist: { Id: number; Name: string; }[];
  newlist: { Id: number; Name: string; }[];
  celblist: { Id: number; Name: string; }[];
  catlist: any;
  collist: any;
  venlist: any;
  // MessageFlag:string;
  // xaddappproductservices: any;
  
 
  // only drop down
 
  constructor(
    private xRouter:Router,
    private xRoute:ActivatedRoute,
    private xmodalService: NgbModal,
    private EncrDecr:EncrDecrService,
    private xaddappproductservices:AddAppProductsService,
    private xviewappprodcutservices:ViewAppProductsService,
    
    private formBuilder:FormBuilder
  ) {
    this.DropDown_product();
    this.DropDown_Product_DTO();
    this.DropDown_Product_DTO_Sub();
    this.DropDown_Product_DTO_Col();
    this.DropDown_Product_DTO_Ven();
  }

  ngOnInit()  
   {
     
    debugger
    const id1 = this.xRoute.snapshot.params['Id'];
    if (id1 == 'new' || id1 =='undefined')
     {
      this.AddAppproductModelObj = new addappproductmodel();
    } else 
    {
      if(id1 != undefined)
      {
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
    }
      // this.commonMessage();
    }
    this.formUsrCommonGroup = this.formBuilder.group({
      brandname:["",Validators.required],
      gender:["",Validators.required],
      color:["",Validators.required],
      detail:["",Validators.required],
      category:["",Validators.required],
      subCategory:["",Validators.required],
      condition:["",Validators.required],
      size:["",Validators.required],
      binPkey:["",Validators.required],
      //userPKey:["",Validators.required],
      binUnit:["",Validators.required],
      binShelf:["",Validators.required],
      pull:["",Validators.required],
      sold:["",Validators.required],
      //Col_Name:["",Validators.required],
      Pro_Name:["",Validators.required],
      Pro_Qty:["",Validators.required],
      Pro_Price:["",Validators.required],
      Pro_Shot:["",Validators.required],
      Pro_Associated_Cost:["",Validators.required],
      Pro_new_Used:["",Validators.required],
      Pro_Celeb_Ent:["",Validators.required],
      Pro_Vendor:["",Validators.required],
      // Ven_PkeyID:["",Validators.required],
     // Ven_Name:["",Validators.required], 
      Pro_Notes:["",Validators.required],
      Pro_Measurements:["",Validators.required],
      Pro_Location:["",Validators.required],
      Pro_Desc:["",Validators.required],
      imageUpload: ["",Validators.required],
      
    });  
    
    this.genderlist = [
      { Id: 1, Name: "Female" },
      { Id: 2, Name: "Male" },
      { Id: 3, Name: "Others" },
    ];
    this.newlist = [
      { Id: 1, Name: "New" },
      { Id: 2, Name: "Used" },
     
    ];
    this.celblist = [
      { Id: 1, Name: "Celebrity" },
      { Id: 2, Name: "Entourage" },
     
    ];
    this.ProductActivelist = [
      {ID:1,HP_IsActive:true},
      {ID:0,HP_IsActive:false},
     
    ]
        
    // this.getModelData();
 
    this.showMultipleImg();
  if(this.AddAppproductModelObj.Pro_Category==0){
    this.isReadonly=true;
  }
  }

  // {
  //   "ProImage_ID":res[0][0].product_Image_DTOs[i].ProImage_ID,
  //   "ProImage_Pro_ID":res[0][0].product_Image_DTOs[i].ProImage_Pro_ID,
  //   "ProImage_ImagePath": res[0][0].product_Image_DTOs[i].ProImage_ImagePath,
  //   "ProImage_ImageName":res[0][0].product_Image_DTOs[i].ProImage_ImageName,
  //   "ProImage_Number": res[0][0].product_Image_DTOs[i].ProImage_Number,
  //   "ProImage_IsFirst": res[0][0].product_Image_DTOs[i].ProImage_IsFirst,
  //   "ProImage_size": res[0][0].product_Image_DTOs[i].ProImage_size,
  //   "ProImage_IsActive":true,
  //   "ProImage_IsDelete":false,
  //   "Type":1
  // };
  showMultipleImg(){
     this.MultiplaImagarray=[{
      "ProImage_ID":0,
       "ProImage_Pro_ID":0,
      "ProImage_ImagePath":"/assets/img/users/default-user.jpg",
      "ProImage_Number":0,
      "ProImage_IsFirst":true,
    },
    {
      "ProImage_ID":0,
       "ProImage_Pro_ID":0,
      "ProImage_ImagePath":"/assets/img/users/default-user.jpg",
      "ProImage_Number":0,
      "ProImage_IsFirst":true,
    },
    {
      "ProImage_ID":0,
       "ProImage_Pro_ID":0,
      "ProImage_ImagePath":"/assets/img/users/default-user.jpg",
      "ProImage_Number":0,
      "ProImage_IsFirst":true,
    },
    {
      "ProImage_ID":0,
       "ProImage_Pro_ID":0,
      "ProImage_ImagePath":"/assets/img/users/default-user.jpg",
      "ProImage_Number":0,
      "ProImage_IsFirst":true,
    },
      
    ]
    // this.MultiplaImagarray=[
    //   "/assets/img/users/default-user.jpg",
    //   "/assets/img/users/default-user.jpg",
    //   "/assets/img/users/default-user.jpg",
    //   "/assets/img/users/default-user.jpg"
      
    // ]
  }
  getModelData(){
    
    console.log("nikhil");
    debugger


  }
  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  EditForms() {
    // this.IsEditDisable = false;
    // this.formUsrCommonGroup.enable();
  }
  commonMessage() {
   
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
      modalRef.componentInstance.MessageFlag = this.MessageFlag;
      modalRef.result.then(result => { }, reason => { });
 
    
  
  }
  DropDown_product(){
    debugger
    this.xaddappproductservices.DropDown_product(this.AddAppproductModelObj)
  .subscribe(respose =>{
    debugger
    console.log('drd',respose)
    this.BinList = respose[0];
    this.binlist = this.Binlist;
  })
}
BinFilter(value){
  debugger
  if (value!='') {
    var filteredcustomer = this.Binlist.filter(function (el) {
      return el.Race_Name != null;
    });
    debugger
    this.binlist = filteredcustomer.filter((s) => s.Bin_Name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
 else{
  this.binlist = this.Binlist.slice();
 }
}


  GetSingleData(){
    debugger
    console.log("editres");
    this.viewproductModelObj.Pro_PkeyID =this.ModelObj;
    this.viewproductModelObj.Type = 2 ;
    this.xviewappprodcutservices.ViewProductData(this.viewproductModelObj).subscribe(res =>{
      console.log('edit',res)
      debugger
this.AddAppproductModelObj.Pro_PkeyID = res[0][0].Pro_PkeyID;
this.AddAppproductModelObj.Pro_BrandName = res[0][0].Pro_BrandName;
this.AddAppproductModelObj.Pro_Gender = res[0][0].Pro_Gender;
this.AddAppproductModelObj.Pro_Color = res[0][0].Pro_Color;
this.AddAppproductModelObj.Pro_Detail = res[0][0].Pro_Detail;
this.AddAppproductModelObj.Pro_IsActive = res[0][0].Pro_IsActive;
this.AddAppproductModelObj.Pro_Category = res[0][0].Pro_Category;
this.AddAppproductModelObj.Pro_SubCategory = res[0][0].Pro_SubCategory;
this.AddAppproductModelObj.Pro_Condition = res[0][0].Pro_Condition;
this.AddAppproductModelObj.Pro_Size = res[0][0].Pro_Size;
this.AddAppproductModelObj.Pro_Bin_PkeyID = res[0][0].Pro_Bin_PkeyID;
this.AddAppproductModelObj.Pro_User_PkeyID = res[0][0].Pro_User_PkeyID;
this.AddAppproductModelObj.Pro_Bin_Unit = res[0][0].Pro_Bin_Unit;
this.AddAppproductModelObj.Pro_Bin_Shelf = res[0][0].Pro_Bin_Shelf;
this.AddAppproductModelObj.Pro_Pull = res[0][0].Pro_Pull;
this.AddAppproductModelObj.Pro_Sold = res[0][0].Pro_Sold;
this.AddAppproductModelObj.Bin_PkeyID = res[0][0].Bin_PkeyID;
this.AddAppproductModelObj.Bin_Name = res[0][0].Bin_Name;
this.AddAppproductModelObj.Cat_Pkey = res[0][0].Cat_Pkey;
this.AddAppproductModelObj.Cat_Name = res[0][0].Cat_Name;
this.AddAppproductModelObj.SubCat_Pkey = res[0][0].SubCat_Pkey;
this.AddAppproductModelObj.SubCat_Name= res[0][0].SubCat_Name;
this.AddAppproductModelObj.Col_PkeyID = res[0][0].Col_PkeyID;
this.AddAppproductModelObj.Col_Name= res[0][0].Col_Name;
this.AddAppproductModelObj.Pro_Name= res[0][0].Pro_Name;
this.AddAppproductModelObj.Pro_Qty= res[0][0].Pro_Qty;
this.AddAppproductModelObj.Pro_Price= res[0][0].Pro_Price;
this.AddAppproductModelObj.Pro_Shot= res[0][0].Pro_Shot;
this.AddAppproductModelObj.Pro_Associated_Cost= res[0][0].Pro_Associated_Cost;
this.AddAppproductModelObj.Pro_new_Used= res[0][0].Pro_new_Used;
this.AddAppproductModelObj.Pro_Celeb_Ent= res[0][0].Pro_Celeb_Ent;
this.AddAppproductModelObj.Pro_Vendor= res[0][0].Pro_Vendor;
this.AddAppproductModelObj.Ven_PkeyID= res[0][0].Ven_PkeyID;
this.AddAppproductModelObj.Ven_Name= res[0][0].Ven_Name;
this.AddAppproductModelObj.Pro_Desc= res[0][0].Pro_Desc;
this.AddAppproductModelObj.Pro_Location= res[0][0].Pro_Location;
this.AddAppproductModelObj.Pro_Notes= res[0][0].Pro_Notes;
this.AddAppproductModelObj.Pro_Measurements= res[0][0].Pro_Measurements;
//this.image = this.AddAppproductModelObj.ProImage_ImagePath;
if(this.AddAppproductModelObj.Pro_Category ==0){
  this.isReadonly =true;
}
this.AddAppproductModelObj.Type=2;
if(this.AddAppproductModelObj.Pro_IsActive==false){
  this.AddAppproductModelObj.Pro_IsActive=false;
 }
 else{
  this.AddAppproductModelObj.Pro_IsActive=true;
 }

 this.MultiplaImagarray=[];
 
 let count =res[0][0].product_Image_DTOs.length;
 if(count>0){
for(var i=0; i<count; i++)
{
  //this.MultiplaImagarray5.push(res[0][0].product_Image_DTOs[i].ProImage_ImagePath);
  
 let   imgdata =
  {
    "ProImage_ID":res[0][0].product_Image_DTOs[i].ProImage_ID,
    "ProImage_Pro_ID":res[0][0].product_Image_DTOs[i].ProImage_Pro_ID,
    "ProImage_ImagePath": res[0][0].product_Image_DTOs[i].ProImage_ImagePath,
    "ProImage_ImageName":res[0][0].product_Image_DTOs[i].ProImage_ImageName,
    "ProImage_Number": res[0][0].product_Image_DTOs[i].ProImage_Number,
    "ProImage_IsFirst": res[0][0].product_Image_DTOs[i].ProImage_IsFirst,
    "ProImage_size": res[0][0].product_Image_DTOs[i].ProImage_size,
    "ProImage_IsActive":true,
    "ProImage_IsDelete":false,
    "Type":1
  };
  this.myimgaes.push(imgdata)
  this.MultiplaImagarray.push(imgdata);
} 
console.log("abd",this.MultiplaImagarray)
}
});
}

  onIsActiveChanged(val){
    debugger
    if(val.target.value=='1: false'){
      this.AddAppproductModelObj.Pro_IsActive=false;
      }
      else{
        this.AddAppproductModelObj.Pro_IsActive=true;
      }

  }
  MultiplaImagarray=[];
  imgList=[];
  fileChange(event, input) {
    debugger;
  
  
    var files = event.target.files;
      var file1 = files[0];
      let fileLength=event.target.files.length;
      //let data;
      for(let i=0; i <fileLength; i++)
      {
        var file1 = files[i];
        this.imgList.push(files[i]);
      if (files && file1) {
        debugger
        var reader1 = new FileReader();
        debugger
        reader1.onload = this._handleReaderLoaded.bind(this);
  
        reader1.readAsBinaryString(file1);
      }
    }
    }

      base64textString:string="";
  imgData:string="";
  ProImage_Number:number=0;
  is_firstImg:boolean;;
  _handleReaderLoaded(readerEvt):string {
    debugger
    
    this.MultiplaImagarray=[];
   let filePath;
   var binaryString = readerEvt.target.result;
   debugger
          this.base64textString= btoa(binaryString);
          //filePath=btoa(binaryString);
          console.log(btoa(binaryString));
          debugger
           this.imgData= "data:image/png;base64,"+this.base64textString;
          //  this.AddAppproductModelObj.Hp_Image=this.imgData;
          this.AddAppproductModelObj.Image_Base=this.imgData;
          this.xaddappproductservices.imgupload(this.AddAppproductModelObj).subscribe( res =>{
            debugger
            if(res !=null)
            {
              //this.imgSrc=res;
let img_no =this.ProImage_Number++;
              console.log(this.image=res[0].Image_Path);
              this.ProductImagesModelObj.ProImage_ImagePath = this.image=res[0].Image_Path;
              debugger
             if(this.ProImage_Number ==1){
              this.is_firstImg =true;
             }
             else
             {
              this.is_firstImg =false;
             }
              //this.MultiplaImagarray.push(res[0].Image_Path);
              let imgdata =
              {
                "ProImage_ImagePath": res[0].Image_Path,
                "ProImage_ImageName": '',
                "ProImage_Number": this.ProImage_Number,
                "ProImage_IsFirst": this.is_firstImg,
                "ProImage_size": '',
                "ProImage_IsActive":true,
                "ProImage_IsDelete":false,
                "Type":1
              };
              this.myimgaes.push(imgdata)
              this.MultiplaImagarray.push(imgdata);
                   //this.myimgaes.push(this.ProductImagesModelObj)
            }
          });
          return filePath;
  }
  public image: any;
  removeImage(): void {
    this.image = '';
  }
  formButton(){
    debugger
   // .fromButton(this.AddAppproductModelObj)
    console.log('responese');
    if (this.myimgaes.length > 0)
{
  this.AddAppproductModelObj.Pro_Images = JSON.stringify(this.myimgaes)
} 
    if(this.ModelObj>0)
    {
      this.AddAppproductModelObj.Pro_PkeyID=this.ModelObj;
      this.AddAppproductModelObj.Type=2;
    }
    else{
      this.AddAppproductModelObj.Type=1;
    }
    if(this.formUsrCommonGroup.valid){
      debugger
      console.log('check',this.AddAppproductModelObj)
      this.xaddappproductservices.ProductData(this.AddAppproductModelObj)
      .subscribe(respose =>{
        
       alert("Product Details Saved Sucessfully");
      console.log('res',respose)
      this.xRouter.navigate(["/home/product/viewappproduct"]);
   
  
    })
    }
    else{
      alert("All Field are Required")
    }
       
  
  }
    
      
  
 
 
 CategoryList:any;
 SubList:any;
 RaceTypeList:any;
 DropDown_Product_DTO(){
   debugger
   this.AddAppproductModelObj.Type =1
   this.xaddappproductservices.DropDown_Product_DTO(this.AddAppproductModelObj)
 .subscribe(respose =>{
   debugger
   console.log('drd',respose)
   
   this.CategoryList = respose[0];
   this.catlist =  this.CategoryList;
  
   
   })
  
 }
 DropDown_Product_DTO_Sub(){
  debugger
  
  this.AddAppproductModelObj.Type =2
  this.xaddappproductservices.DropDown_Product_DTO(this.AddAppproductModelObj)
.subscribe(respose =>{
  debugger
  console.log('drd',respose)
  
 
  this.SubList = respose[0];
  this.SubList =  this.SubList;
  
  })
 
}
DropDown_Product_DTO_Col(){
  debugger
  
  this.AddAppproductModelObj.Type =3
  this.xaddappproductservices.DropDown_Product_DTO(this.AddAppproductModelObj)
.subscribe(respose =>{
  debugger
  console.log('drd',respose)
  
 
  this.collist = respose[0];
  this.collist =  this.collist;
  
  })
 
}


DropDown_Product_DTO_Ven(){
  debugger
  
  this.AddAppproductModelObj.Type =4
  this.xaddappproductservices.DropDown_Product_DTO(this.AddAppproductModelObj)
.subscribe(respose =>{
  debugger
  console.log('drd',respose)
  
 
  this.venlist = respose[0];
  this.venlist =  this.venlist;
  
  })
 
}

 CategoryFilter(value){
   debugger
   this.isReadonly=true;
  if (value!='') {
    var filteredcustomer = this.CategoryList.filter(function (el) {
      return el.Cat_Name != null;
    });
    this.catlist = filteredcustomer.filter((s) => s.Cat_Name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
 else{
  this.catlist = this.CategoryList.slice();
 }
}

CategoryvalueChange(value){
  debugger
  
 if (value!='') {
  this.isReadonly=false;
   var filteredcustomer = this.CategoryList.filter(function (el) {
     return el.Cat_Name != null;
   });
   debugger
  // this.catlist = filteredcustomer.filter((s) => s.Cat_Name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
 }
else{
  this.isReadonly=true;
 this.catlist = this.CategoryList.slice();
}
}

SubCatFilter(value){
  if (value!='') {
    var filteredcustomer = this.SubList.filter(function (el) {
      return el.SubCat_Name != null;
    });
    this.SubList = filteredcustomer.filter((s) => s.SubCat_Name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
 else{
  this.SubList = this.SubList.slice();
 }
}

ColourFilter(value){
  debugger
  this.isReadonly=true;
 if (value!='') {
   var filteredcustomer = this.collist.filter(function (el) {
     return el.Col_Name != null;
   });
   this.collist = filteredcustomer.filter((s) => s.Col_Name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
 }
else{
 this.collist = this.collist.slice();
}
}


VendorFilter(value){
  debugger
  this.isReadonly=true;
 if (value!='') {
   var filteredcustomer = this.venlist.filter(function (el) {
     return el.Ven_Name != null;
   });
   this.venlist = filteredcustomer.filter((s) => s.Ven_Name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
 }
else{
 this.venlist = this.venlist.slice();
}
}
checkChangePull(event)
{
  debugger
  this.AddAppproductModelObj.Pro_Pull =event;//dataItem.HP_IsActive;

}
checkChangeSold(event)
{
  debugger
 
  this.AddAppproductModelObj.Pro_Sold =event;
}
checkChangeShot(event)
{
  debugger
 
  this.AddAppproductModelObj.Pro_Shot =event;
}
onGenderChanged(event)
{
  debugger
 
 }
onNewChanged(event)
{
  debugger
 
}
onCelbChanged(event)
{
  debugger
 
}
deleteData(event,indx)
{
  debugger
 let pro_img_id= event.ProImage_ID
 this.ProductImagesModelObj.ProImage_ID=pro_img_id;
 this.ProductImagesModelObj.ProImage_IsDelete=true;
 this.ProductImagesModelObj.Type=4;
 this.xaddappproductservices.createUpdate_ImageProduct(this.ProductImagesModelObj)
 .subscribe(respose =>{
   debugger;
//    indexOf(searchElement)
// indexOf(searchElement, fromIndex)

  const index = this.MultiplaImagarray.indexOf(pro_img_id, indx);
  if (indx > -1) {
    debugger;
    this.MultiplaImagarray.splice(indx);
    this.myimgaes.splice(indx);
    //this.myimgaes.splice(index, 1); 
  }
  this.MultiplaImagarray.forEach((value,index)=>{
    if(value==pro_img_id) this.MultiplaImagarray.splice(index,1);
  });
 })

 // 
}
}



