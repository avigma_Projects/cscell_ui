import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddAppProductsComponent } from './add-app-products.component';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import{NgMultiSelectDropDownModule}from 'ng-multiselect-dropdown'
import { ReactiveFormsModule } from '@angular/forms';
import { AddAppProductsService } from './add-app-products.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
export const appproductRoutes=[
  { path:'addproduct', component:AddAppProductsComponent},
  { path:'addproduct/:Id', component:AddAppProductsComponent}
  

 ]
@NgModule({
  declarations: [AddAppProductsComponent],
  imports: [RouterModule.forChild(appproductRoutes),
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule, 
    CommonModule,
    DropDownsModule
  ],
  providers: [AddAppProductsService],
  bootstrap: [AddAppProductsComponent]
})
export class AddAppProductsModule { }
