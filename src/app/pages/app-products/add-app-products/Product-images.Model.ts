export class ProductImagesModel{
    ProImage_ID:number=0;
    ProImage_Pro_ID:number=0;
    ProImage_ImageName:string='';
    ProImage_size:number=0;
    ProImage_ImagePath:string=''; 
    ProImage_IsFirst: boolean;
    ProImage_Number:number=0;
    ProImage_IsActive:boolean=true;
    ProImage_IsDelete:boolean=false;
    Type:number=0;
}