export class addappproductmodel
{

    Pro_PkeyID:number=0;
    Pro_BrandName:string='';
   
    Pro_Gender:number;
    
    Pro_Color:number;
    Pro_Detail:string='';
    Pro_Category:number;
    Pro_SubCategory:number;
    Pro_Condition:number;
    Pro_Size:string='';
    //Image_Base:string;
    Pro_IsActive:boolean=true;
    Pro_IsDelete:boolean=false;
    Pro_Bin_PkeyID:number;
    Pro_User_PkeyID:number;
    Pro_Bin_Unit:string='';
    Pro_Bin_Shelf:string='';
    Pro_Pull:boolean;
    Pro_Sold:boolean;
    Bin_PkeyID:number;
    Bin_Name:string='';
    
    //ProImage_ImagePath:string='';
    Cat_Pkey:number;
    Cat_Name:string='';
    SubCat_Pkey:number;
    SubCat_Name:string='';
    
    Pro_Images:string='';
    Image_Base:string;
    Pro_Name:string;
    Pro_Vendor_Name:string;
   
    ProImage_ImagePath:string;
    Col_PkeyID:number;
    Col_Name:string='';
    Pro_Desc:string;
    Pro_new_Used:number;
    Pro_Celeb_Ent:number;
    Pro_Vendor:number;
    Pro_Qty:number;
    Pro_Associated_Cost:number;
    Pro_Location:string='';
    Pro_Notes:string='';
    Pro_Measurements:string='';
    Pro_Shot:boolean;
    Pro_Price:number;
    Pro_BarCode_ImagePath:string='';
    Ven_PkeyID:number;
    Ven_Name:string='';
    Type:number=2;

}