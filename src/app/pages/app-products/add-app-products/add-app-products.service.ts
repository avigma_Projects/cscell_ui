import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { BaseUrl } from 'src/app/services/apis/rest-api';
import { ErrorhandlepageServices } from 'src/app/services/common-handleError/commonhandleError.services';
import { environment } from 'src/environments/environment';
import { viewappproductmodel } from '../view-app-products/view-app-products.model';
import { addappproductmodel } from './add-app-product.model';
import { ProductImagesModel } from './Product-images.Model';

@Injectable({
  providedIn: 'root'
})
export class AddAppProductsService {
  baseurl: string;
 
   open(IplAppModalContent: IplAppModalContent, arg1: { size: string; ariaLabelledBy: string; }) {
    throw new Error('Method not implemented.');
  }
  public token:any;

  constructor(private _http: HttpClient,
    private _Route: Router,
   
    private xHomepageServices: ErrorhandlepageServices) { }
   
    private apiUrlPOST_ImgProduct = BaseUrl + environment.Admin.createUpdate_ImageProduct
  private apiUrlPOSTimg = BaseUrl + environment.Admin.ImageProductDetails;
  private apiUrlPOST = BaseUrl + environment.Admin.postproductdetails;
  public ProductData(Modelobj: addappproductmodel) {
    debugger
    var ANYDTO: any = {};
    ANYDTO.Pro_PkeyID = Modelobj.Pro_PkeyID;
    ANYDTO.Pro_BrandName = Modelobj.Pro_BrandName;
    ANYDTO.Pro_Gender = Modelobj.Pro_Gender;
    ANYDTO.Pro_Color = Modelobj.Pro_Color;
    ANYDTO.Pro_Detail = Modelobj.Pro_Detail;
    ANYDTO.Pro_Category = Modelobj.Pro_Category;
    ANYDTO.Pro_SubCategory = Modelobj.Pro_SubCategory;
    ANYDTO.Pro_Condition = Modelobj.Pro_Condition;
    ANYDTO.Pro_Size = Modelobj.Pro_Size;
    ANYDTO.Pro_Bin_PkeyID = Modelobj.Pro_Bin_PkeyID;
    ANYDTO.Pro_User_PkeyID = Modelobj.Pro_User_PkeyID;
    ANYDTO.Pro_Bin_Unit = Modelobj.Pro_Bin_Unit;
    ANYDTO.Pro_Bin_Shelf = Modelobj.Pro_Bin_Shelf;
    ANYDTO.Pro_Pull = Modelobj.Pro_Pull;
    ANYDTO.Pro_Sold = Modelobj.Pro_Sold;
    ANYDTO.Pro_IsActive = Modelobj.Pro_IsActive;
    ANYDTO.Pro_IsDelete = Modelobj.Pro_IsDelete;
    ANYDTO.Pro_Images = Modelobj.Pro_Images;
    ANYDTO.Col_Name = Modelobj.Col_Name;
    ANYDTO.Pro_Name = Modelobj.Pro_Name;
    ANYDTO.Pro_Qty = Modelobj.Pro_Qty;
    ANYDTO.Pro_Price = Modelobj.Pro_Price;
    ANYDTO.Pro_Shot = Modelobj.Pro_Shot;
    ANYDTO.Pro_Associated_Cost = Modelobj.Pro_Associated_Cost;
    ANYDTO.Pro_new_Used = Modelobj.Pro_new_Used;
    ANYDTO.Pro_Celeb_Ent = Modelobj.Pro_Celeb_Ent;
    ANYDTO.Ven_PkeyID = Modelobj.Ven_PkeyID;
    ANYDTO.Ven_Name = Modelobj.Ven_Name;
    ANYDTO.Pro_Vendor = Modelobj.Pro_Vendor;
    ANYDTO.Pro_Location = Modelobj.Pro_Location;
    ANYDTO.Pro_Notes = Modelobj.Pro_Notes;
    ANYDTO.Pro_Measurements = Modelobj.Pro_Measurements;
    ANYDTO.Pro_Desc= Modelobj.Pro_Desc;
    // if (Modelobj.HP_pkeyID !=  0  && Modelobj.HP_pkeyID !=  null)
    // {
    //   Modelobj.Type = 2;
    // }
    // else
    // {
    //   Modelobj.Type=1;
    // }

    ANYDTO.Type=Modelobj.Type;
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.CommonhandleError)
      );
  
  }

  public createUpdate_ImageProduct(Modelobj: ProductImagesModel) {
    var ANYDTO: any = {};
    ANYDTO.ProImage_ID = Modelobj.ProImage_ID;
    ANYDTO.ProImage_IsDelete= Modelobj.ProImage_IsDelete;
    ANYDTO.Type=Modelobj.Type;
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST_ImgProduct, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.CommonhandleError)
      );
  
  }
  private apiUrlPOST1 = BaseUrl + environment.Admin.DropDownProduct;
  public DropDown_product(Modelobj:viewappproductmodel) {
    debugger
    let ANYDTO: any = {};
    ANYDTO.Bin_PkeyID=Modelobj.Bin_PkeyID;
    ANYDTO.Type=Modelobj.Type;
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST1, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.CommonhandleError)
      );
  }

  private apiUrlPOST2 = BaseUrl + environment.Admin.DropDownDetails;
    public DropDown_Product_DTO(Modelobj:viewappproductmodel) {
      debugger
      let ANYDTO: any = {};
      ANYDTO.Cat_Pkey=Modelobj.Cat_Pkey;
      ANYDTO.SubCat_Pkey=Modelobj.SubCat_Pkey;
      ANYDTO.Col_PkeyID=Modelobj.Col_PkeyID;
      ANYDTO.Ven_PkeyID=Modelobj.Ven_PkeyID;
      ANYDTO.Type=Modelobj.Type;
      let headers = new HttpHeaders({ "Content-Type": "application/json" });
      headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
      return this._http
        .post<any>(this.apiUrlPOST2, ANYDTO, { headers: headers })
        .pipe(
          tap(data => {
            return data;
          }),
          catchError(this.CommonhandleError)
        );
    }
  private apifileupload = environment.Admin.ImageProductDetails //this.baseUrl + "/api/fileupload/FileUpload";

  public imgupload(Modelobj: addappproductmodel) {
     var ANYDTO: any = {};
     debugger
    //  console.log('name',Modelobj);
  

  var form = new FormData();
    // form.append("HP_pkeyID", Modelobj.HP_pkeyID.toString());
    // form.append("HP_ProductSKU", Modelobj.HP_ProductSKU.toString());
    // form.append("HP_ProductName", Modelobj.HP_ProductName.toString());
    // form.append("HP_Qty", Modelobj.HP_Qty.toString());
    // form.append("HP_Price", Modelobj.HP_Price.toString());
    // form.append("HP_WebLsiteink", Modelobj.HP_WebLsiteink.toString());
    //form.append("Image_Base", Modelobj.Image_Base.toString());
    // form.append("HP_IsActive", Modelobj.HP_IsActive.toString());
    // form.append("Hp_IsDelete", Modelobj.Hp_IsDelete.toString());
    //ANYDTO.Image_Base=Modelobj.Image_Base.toString();
    form.append("Image_Base", Modelobj.Image_Base.toString());

    ANYDTO.Image_Base=Modelobj.Image_Base.toString();

    debugger
    //let headers = new HttpHeaders({"mimetype": "multipart/form-data"});
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);

    return this._http
      .post<any>(this.apiUrlPOSTimg, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
          //console.log(data)
        }),
        catchError(this.CommonhandleError)

      );
           }
  
  public CommonhandleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User Found...!');
      // window.location.href = '/';
    } else {
      alert("Something bad happened, please try again later...😌");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }  
  
}
