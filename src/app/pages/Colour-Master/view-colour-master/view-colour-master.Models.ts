export class ViewColourModel{
    Col_PkeyID:number=0;
    Col_Name:string='';
    Col_IsActive:boolean=true;
    Col_IsDelete:boolean=false;
    Type:Number=1;
}

export class AddColourModel{
    Col_PkeyID:number=0;
    Col_Name:string='';
    Col_IsActive:boolean=false;
    Col_IsDelete:boolean=false;
    UserID: Number = 0;
    Type:Number=2;
}