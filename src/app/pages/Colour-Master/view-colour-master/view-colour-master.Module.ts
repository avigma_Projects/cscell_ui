import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {ViewColourMasterComponent} from './view-colour-master.component';
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {ViewColourService} from './view-colour-master.Service';
export const ViewColourRouts: Routes = [
    {  path: 'viewappcolour', component:ViewColourMasterComponent},
  
      
    { path: '', redirectTo: 'home', pathMatch: 'full'}
  ]
  @NgModule({
    declarations: [ViewColourMasterComponent],
    imports: [ RouterModule.forChild(ViewColourRouts),
       
      GridModule, ExcelModule,
      FormsModule, ReactiveFormsModule,
      CommonModule
    ],
    providers: [ViewColourService],
    bootstrap: [ViewColourMasterComponent]
  })
  export class ViewColourModule { }
  