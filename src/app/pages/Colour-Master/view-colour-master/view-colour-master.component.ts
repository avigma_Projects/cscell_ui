import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';


import { State } from "@progress/kendo-data-query";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import{AddColourModel, ViewColourModel}from './view-colour-master.Models'
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';

import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { ActivatedRoute, Router, Routes } from '@angular/router';
import { ViewColourService } from './view-colour-master.Service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DatePipe } from '@angular/common';
import { FilterdataPipe } from '../../../shared/filterdata.pipe';
import { Buttons } from '../../appinces-details/constants';


@Component({
  selector: 'app-view-colour-master',
  templateUrl: './view-colour-master.component.html',
  styleUrls: ['./view-colour-master.component.scss'],
  providers:[DatePipe,FilterdataPipe]
})
export class ViewColourMasterComponent implements OnInit {

  @ViewChild('contentCateFORM') contentCateFORM: ElementRef;
  ViewColourModelObj: ViewColourModel = new ViewColourModel();
 // AddAppproductModelObj: addappproductmodel = new addappproductmodel();
 AddColourModelObj: AddColourModel = new AddColourModel();

  public griddata: any;
  formUsrCommonGroup:FormGroup;

  GroupList: any;
  public gridData: any[];
  public gridView: any[];
  public state: State = {};
  //griddata:any;
  UsrName:any;
  buttons = Buttons;
  myFormGroup: FormGroup;
  fallbackIcon = 'fas fa-user';
  button: string;
  MessageFlag: string;
  ModelObj: number;
  isUpdateDisable: boolean;
  isHelpActive: boolean;
  categoryName: string;
  //FormFields = FormFields;
  iconCss: any;
  iconColor: string;
  isActive: boolean;
  conId: any;
  isSubmitted: boolean;
  Col_Name: string;
  Col_IsActive: boolean;
  Col_PkeyID: any;
  isEditDisable: boolean;
  data: any;
  CustomArray: any;
  contentx: any;
  
  constructor(private formbuilder:FormBuilder,
    private xviewColourservice:ViewColourService ,
    private xaddColourservice:ViewColourService ,
    private xRouter:Router,
    private EncrDecr: EncrDecrService,
    public datepipe: DatePipe,
    private xRoute: ActivatedRoute,
    private xmodalService: NgbModal,

  ) 
  
  
  
  
  { 

    debugger
    if (localStorage.getItem('TOKEN') != null) {
      this.UsrName = localStorage.getItem('UserName');
    } 
  }

  ngOnInit() {
    debugger
  
    this.formUsrCommonGroup = this.formbuilder.group({
      Col_Name: ["", Validators.required],
     
   });
   this.gridView = this.gridData;
   debugger
      this.getcolourproduct()
     }
   get fx() {
     return this.formUsrCommonGroup.controls;
   }

   getcolourproduct() 
   {
     debugger
     this.ViewColourModelObj.Type=3;
     console.log("response");
    this.xviewColourservice.ViewColourData(this.ViewColourModelObj).subscribe((res)=>{
      debugger
       console.log('nikhil',res);
     this.griddata = res[0];
     this.CustomArray = Response[0];
     })
     var rec=this.xviewColourservice.ViewColourData;
     console.log(rec);
     // console.log(rec);
     
 
   }

   GetGridDetails(){ 
    debugger
    this.xviewColourservice.ViewColourData(this.ViewColourModelObj)

.subscribe(res =>{
  console.log('unnati',res)
  this.griddata = res[0];
})
  }
  commonMessage() {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });

  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }

  getModelData() {
    debugger
    const id1 = this.xRoute.snapshot.params['id'];
    if (id1 == 'new') {
      this.ViewColourModelObj = new ViewColourModel();
    } else {
      debugger
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      debugger
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
    }
  }

  GetSingleData(){
    debugger
    
    this.ViewColourModelObj.Col_PkeyID =   this.ModelObj;
    
    
    
    this.ViewColourModelObj.Type = 2;
    this.xviewColourservice.ViewColourData(this.ViewColourModelObj).subscribe(res =>{
      console.log('edit',res)
      debugger
this.ViewColourModelObj.Col_PkeyID = res[0][0].Col_PkeyID;
this.ViewColourModelObj.Col_Name = res[0][0].Col_Name;
this.ViewColourModelObj.Col_IsActive = res[0][0].Col_IsActive;



this.ViewColourModelObj.Type =2;
if(this.ViewColourModelObj.Col_IsActive==false){
  this.ViewColourModelObj.Col_IsActive=false;
 }
 else{
  this.ViewColourModelObj.Col_IsActive=true;
 }

    })

  }

  checkChange(event, dataItem) {
    debugger
        this.AddColourModelObj.Col_PkeyID = dataItem.Col_PkeyID;
        this.AddColourModelObj.Col_IsActive =event;
        this.AddColourModelObj.Type = 5;
    
        this.xaddColourservice
        .ColourDataPost(this.AddColourModelObj)
        .subscribe(response => {
          this.MessageFlag = "Colour status upated...!";
          this.commonMessage();
            this.getcolourproduct();
          });    
      }

      addcategory(event, content) {
        debugger
        if (event === "Create Customer Number") {
          this.isEditDisable = false;
          this.AddColourModelObj = new AddColourModel();
          this.xmodalService.open(content);
        } else {
          this.xmodalService.open(this.contentCateFORM);
        }
      }


      showDetails(content, dataItem) {
        debugger
        this.isHelpActive = false;
        this.ModelObj = dataItem.Col_PkeyID;
        //console.log('dataItem', dataItem);
       
        this.AddColourModelObj.Col_PkeyID = dataItem.Col_PkeyID;
        this.AddColourModelObj.Col_Name = dataItem.Col_Name;
        
        this.Col_PkeyID = dataItem.Col_PkeyID;
        
      
        this.AddColourModelObj.Col_IsActive = dataItem.Col_IsActive;
        console.log("showDetails", this.AddColourModelObj)
        this.isUpdateDisable = true;
        this.formUsrCommonGroup.disable();
        this.xmodalService.open(content);
        
      }

      deleteDetails(event, dataItem) {
        debugger
         var cfrm = confirm("Do you want to delete this Record...!");
    
        if (cfrm == true) {
          this.AddColourModelObj.Col_PkeyID = dataItem.Col_PkeyID;
          this.AddColourModelObj.Col_IsActive =false;
          this.AddColourModelObj.Col_IsDelete =true;
          //this.AddAppUserModelObj.User_DOB=null;
          this.AddColourModelObj.Type=4;
    debugger
          this.xaddColourservice.ColourDataPost(this.AddColourModelObj)
            .subscribe(response => {
    
              debugger
              this.MessageFlag = "Data Deleted Succesfully";
              this.commonMessage();
              console.log('delete response', response);
              this.getcolourproduct();
              
            });
        }
      }

      SetHelpFlag()
      {
        this.isHelpActive = !this.isHelpActive
        if (this.isHelpActive) {
          this.MessageFlag = "Item Help mode is on...!";
          this.commonMessage();
        }
        else
        {
          this.MessageFlag = "Item Help mode is off...!";
          this.commonMessage();
        }
      }

      DispalyInfo(event: Event, lblName)
      {    
        if (this.isHelpActive) {
          event.preventDefault();
          this.MessageFlag = "Add Information for " + lblName;
          this.commonMessage();
        }    
      }

      checkValue(state){
        //console.log(state);
      }

      formButton(content){
        debugger
       // .fromButton(this.AddAppproductModelObj)
       this.contentx = content;
       this.isSubmitted = true;
      debugger
          // stop here if form is invalid
          if (this.formUsrCommonGroup.invalid) {
            return;
          }
        console.log('responese');
        debugger
        if(this.ModelObj>0)
        {
          this.AddColourModelObj.Col_PkeyID=this.ModelObj;
          this.AddColourModelObj.Type=2;
        }
        else{
          this.AddColourModelObj.Type=1;
        }
           debugger
            console.log('check',this.AddColourModelObj)
            this.xaddColourservice
                   .ColourDataPost(this.AddColourModelObj)
          .subscribe(respose =>{
            this.getcolourproduct();
           
          console.log('res',respose)
      
          })
      
      }

      closeModal() {
        this.xmodalService.dismissAll();
      }
      filterCall() {
        this.ViewColourModelObj.Type = 3;
        this.xviewColourservice
          .ViewColourData( this.ViewColourModelObj)
          .subscribe(response => {
            this.state.take = 15;
            this.state.skip = 0;
            //console.log("category", response);
            this.griddata = response[0];
          });
      }

      getData() {
        debugger
            this.AddColourModelObj.Col_PkeyID = 0;
            this.AddColourModelObj.Col_Name = "";
           
            this.AddColourModelObj.Col_IsActive = false;
        
          }

          content;
          swap = [];
          addRow(item) {
            
            console.log('check',item)
            debugger
        
            let data = {
              Col_PkeyID: 0,
              Col_Name: item.Col_Name,
              
              Col_IsActive : false,
            }
            this.griddata.push(data);
            this.formButton(this.content);
            this.getData();
            // this.SurveyArrayobj.sur_OPT_VAL = "";
            console.log('check array :',this.swap);
            alert("Data Saved...");
          }
        
          deleteRow(index) {  
            debugger
            if(this.data.length == 1) { 
              // this.toastr.error("Can't delete the row when there is only one row", 'Warning');  
                return false;  
            } else {
                // this.getData();  
                this.CustomArray.splice(index, 1);  
                // this.toastr.warning('Row deleted successfully', 'Delete row');  
                return true;  
            }  
          }




  }


