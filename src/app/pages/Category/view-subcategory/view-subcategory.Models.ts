export class ViewSubCategoryModel{
    SubCat_Pkey:number=0;
    SubCat_Name:string='';
    SubCat_Cat_Pkey:number=0;
    SubCat_IsActive:boolean=true;
    SubCat_IsDelete:boolean=false;
    Cat_Pkey:number=0;
    Cat_Name:string='';
    Type:Number=1;
}

export class AddSubCategoryModel{
    SubCat_Pkey:number=0;
    SubCat_Name:string='';
    SubCat_Cat_Pkey:number=0;
    SubCat_IsActive:boolean=false;
    SubCat_IsDelete:boolean=false;
    Cat_Pkey:number=0;
    Cat_Name:string='';
    Type:Number=2;
    UserID: Number = 0;
   
}