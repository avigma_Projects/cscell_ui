import { Component, OnInit } from '@angular/core';
//import { Buttons } from './constants';
//import { GridColumnsproduct } from './constants';
import { State } from "@progress/kendo-data-query";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { AddAppProductsComponent } from '../add-app-products/add-app-products.component';
import{AddSubCategoryModel, ViewSubCategoryModel}from './view-subcategory.Models'
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { ActivatedRoute, Router, Routes } from '@angular/router';
import { ViewSubCategoryService } from './view-subcategory.Service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { FilterdataPipe } from '../../../shared/filterdata.pipe';
import { ViewCategoryService } from '../view-category/View-Category.Service';
@Component({
  selector: 'app-view-subcategory',
  templateUrl: './view-subcategory.component.html',
  styleUrls: ['./view-subcategory.component.scss'],
  providers:[DatePipe,FilterdataPipe]
})
export class ViewSubcategoryComponent implements OnInit {
  
  ViewSubCategoryModelObj: ViewSubCategoryModel  = new ViewSubCategoryModel();
  AddSubCategoryModelObj: AddSubCategoryModel = new AddSubCategoryModel();
  public griddata: any;
  formUsrCommonGroup:FormGroup;

  GroupList: any;
  public gridData: any[];
  public gridView: any[];
  public state: State = {};
  //griddata:any;
  UsrName:any;
  //public state: State = {};
  //griddata:any;
  //buttons = Buttons;
  //gridColumns = GridColumnsproduct;
  public defaultTaskItem: { Cat_Name: string, Cat_Pkey: number } = { Cat_Name: 'Select', Cat_Pkey: 0 };
  MessageFlag: string;
  ModelObj: number;
  isHelpActive: boolean;
  isUpdateDisable: boolean;
  SubCat_Pkey: any;
  SubCat_IsActive: boolean;
  SubCat_Name: string;
  isSubmitted: boolean;
  CustomArray: any;
  data: any;
  contentx: any;
  catlist: any;
  

  constructor(private formbuilder:FormBuilder,
    private xviewsubCategoryservice:ViewSubCategoryService ,
    private xaddsubCategoryservice:ViewSubCategoryService ,
    private xRouter:Router,
    private EncrDecr: EncrDecrService,
    public datepipe: DatePipe,
    private xRoute: ActivatedRoute,
    private xmodalService: NgbModal,
    
  ) 
  { 
    debugger
    if (localStorage.getItem('TOKEN') != null) {
      this.UsrName = localStorage.getItem('UserName');
      
    } 
    this.DropDown_Category_Master();
  }
  ngOnInit(): void {
    this.formUsrCommonGroup = this.formbuilder.group({
      SubCat_Name: ["", Validators.required],
      Cat_Name: ["", Validators.required],
      disbledFaltu1: ["", Validators.nullValidator],
      disbledFaltu2: ["", Validators.nullValidator],
    });
    debugger
    this.gridView = this.gridData;
    this.getsubcategoryproduct()
     
  }
  getsubcategoryproduct() 
  {
    debugger
    this.ViewSubCategoryModelObj.Type=1;
    console.log("response");
   this.xviewsubCategoryservice.ViewSubCategoryData(this.ViewSubCategoryModelObj).subscribe((res)=>{
     debugger
      console.log('nikhil',res);
    this.griddata = res[0];
    })
    var rec=this.xviewsubCategoryservice.ViewSubCategoryData;
    console.log(rec);
    // console.log(rec);
    

  }
  GetGridDetails(){ 
    debugger
this.xviewsubCategoryservice.ViewSubCategoryData(this.ViewSubCategoryModelObj)

.subscribe(res =>{
  console.log('unnati',res)
  this.griddata = res[0];
})
  }
  commonMessage() {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });

  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }
  getModelData() {
    debugger
    const id1 = this.xRoute.snapshot.params['id'];
    if (id1 == 'new') {
      this.ViewSubCategoryModelObj = new ViewSubCategoryModel();
    } else {
      debugger
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      debugger
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
    }
  }
  GetSingleData(){
    debugger
    
    this.ViewSubCategoryModelObj.SubCat_Pkey =   this.ModelObj;
    //this.ViewListModelobj.PlayerId =   this.ModelObj;
    //this.ViewCategoryModelObj.UserID =   this.ModelObj;
    
    this.ViewSubCategoryModelObj.Type = 2;
    this.xviewsubCategoryservice.ViewSubCategoryData(this.ViewSubCategoryModelObj).subscribe(res =>{
      console.log('edit',res)
      debugger
this.ViewSubCategoryModelObj.SubCat_Pkey = res[0][0].SubCat_Pkey;
this.ViewSubCategoryModelObj.SubCat_Name = res[0][0].SubCat_Name;
this.ViewSubCategoryModelObj.SubCat_Cat_Pkey = res[0][0].SubCat_Cat_Pkey;
//this.ViewCategoryModelObj.sum_Assist = res[0][0].sum_Assist;
this.ViewSubCategoryModelObj.SubCat_IsActive = res[0][0].SubCat_IsActive;
this.ViewSubCategoryModelObj.Cat_Pkey = res[0][0].Cat_Pkey;
this.ViewSubCategoryModelObj.Cat_Name = res[0][0].Cat_Name;
this.ViewSubCategoryModelObj.Type =2;
if(this.ViewSubCategoryModelObj.SubCat_IsActive==false){
  this.ViewSubCategoryModelObj.SubCat_IsActive=false;
 }
 else{
  this.ViewSubCategoryModelObj.SubCat_IsActive=true;
 }

    })

  }
  checkChange(event, dataItem) {
debugger
    this.AddSubCategoryModelObj.SubCat_Pkey = dataItem.SubCat_Pkey;
    this.AddSubCategoryModelObj.SubCat_IsActive =event;
    this.AddSubCategoryModelObj.Type = 5;

    this.xaddsubCategoryservice
    .SubCategoryDataPost(this.AddSubCategoryModelObj)
    .subscribe(response => {
      this.MessageFlag = "SubCategory status upated...!";
      this.commonMessage();
        this.getsubcategoryproduct();
      });    
  }

  showDetails(content, dataItem) {
    debugger
    this.isHelpActive = false;
    this.ModelObj = dataItem.SubCat_Pkey;
    //console.log('dataItem', dataItem);
    //this.categoryName = dataItem.Con_Cat_Name;
    //this.fallbackIcon = dataItem.Con_Cat_Icon;
    //this.iconCss.setValue(dataItem.Con_Cat_Icon);
    this.AddSubCategoryModelObj.SubCat_Pkey = dataItem.SubCat_Pkey;
    this.AddSubCategoryModelObj.SubCat_Name = dataItem.SubCat_Name;
    this.AddSubCategoryModelObj.SubCat_Cat_Pkey = dataItem.SubCat_Cat_Pkey;
    this.AddSubCategoryModelObj.Cat_Pkey = dataItem.Cat_Pkey;
    this.AddSubCategoryModelObj.Cat_Name = dataItem.Cat_Name;
    //this.AddContractorCategoryModelObj.Con_Cat_Icon = dataItem.Con_Cat_Icon;
    this.SubCat_Pkey= dataItem.SubCat_Pkey;
    // if (this.iconColor != '') {
    //   //console.log('color');
    //   var str1 = new String("#");
    //   var str2 = dataItem.Con_Cat_Back_Color;
    //   if (str2 == null) {
    //     str2 = '000000'
    //   }
    //   this.iconColor = str1.concat(str2);
    //   // this.AddContractorCategoryModelObj.Main_Cat_Back_Color = str1.concat(str2);
    // }
  
    this.AddSubCategoryModelObj.SubCat_IsActive = dataItem.SubCat_IsActive;
    //console.log("showDetails", this.AddContractorCategoryModelObj)
    this.isUpdateDisable = true;
    this.formUsrCommonGroup.disable();
    this.xmodalService.open(content);
    
  }
  deleteDetails(event, dataItem) {
    debugger
     var cfrm = confirm("Do you want to delete this Record...!");

    if (cfrm == true) {
      this.AddSubCategoryModelObj.SubCat_Pkey = dataItem.SubCat_Pkey;
      this.AddSubCategoryModelObj.SubCat_IsActive =false;
      this.AddSubCategoryModelObj.SubCat_IsDelete =true;
      //this.AddAppUserModelObj.User_DOB=null;
      this.AddSubCategoryModelObj.Type=4;

      //this.xaddsubCategoryservice.SubCategoryDataPost(this.AddSubCategoryModelObj)
      this.xaddsubCategoryservice.SubCategoryDataPost(this.AddSubCategoryModelObj)
        .subscribe(response => {

          debugger
         
          this.MessageFlag = "Data Deleted Succesfully";
          this.commonMessage();
          console.log('delete response', response);
          this.getsubcategoryproduct();
          
        });
    }
  }

  DispalyInfo(event: Event, lblName)
  {    
    if (this.isHelpActive) {
      event.preventDefault();
      this.MessageFlag = "Add Information for " + lblName;
      this.commonMessage();
    }    
  }
  formButton(content){
    debugger
   // .fromButton(this.AddAppproductModelObj)
   this.contentx = content;
   this.isSubmitted = true;
  debugger
      // stop here if form is invalid
      if (this.formUsrCommonGroup.invalid) {
        alert('All fields are required?')
        return;
      }
    console.log('responese');
    debugger
    if(this.ModelObj>0)
    {
      this.AddSubCategoryModelObj.SubCat_Pkey=this.ModelObj;
      this.AddSubCategoryModelObj.Type=2;
    }
    else{
      this.AddSubCategoryModelObj.Type=1;
    }
       debugger
        console.log('check',this.AddSubCategoryModelObj)
        this. xaddsubCategoryservice
               .SubCategoryDataPost(this.AddSubCategoryModelObj)
      .subscribe(respose =>{
        
        alert("Data Saved...");
        this.getsubcategoryproduct();
      console.log('res',respose)
  
      })
  
  }
  getData() {
    debugger
        this.AddSubCategoryModelObj.SubCat_Pkey = 0;
        this.AddSubCategoryModelObj.SubCat_Name = "";
        this.AddSubCategoryModelObj.SubCat_Cat_Pkey= 0;
        this.AddSubCategoryModelObj.Cat_Name = "";
        this.AddSubCategoryModelObj.SubCat_IsActive = false;
    
      }
   content;
   swap = [];
   addRow(item) {
     
     console.log('check',item)
     debugger
 
     let data = {
      SubCat_Pkey: 0,
      SubCat_Name: item.SubCat_Name,
      SubCat_Cat_Pkey: item.SubCat_Cat_Pkey,
      Cat_Name:item.Cat_Name,
      SubCat_IsActive : false,
     }
     this.griddata.push(data);
     this.formButton(this.content);
     this.getData();
     // this.SurveyArrayobj.sur_OPT_VAL = "";
     console.log('check array :',this.swap);
     //alert("Data Saved...");
   }
 
   deleteRow(index) {  
     debugger
     if(this.data.length == 1) { 
       // this.toastr.error("Can't delete the row when there is only one row", 'Warning');  
         return false;  
     } else {
         // this.getData();  
         this.CustomArray.splice(index, 1);  
         // this.toastr.warning('Row deleted successfully', 'Delete row');  
         return true;  
     }  
   }

   CategoryList:any;
RaceTypeList:any;
DropDown_Category_Master(){
  debugger
  this.xaddsubCategoryservice.DropDown_Category_Master(this.AddSubCategoryModelObj)
.subscribe(respose =>{
  console.log('drd',respose)
  
  this.CategoryList = respose[0];
  this.catlist =  this.CategoryList;
  
  })
 
}

  CategoryFilter(value){
    if (value!='') {
      var filteredcustomer = this.CategoryList.filter(function (el) {
        return el.Cat_Name != null;
      });
      this.catlist = filteredcustomer.filter((s) => s.Cat_Name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
    }
   else{
    this.catlist = this.CategoryList.slice();
   }
  }
}
