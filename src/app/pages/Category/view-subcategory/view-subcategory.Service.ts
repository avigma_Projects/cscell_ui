import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
import { BaseUrl } from 'src/app/services/apis/rest-api';
import { ErrorhandlepageServices } from 'src/app/services/common-handleError/commonhandleError.services';
import { environment } from 'src/environments/environment';
import{AddSubCategoryModel, ViewSubCategoryModel}from './view-subcategory.Models'
@Injectable({
    providedIn: 'root'
  })
  export class ViewSubCategoryService {
    open(IplAppModalContent: IplAppModalContent, arg1: { size: string; ariaLabelledBy: string; }) {
      throw new Error('Method not implemented.');
    }
    public token:any;
    constructor(private _http: HttpClient,
        private _Route: Router,
        private xHomepageServices: ErrorhandlepageServices) 
        { 
          this.token = JSON.parse(localStorage.getItem('TOKEN'));
    
      }
      private apiUrlPOST = BaseUrl + environment.Admin.GetSubCategoryDetails;
  public ViewSubCategoryData(Modelobj:ViewSubCategoryModel)
   {
    var ANYDTO:any = {};
    ANYDTO.SubCat_Pkey=Modelobj.SubCat_Pkey;
    ANYDTO.SubCat_Cat_Pkey=Modelobj.SubCat_Cat_Pkey;
    ANYDTO.Type=Modelobj.Type;


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
       
      );

  }


  private apiUrlPOST1 = BaseUrl + environment.Admin.AddSubCategoryDetails;

  public SubCategoryDataPost(Modelobj:AddSubCategoryModel) {
    debugger
    var ANYDTO: any = {};
    ANYDTO.SubCat_Pkey = Modelobj.SubCat_Pkey;
    ANYDTO.SubCat_Name = Modelobj.SubCat_Name;
    ANYDTO.SubCat_Cat_Pkey = Modelobj.SubCat_Cat_Pkey;
    ANYDTO.SubCat_IsActive = Modelobj.SubCat_IsActive;
    ANYDTO.Cat_Name = Modelobj.Cat_Name;
    ANYDTO.SubCat_IsDelete = Modelobj.SubCat_IsDelete;
    ANYDTO.UserID = Modelobj.UserID;
    ANYDTO.Type = Modelobj.Type;
    // if (Modelobj.Type != 3) {
    //   ANYDTO.Type = 1;

    //   if (Modelobj.SubCat_Pkey != 0) {
    //     ANYDTO.Type = 2;
    //   }
    //   if (Modelobj.SubCat_IsDelete) {
    //     ANYDTO.Type = 4;
    //   }
    // }
    // else{
    //   ANYDTO.Type = 3;
    // } 
   

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST1, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
      );
  }

  private apiUrlPOST2 = BaseUrl + environment.Admin.DropDownCategory;
    public DropDown_Category_Master(Modelobj:ViewSubCategoryModel) {
      debugger
      let ANYDTO: any = {};
      ANYDTO.Cat_Pkey=Modelobj.Cat_Pkey;
      ANYDTO.Type=Modelobj.Type;
      let headers = new HttpHeaders({ "Content-Type": "application/json" });
      headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
      return this._http
        .post<any>(this.apiUrlPOST2, ANYDTO, { headers: headers })
        .pipe(
          tap(data => {
            return data;
          }),
          catchError(this.CommonhandleError)
        );
    }
    // common handler

    public CommonhandleError(error: HttpErrorResponse) {
      if (error.status == 401) {
        alert('Unauthorized User Found...!');
        // window.location.href = '/';
      } else {
        alert("Something bad happened, please try again later...😌");
      }
  
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error("An error occurred:", error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` + `body was: ${error.error}`
        );
      }
      // return an observable with a user-facing error message
      return throwError("Something bad happened; please try again later.");
    }  

   
}
