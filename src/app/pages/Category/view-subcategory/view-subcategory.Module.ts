import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {ViewSubCategoryService} from './view-subcategory.Service';
import { ViewSubcategoryComponent } from './view-subcategory.component';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
export const ViewSubCategoryRouts: Routes = [
    {  path: 'viewappsubcategory', component:ViewSubcategoryComponent},
  
      // { 
      //  path: 'add/:id',
      //  loadChildren: () => import('../add-app-products/add-app-products.module').then(m => m.AddAppProductsModule ),
      //  },
    { path: '', redirectTo: 'home', pathMatch: 'full'}
  ]
  @NgModule({
    declarations: [ViewSubcategoryComponent],
    imports: [ RouterModule.forChild(ViewSubCategoryRouts),
       
      GridModule, ExcelModule,
      FormsModule, ReactiveFormsModule,
      DropDownsModule,
      CommonModule
    ],
    providers: [ViewSubCategoryService],
    bootstrap: [ViewSubcategoryComponent]
  })
  export class ViewSubCategoryModule { }
  