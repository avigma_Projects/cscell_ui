import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {ViewCategoryComponent} from './view-category.component';
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {ViewCategoryService} from './View-Category.Service';
export const ViewCategoryRouts: Routes = [
    {  path: 'viewappcategory', component:ViewCategoryComponent},
  
      // { 
      //  path: 'add/:id',
      //  loadChildren: () => import('../add-app-products/add-app-products.module').then(m => m.AddAppProductsModule ),
      //  },
    { path: '', redirectTo: 'home', pathMatch: 'full'}
  ]
  @NgModule({
    declarations: [ViewCategoryComponent],
    imports: [ RouterModule.forChild(ViewCategoryRouts),
       
      GridModule, ExcelModule,
      FormsModule, ReactiveFormsModule,
      CommonModule
    ],
    providers: [ViewCategoryService],
    bootstrap: [ViewCategoryComponent]
  })
  export class ViewCategoryModule { }
  