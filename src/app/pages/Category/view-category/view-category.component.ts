import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

//import { Buttons } from './constants';
//import { GridColumnsproduct } from './constants';
import { State } from "@progress/kendo-data-query";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { AddAppProductsComponent } from '../add-app-products/add-app-products.component';
import{AddCategoryModel, ViewCategoryModel}from './View-Category.Models'
import { EncrDecrService } from 'src/app/services/util/encr-decr.service';
import { IplAppModalContent } from 'src/app/component/iplapp-modal-content/iplapp-modal-content.component';
//import { IplAppModalContent } from 'src/app/components';
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { ActivatedRoute, Router, Routes } from '@angular/router';
import { ViewCategoryService } from './View-Category.Service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import {FormFields} from './constants/form-fields'
import { DatePipe } from '@angular/common';
import { FilterdataPipe } from '../../../shared/filterdata.pipe';
import { Buttons } from '../../appinces-details/constants';
//import { FormFields } from '../../app-products/view-app-products/constants/form-fields';
@Component({
  selector: 'app-view-category',
  templateUrl: './view-category.component.html',
  styleUrls: ['./view-category.component.scss'],
  providers:[DatePipe,FilterdataPipe]
})
export class ViewCategoryComponent implements OnInit {
  @ViewChild('contentCateFORM') contentCateFORM: ElementRef;
  ViewCategoryModelObj: ViewCategoryModel = new ViewCategoryModel();
 // AddAppproductModelObj: addappproductmodel = new addappproductmodel();
 AddCategoryModelObj: AddCategoryModel = new AddCategoryModel();

  public griddata: any;
  formUsrCommonGroup:FormGroup;

  GroupList: any;
  public gridData: any[];
  public gridView: any[];
  public state: State = {};
  //griddata:any;
  UsrName:any;
  //public state: State = {};
  //griddata:any;
  //buttons = Buttons;
  //gridColumns = GridColumnsproduct;
  buttons = Buttons;
  myFormGroup: FormGroup;
  fallbackIcon = 'fas fa-user';
  button: string;
  MessageFlag: string;
  ModelObj: number;
  isUpdateDisable: boolean;
  isHelpActive: boolean;
  categoryName: string;
  //FormFields = FormFields;
  iconCss: any;
  iconColor: string;
  isActive: boolean;
  conId: any;
  isSubmitted: boolean;
  //formBuilder: any;
  Cat_Name: string;
  Cat_IsActive: boolean;
  Cat_Pkey: any;
  isEditDisable: boolean;
  data: any;
  CustomArray: any;
  contentx: any;
 
  constructor(private formbuilder:FormBuilder,
    private xviewCategoryservice:ViewCategoryService ,
    private xaddCategoryservice:ViewCategoryService ,
    private xRouter:Router,
    private EncrDecr: EncrDecrService,
    public datepipe: DatePipe,
    private xRoute: ActivatedRoute,
    private xmodalService: NgbModal,
  ) 
  { 
    debugger
    if (localStorage.getItem('TOKEN') != null) {
      this.UsrName = localStorage.getItem('UserName');
    } 
  }

  // ngOnInit(): void {
  //   debugger
  //   if (localStorage.getItem('TOKEN') != null) {
  //     this.UsrName = localStorage.getItem('UserName');
  //     this.button = this.isUpdateDisable ? 'Update' : ' Save';
  //     this.formUsrCommonGroup = this.formBuilder.group({
  //       Cat_Name: ["", Validators.required],
  //       // disbledFaltu1: ["", Validators.nullValidator],
  //       // disbledFaltu2: ["", Validators.nullValidator],
  //     });
  //   } 
  //   debugger
  //   this.gridView = this.gridData;
  //   this.getcategoryproduct()
     
  // }
  ngOnInit() {  
    debugger
  
   this.formUsrCommonGroup = this.formbuilder.group({
    Cat_Name: ["", Validators.required],
    disbledFaltu1: ["", Validators.nullValidator],
    disbledFaltu2: ["", Validators.nullValidator],
  });
  this.gridView = this.gridData;
  debugger
     this.getcategoryproduct()
    }
  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  
  getcategoryproduct() 
  {
    debugger
    this.ViewCategoryModelObj.Type=1;
    console.log("response");
   this.xviewCategoryservice.ViewCategoryData(this.ViewCategoryModelObj).subscribe((res)=>{
     debugger
      console.log('nikhil',res);
    this.griddata = res[0];
    this.CustomArray = Response[0];
    })
    var rec=this.xviewCategoryservice.ViewCategoryData;
    console.log(rec);
    // console.log(rec);
    

  }
  GetGridDetails(){ 
    debugger
this.xviewCategoryservice.ViewCategoryData(this.ViewCategoryModelObj)

.subscribe(res =>{
  console.log('unnati',res)
  this.griddata = res[0];
})
  }
  commonMessage() {
    const modalRef = this.xmodalService.open(IplAppModalContent, { size: "sm", ariaLabelledBy: "modal-basic-title" });
    modalRef.componentInstance.MessageFlag = this.MessageFlag;
    modalRef.result.then(result => { }, reason => { });

  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }
  getModelData() {
    debugger
    const id1 = this.xRoute.snapshot.params['id'];
    if (id1 == 'new') {
      this.ViewCategoryModelObj = new ViewCategoryModel();
    } else {
      debugger
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      debugger
      this.ModelObj = parseInt(id);
      console.log('id',this.ModelObj)
      this.GetSingleData();
    }
  }
  GetSingleData(){
    debugger
    
    this.ViewCategoryModelObj.Cat_Pkey =   this.ModelObj;
    
    //this.ViewCategoryModelObj.UserID =   this.ModelObj;
    
    this.ViewCategoryModelObj.Type = 2;
    this.xviewCategoryservice.ViewCategoryData(this.ViewCategoryModelObj).subscribe(res =>{
      console.log('edit',res)
      debugger
this.ViewCategoryModelObj.Cat_Pkey = res[0][0].Cat_Pkey;
this.ViewCategoryModelObj.Cat_Name = res[0][0].Cat_Name;
this.ViewCategoryModelObj.Cat_IsActive = res[0][0].Cat_IsActive;



this.ViewCategoryModelObj.Type =2;
if(this.ViewCategoryModelObj.Cat_IsActive==false){
  this.ViewCategoryModelObj.Cat_IsActive=false;
 }
 else{
  this.ViewCategoryModelObj.Cat_IsActive=true;
 }

    })

  }
  checkChange(event, dataItem) {
debugger
    this.AddCategoryModelObj.Cat_Pkey = dataItem.Cat_Pkey;
    this.AddCategoryModelObj.Cat_IsActive =event;
    this.AddCategoryModelObj.Type = 5;

    this.xaddCategoryservice
    .CategoryDataPost(this.AddCategoryModelObj)
    .subscribe(response => {
      this.MessageFlag = "Category status upated...!";
      this.commonMessage();
        this.getcategoryproduct();
      });    
  }
  // addcategory(event, content) {
  //   debugger
  //   this.Cat_Name = "";
  //   //this.iconCss.setValue('fas fa-user');
  //   //this.iconColor = "#000000";
  //   this.Cat_IsActive = true;
  //   this.isHelpActive = false;
  //   if (event === 'Create Category') {
  //     this.isUpdateDisable = false;
  //     this.AddCategoryModelObj = new AddCategoryModel();
  //     this.xmodalService.open(content);
  //   } else {
  //     this.xmodalService.open(this.contentCateFORM);
     
  //   }
  // }


  addcategory(event, content) {
    debugger
    if (event === "Create Customer Number") {
      this.isEditDisable = false;
      this.AddCategoryModelObj = new AddCategoryModel();
      this.xmodalService.open(content);
    } else {
      this.xmodalService.open(this.contentCateFORM);
    }
  }
  showDetails(content, dataItem) {
    debugger
    this.isHelpActive = false;
    this.ModelObj = dataItem.Cat_Pkey;
    //console.log('dataItem', dataItem);
   
    this.AddCategoryModelObj.Cat_Pkey = dataItem.Cat_Pkey;
    this.AddCategoryModelObj.Cat_Name = dataItem.Cat_Name;
    
    this.Cat_Pkey = dataItem.Cat_Pkey;
    
  
    this.AddCategoryModelObj.Cat_IsActive = dataItem.Cat_IsActive;
    console.log("showDetails", this.AddCategoryModelObj)
    this.isUpdateDisable = true;
    this.formUsrCommonGroup.disable();
    this.xmodalService.open(content);
    
  }
  deleteDetails(event, dataItem) {
    debugger
     var cfrm = confirm("Do you want to delete this Record...!");

    if (cfrm == true) {
      this.AddCategoryModelObj.Cat_Pkey = dataItem.Cat_Pkey;
      this.AddCategoryModelObj.Cat_IsActive =false;
      this.AddCategoryModelObj.Cat_IsDelete =true;
      //this.AddAppUserModelObj.User_DOB=null;
      this.AddCategoryModelObj.Type=4;
debugger
      this.xaddCategoryservice.CategoryDataPost(this.AddCategoryModelObj)
        .subscribe(response => {

          debugger
          this.MessageFlag = "Data Deleted Succesfully";
          this.commonMessage();
          console.log('delete response', response);
          this.getcategoryproduct();
          
        });
    }
  }


  SetHelpFlag()
  {
    this.isHelpActive = !this.isHelpActive
    if (this.isHelpActive) {
      this.MessageFlag = "Item Help mode is on...!";
      this.commonMessage();
    }
    else
    {
      this.MessageFlag = "Item Help mode is off...!";
      this.commonMessage();
    }
  }

  DispalyInfo(event: Event, lblName)
  {    
    if (this.isHelpActive) {
      event.preventDefault();
      this.MessageFlag = "Add Information for " + lblName;
      this.commonMessage();
    }    
  }
  onIconPickerSelect(icon: string): void {
    this.iconCss.setValue(icon);
  }
  contractorCategoryHandler($event){
    this.categoryName = $event.target.value;
  }
  //color picker
  colorPicker($event){
    this.iconColor = $event.target.value;
  }
  checkValue(state){
    //console.log(state);
  }



//   formButton() {
//     debugger
//     this.isSubmitted = false;
 
//     //  if (this.AddContractorCategoryModelObj.Con_Cat_Back_Color == "") {
//     //    this.AddContractorCategoryModelObj.Con_Cat_Back_Color = '#000000';
//     //  }
 
//      if (this.ModelObj == undefined) {
//        this.AddCategoryModelObj.Cat_Pkey = 0;
//      } else {
//        this.AddCategoryModelObj.Cat_Pkey = this.ModelObj;
//      }
//  debugger
//      this.xaddCategoryservice
//        .CategoryDataPost(this.AddCategoryModelObj)
//        .subscribe(response => {
//          if (response[0].Status != "0") {
//            this.AddCategoryModelObj.Cat_Pkey = parseInt(
//              response[0].Cat_Pkey
//            );
 
//            this.MessageFlag = "Contractor Category Data Saved...!";
//            this.isSubmitted = true;
//            this.commonMessage();
//            this.GetGridDetails() ;
//          }
//          else {
//            if (response[0] == 'Index was outside the bounds of the array.') {
//              this.MessageFlag = "Some Think Wrong...!";
//              this.commonMessage();
//            }
//          }
//        });
//    }
formButton(content){
  debugger
 // .fromButton(this.AddAppproductModelObj)
 this.contentx = content;
 this.isSubmitted = true;
debugger
    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }
  console.log('responese');
  debugger
  if(this.ModelObj>0)
  {
    this.AddCategoryModelObj.Cat_Pkey=this.ModelObj;
    this.AddCategoryModelObj.Type=2;
  }
  else{
    this.AddCategoryModelObj.Type=1;
  }
     debugger
      console.log('check',this.AddCategoryModelObj)
      this.xaddCategoryservice
             .CategoryDataPost(this.AddCategoryModelObj)
    .subscribe(respose =>{
      this.getcategoryproduct();
     
    console.log('res',respose)

    })

}
 
   saveForms() {
     debugger
     this.AddCategoryModelObj.Cat_Name = this.Cat_Name;
     //this.AddContractorCategoryModelObj.Con_Cat_Icon = this.iconCss.value;
     //this.AddContractorCategoryModelObj.Con_Cat_Back_Color = this.iconColor;
     this.AddCategoryModelObj.Cat_IsActive = this.Cat_IsActive;
     this.isUpdateDisable = false;
     this.xaddCategoryservice
       .CategoryDataPost(this.AddCategoryModelObj)
       .subscribe(response => {
         //console.log(response);
         if (response[0].Status != "0") {
           this.AddCategoryModelObj.Cat_Pkey = parseInt(
             response[0].Cat_Pkey
           );
 
           this.MessageFlag = "Contractor Category Data Saved...!";
           this.isSubmitted = true;
           this.commonMessage();
           this.GetGridDetails() ;
         }
         else {
           if (response[0].Status == "0") {
             this.MessageFlag = "This Record Allready Exist";
             this.commonMessage();
           }
           if (response[0] == 'Index was outside the bounds of the array.') {
             this.MessageFlag = "Some Think Wrong...!";
             this.commonMessage();
           }
         }
       });
   }
   //update category
   updateForms(){
     debugger
    this.AddCategoryModelObj.Cat_Name = this.categoryName;
     //this.AddContractorCategoryModelObj.Con_Cat_Icon = this.fallbackIcon;
     //this.AddContractorCategoryModelObj.Con_Cat_Back_Color = this.iconColor;
     this.AddCategoryModelObj.Cat_IsActive = this.Cat_IsActive;
     this.AddCategoryModelObj.Cat_Pkey = this.Cat_Pkey;
     this.xaddCategoryservice
     .CategoryDataPost(this.AddCategoryModelObj)
     .subscribe(response => {
       //console.log(response);
       if (response[0].Status != "0") {
         // this.AddContractorCategoryModelObj.Main_Cat_pkeyID = parseInt(
         //   response[0].Main_Cat_pkeyID
         // );
 
         this.MessageFlag = "Contractor Category Data Updated...!";
         this.isSubmitted = true;
         this.commonMessage();
         this.GetGridDetails() ;
       }
       else {
         if (response[0] == 'Index was outside the bounds of the array.') {
           this.MessageFlag = "Some Think Wrong...!";
           this.commonMessage();
         }
       }
     });
   }
   closeModal() {
    this.xmodalService.dismissAll();
  }
  filterCall() {
    this.ViewCategoryModelObj.Type = 3;
    this.xviewCategoryservice
      .ViewCategoryData( this.ViewCategoryModelObj)
      .subscribe(response => {
        this.state.take = 15;
        this.state.skip = 0;
        //console.log("category", response);
        this.griddata = response[0];
      });
  }
  getData() {
debugger
    this.AddCategoryModelObj.Cat_Pkey = 0;
    this.AddCategoryModelObj.Cat_Name = "";
   
    this.AddCategoryModelObj.Cat_IsActive = false;

  }

  content;
  swap = [];
  addRow(item) {
    
    console.log('check',item)
    debugger

    let data = {
      Cat_Pkey: 0,
      Cat_Name: item.Cat_Name,
      
      Cat_IsActive : false,
    }
    this.griddata.push(data);
    this.formButton(this.content);
    this.getData();
    // this.SurveyArrayobj.sur_OPT_VAL = "";
    console.log('check array :',this.swap);
    alert("Data Saved...");
  }

  deleteRow(index) {  
    debugger
    if(this.data.length == 1) { 
      // this.toastr.error("Can't delete the row when there is only one row", 'Warning');  
        return false;  
    } else {
        // this.getData();  
        this.CustomArray.splice(index, 1);  
        // this.toastr.warning('Row deleted successfully', 'Delete row');  
        return true;  
    }  
  }
}
