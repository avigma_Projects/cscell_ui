export class ViewCategoryModel{
    Cat_Pkey:number=0;
    Cat_Name:string='';
    Cat_IsActive:boolean=true;
    Cat_IsDelete:boolean=false;
    Type:Number=1;
}

export class AddCategoryModel{
    Cat_Pkey:number=0;
    Cat_Name:string='';
    Cat_IsActive:boolean=false;
    Cat_IsDelete:boolean=false;
    UserID: Number = 0;
    Type:Number=2;
}