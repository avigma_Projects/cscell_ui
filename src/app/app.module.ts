import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { AgmCoreModule } from '@agm/core';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns'; 

import { ToastrModule } from 'ngx-toastr';
import { PipesModule } from './theme/pipes/pipes.module';

import { AppRoutingModule } from './app.routing';
import { AppSettings } from './app.settings';

import { AppComponent } from './app.component';
import { PagesComponent } from './pages/pages.component';
import { HeaderComponent } from './theme/components/header/header.component';
import { FooterComponent } from './theme/components/footer/footer.component';
import { SidebarComponent } from './theme/components/sidebar/sidebar.component';
import { VerticalMenuComponent } from './theme/components/menu/vertical-menu/vertical-menu.component';
import { HorizontalMenuComponent } from './theme/components/menu/horizontal-menu/horizontal-menu.component';
import { BreadcrumbComponent } from './theme/components/breadcrumb/breadcrumb.component';
import { BackTopComponent } from './theme/components/back-top/back-top.component';
import { FullScreenComponent } from './theme/components/fullscreen/fullscreen.component';
//import { ApplicationsComponent } from './theme/components/applications/applications.component';
//import { MessagesComponent } from './theme/components/messages/messages.component';
import { UserMenuComponent } from './theme/components/user-menu/user-menu.component';
// import { FlagsMenuComponent } from './theme/components/flags-menu/flags-menu.component';
// import { SideChatComponent } from './theme/components/side-chat/side-chat.component';
// import { FavoritesComponent } from './theme/components/favorites/favorites.component';
import { BlankComponent } from './pages/blank/blank.component';
import { SearchComponent } from './pages/search/search.component';
import { NotFoundComponent } from './pages/errors/not-found/not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { HomeModule } from './home/home.module';
//import { ViewColourMasterComponent } from './pages/Colour-Master/view-colour-master/view-colour-master.component';
//import { AddLocationMasterComponent } from './pages/Location-Master/add-location-master/add-location-master.component';
//import { ViewLocationMasterComponent } from './pages/Location-Master/view-location-master/view-location-master.component';
//import { ViewVendorMasterComponent } from './pages/Vendor-Master/view-vendor-master/view-vendor-master.component';
// import { ProductsComponent } from './pages/products/products.component';
// import { AddAppProductsComponent } from './pages/app-products/add-app-products/add-app-products.component';
// import { ViewAppProductsComponent } from './pages/app-products/view-app-products/view-app-products.component';
// import { GroupDetailsModule } from './group-details/group-details.module';
// import { AddGroupDetailsComponent } from './add-group-details/add-group-details.component';
// import { ViewGroupDetailsComponent } from './view-group-details/view-group-details.component';
// import { AddGroupDetail1Component } from './add-group-detail1/add-group-detail1.component';
// import { AddGroupDetails1Component } from './add-group-details1/add-group-details1.component';
// import { ViewGroupDetailsComponent } from './pages/Group-Details/view-Group-Details/view-group-details/view-group-details.component';
// import { AddGroupDetailsComponent } from './pages/Group-Details/add-group-details/add-group-details.component';
// import{ViewGroupDetailsModule} from './pages/group-details2/view-group-details/view-group-details.module'
// import { AddGroupDetailsModule } from './pages/group-details2/add-group-details/add-group-details.module';
// import { AddGroupDetailModule } from './pages/Group-Details/add-group-details/add-group-detail.module';
// import { viewGroupDetailModule } from './pages/Group-Details/view-Group-Details/view-group-details.module';
// import { AddGroupDetailsModule } from './pages/group-details2/add-group-details/add-group-details.module';
// import { ViewGroupDetailsRoutingModule } from './pages/group-details2/view-group-details/view-group-details-routing.module';
 

@NgModule({  
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    PerfectScrollbarModule,
    NgbModule,
    MultiselectDropdownModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAO7Mg2Cs1qzo_3jkKkZAKY6jtwIlm41-I'
    }),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ToastrModule.forRoot(), 
    PipesModule,
    AppRoutingModule,
    HttpClientModule,
    HomeModule,
    // GroupDetailsModule
  ],
  declarations: [
    AppComponent,
    PagesComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    VerticalMenuComponent,
    HorizontalMenuComponent,
    BreadcrumbComponent,
    BackTopComponent,
    FullScreenComponent,
    // ApplicationsComponent,
    // MessagesComponent,
    UserMenuComponent,
    //FlagsMenuComponent,
    //SideChatComponent,
   
    BlankComponent,
    //SearchComponent,
    NotFoundComponent,
    //ViewColourMasterComponent,
    //AddLocationMasterComponent,
   // ViewLocationMasterComponent,
    //ViewVendorMasterComponent,
    // ProductsComponent,
    // AddAppProductsComponent,
    // ViewAppProductsComponent,
   

    // AddGroupDetailsComponent,
    // ViewGroupDetailsComponent,
    // AddGroupDetail1Component,
    // AddGroupDetails1Component,
    // ViewGroupDetailsComponent,
    // AddGroupDetailModule,
    // viewGroupDetailModule,
    // viewGroupDetailModule 
    // AddGroupDetailsModule,
    // ViewGroupDetailsRoutingModule
   
    

  ],
  providers: [ 
    AppSettings,
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { } 