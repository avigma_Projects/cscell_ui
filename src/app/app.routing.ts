import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules  } from '@angular/router'; 
import { PagesComponent } from './pages/pages.component';
import { BlankComponent } from './pages/blank/blank.component';
import { SearchComponent } from './pages/search/search.component';
import { NotFoundComponent } from './pages/errors/not-found/not-found.component';
// import {} from './pages/support-ticket/view-support-ticket/view-support-ticket.module'
import { LoginComponent } from './pages/auth/login/login.component';

export const routes: Routes = [
  {
    path: 'admin',
    loadChildren: () => import('../app/pages/auth/auth.module').then(m => m.AuthModule),
  },
  {
    path: 'home', 
    component: PagesComponent,
    children:[
      { path: 'dashboard', loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule), data: { breadcrumb: 'Dashboard' }  },          
      // {
      //   path: 'iplcompany',
      //   loadChildren: () => import('../app/pages/ipl-company/view-ipl-company/view-ipl-company.module')
      //     .then(m => m.ViewIplCompanyModule), data: { breadcrumb: 'Company' }
      // },
      // {
      //   path: 'contact',
      //   loadChildren: () => import('../app/pages/contact-details/view-contact-us.module').then(m => m.ViewContactUsModule), data: { breadcrumb: 'Contact Details' }
      // },
      {
        path: 'user',
        loadChildren: () => import('../app/pages/user/view-user/view-user.module').then(m => m.ViewUserModule), data: { breadcrumb: 'Admin User Details' }
      },
  //      {
  //   path: 'support',
  //   loadChildren: () =>
  //     import('./pages/support-ticket/view-support-ticket/view-support-ticket.module').then((m) => m.ViewSupportTicketModule), data: { breadcrumb: 'Support Ticket' }
  // },
  // {
  //   path: 'adopter',
  //   loadChildren: () =>
  //     import('./pages/ipl-adopter/view-ipl-adopter/view-ipl-adopter.module').then((m) => m.ViewIplAdopterModule), data: { breadcrumb: 'Adopter ' }
  // },
  {
    path: 'appuser',
    loadChildren: () =>
      import('./pages/app-user/view-app-user/view-app-user.module').then((m) => m.AppUserViewModule), data: { breadcrumb: ' User ' }
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./pages/auth/profile/profile.module').then((m) => m.ProfileModule), data: { breadcrumb: 'Profile' }
  },
  // {
  //   path: 'video',
  //   loadChildren: () =>
  //     import('./pages/training/video-training/video-training.module').then((m) => m.VideoTrainingModule), data: { breadcrumb: 'Video Training' }
  // },
  // {
  //   path: 'applinces',
  //   loadChildren: () =>
  //     import('./pages/appinces-details/view-applince/appinces-details.module').then((m) => m.ViewApplincesModule), data: { breadcrumb: 'Appliance ' }
  // },

  // // {
  //   path: 'import',
  //   loadChildren: () =>
  //     import('./pages/import-from/view-import-from/import-from.module').then((m) => m.ViewImportFromModule), data: { breadcrumb: 'Import Details' }
  // },
  {
    path: 'product',
    loadChildren: () =>
      import('./pages/app-products/view-app-products/view-app-products.module').then((m) => m.ViewAppProductsModule ), data: { breadcrumb: ' Product ' }
  },
  {
    path: 'product/add',
    loadChildren: () =>
      import('./pages/app-products/add-app-products/add-app-products.module').then((m) => m.AddAppProductsModule ), data: { breadcrumb: ' Product ' }
   },
  {
    path: 'Category',
    loadChildren: () =>
      import('./pages/Category/view-category/View-Category.Module').then((m) => m.ViewCategoryModule ), data: { breadcrumb: ' Category ' }
  },
  {
    path: 'Category/add',
    loadChildren: () =>
      import('./pages/Category/view-category/View-Category.Module').then((m) => m.ViewCategoryModule ), data: { breadcrumb: ' Category ' }
   },
  
  {
    path: 'SubCategory',
    loadChildren: () =>
      import('./pages/Category/view-subcategory/view-subcategory.Module').then((m) => m.ViewSubCategoryModule ), data: { breadcrumb: ' SubCategory ' }
  },
  {
    path: 'bin-master',
    loadChildren: () =>
      import('./pages/bin-master/bin-master.module').then((m) => m.BinMasterModule ), data: { breadcrumb: ' Bin Master ' }
  },
  {
    path: 'bin-master-datils',
    loadChildren: () =>
      import('./pages/bin-master/bin-master-details/bin-master-details.module').then((m) => m.BinMasterDetailsModule ), data: { breadcrumb: ' Bin Master Detials' }
  },
  {
    path: 'vendormaster',
    loadChildren: () =>
      import('./pages/Vendor-Master/view-vendor-master/view-vendor-master.Module').then((m) => m.ViewVendorModule ), data: { breadcrumb: ' Vendor ' }
  },
  {
    path: 'addvendormaster',
    loadChildren: () =>
      import('./pages/Vendor-Master/add-vendor-master/add-vendor-master.Module').then((m) => m.AddVendorModule ), data: { breadcrumb: ' Vendor ' }
   },
   {
    path: 'locationmaster',
    loadChildren: () =>
      import('./pages/Location-Master/view-location-master/view-location-master.Module').then((m) => m.ViewLocationModule ), data: { breadcrumb: ' Location ' }
  },
  {
    path: 'addlocationmaster',
    loadChildren: () =>
      import('./pages/Location-Master/add-location-master/add-location-master.Module').then((m) => m.AddLocationModule ), data: { breadcrumb: ' Location ' }
   },
   {
    path: 'Colour',
    loadChildren: () =>
      import('./pages/Colour-Master/view-colour-master/view-colour-master.Module').then((m) => m.ViewColourModule ), data: { breadcrumb: ' Colour ' }
  },
  {
    path: 'Colour/add',
    loadChildren: () =>
      import('./pages/Colour-Master/view-colour-master/view-colour-master.Module').then((m) => m.ViewColourModule ), data: { breadcrumb: ' Colour ' }
   },
  /*
  {//import view module after exec
    path: 'groupdetails',
    loadChildren: () =>
      import('./pages/Group-Details/view-Group-Details/view-group-details.module').then((m) => m.viewGroupDetailModule), data: { breadcrumb: 'Getuser-Group-Detais' }
  },*/
  



  /*{


    http://localhost:4200/home/background/addbackgroundProvider/new
    path:'view-background',
    loadChildren:()=>
    import('./pages/background-providers/view-background-provider/view-background-provider.module')

  },*/

      { path: 'search', component: SearchComponent, data: { breadcrumb: 'Search' } }
    
    ]
  },
  
  // { path: '**', component: NotFoundComponent }
   { path: '', redirectTo: '/admin/login', pathMatch: 'full' },
  
]; 

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // preloadingStrategy: PreloadAllModules, // <- comment this line for activate lazy load
      // relativeLinkResolution: 'IPLAdmin',
      // // useHash: true
    })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }