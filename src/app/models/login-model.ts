export class LoginModel {
  Ipl_Ad_User_PkeyID: Number = 0;
  Ipl_Ad_User_Login_Name: String = "";
  Ipl_Ad_User_Password: string = "";
  user_IsEmailActive: boolean = false;
  user_ActivationCode: string = "";
  type: Number = 2;
  user_LoginNameForgot: string = "";
  user_PasswordOld: string = "";
  user_PasswordNew: string = "";
  user_PasswordConfirm: string = "";
  tokendetails: any;
  ClientId:string = "1";
}
